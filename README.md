# 프로젝트

[CoreUI V4.0](https://coreui.io/react/) 의 템플릿을 사용하여 GStar CAD 라이센스 프로젝트 관리자 페이지를 구현하는데 있어
Front End 를 React 기반으로 구현 처리 합니다.

### React 기반의 CoreUI 관리자 페이지 입니다.

- React 2.0
- axios
- redux
- reactstrap
- npm
- CoreUI V4.0
- [Formik(For Input Validate)](https://www.npmjs.com/package/formik)

### Instalation

```bash
$ npm install
```

or

```bash
$ yarn install
```

### Basic usage

```bash
# dev server with hot reload at http://localhost:3000
$ npm start
```

or

```bash
# dev server with hot reload at http://localhost:3000
$ yarn start
```

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

#### Build

```
npm start           : node.js 서버 시작
npm build-{env}     : 관리자 배포 처리
```

or

## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
coreui-pro-react-admin-template
├── public/          # static files
│   └── index.html   # html template
│
├── src/             # project root
│   ├── assets/      # images, icons, etc.
│   ├── components/  # common components - header, footer, sidebar, etc.
│   ├── layouts/     # layout containers
│   ├── scss/        # scss styles
│   ├── views/       # application views
│   ├── _nav.js      # sidebar navigation config
│   ├── App.js
│   ├── ...
│   ├── index.js
│   ├── routes.js    # routes config
│   └── store.js     # template state example
│
└── package.json
```

## CoreUI 문서

The documentation for the CoreUI Admin Template is hosted at our website [CoreUI for React](https://coreui.io/react/)

브랜치 체크