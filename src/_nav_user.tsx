import React from 'react'
import {
  cilSpeedometer,
  cilAlignCenter,
  cilCog,
  cilGroup,
  cilChartLine,
  cilBarcode,
  cilBook,
  cibAudible,
  cibAlgolia,
  cilFile
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import {
  CNavItem,
  CNavGroup
} from '@coreui/react-pro'
import { ElementType } from 'react'

export type Badge = {
  color: string
  text: string
}

export type NavItem = {
  component: string | ElementType
  name: string | JSX.Element
  icon?: string | JSX.Element
  badge?: Badge
  to: string
  items?: NavItem[]
}

const _nav_user = [
  {
    component: CNavItem,
    name: '대시보드',
    icon: <CIcon icon={cilBook} customClassName="nav-icon" />,
    to: '/realtime-content',
  },
  {
    component: CNavItem,
    name: '정보 현황판',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    to: '/dashboard',
  },
  {
    component: CNavItem,
    name: '중계기 현황판',
    icon: <CIcon icon={cibAudible} customClassName="nav-icon" />,
    to: '/router-content',
  },
  {
    component: CNavItem,
    name: '센서 현황판',
    icon: <CIcon icon={cibAlgolia} customClassName="nav-icon" />,
    to: '/sensor-content',
  },
  {
    component: CNavGroup,
    name: '센서 관제',
    to: '/monitoring-management',
    icon: <CIcon icon={cilChartLine} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '충전소&시설',
        to: '/monitoring-management/station-new',
      },
      {
        component: CNavItem,
        name: '리포트 확인',
        to: '/monitoring-management/report',
      },
    ],
  },
  {
    component: CNavGroup,
    name: '데이터 수신 / 통계',
    to: '/statistics',
    icon: <CIcon icon={cilAlignCenter} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '데이터 수신 목록',
        to: '/statistics/sensor-data',
      },
      {
        component: CNavItem,
        name: '데이터 통계',
        to: '/statistics/date-data',
      },
    ],
  },
]

export default _nav_user
