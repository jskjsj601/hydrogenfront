import React, { Suspense } from 'react'
import { connect } from "react-redux"
import { HashRouter, Route, Switch } from 'react-router-dom'
import { CSpinner } from '@coreui/react-pro'
import Login from './views/Login.js'
import RouteWithProps from './modules/routes/RouteWithProps.js'
import PrivateRoute from './modules/routes/PrivateRoute.js'
import './scss/style.scss'
import authentication from './modules/login/authentication'


// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))
const Page500 = React.lazy(() => import('./views/error/Page500'))

class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
    const authState = authentication.getAuthState()
    this.state = {
      user: {
        isAuthenticated: authState.isLogined,
        userID: authState.userID,
        userInfo: authState.userInfo
      }
    }
    const { dispatch } = this.props
    dispatch({
      type: 'set',
      isAuthenticated: authState.isLogined,
      userID: authState.userID,
      userInfo: authState.userInfo
    })
  }

  login(username, password) {
    const { dispatch } = this.props
    const requestLogin = authentication.login(username, password).then(result => {
      if (result) {
        const statusCode = result.status
        if (statusCode === 200) {
          // 로그인 성공
          const userInfo = result.payload
          this.setState({
            user: {
              isAuthenticated: true,
              userID: userInfo.username,
              userInfo: JSON.stringify(userInfo)
            }
          })
          dispatch({
            type: 'set',
            isAuthenticated: true,
            userID: userInfo.username,
            userInfo: JSON.stringify(userInfo)
          })
        } else {
          this.setState({
            user: {
              isAuthenticated: false,
              userID: null,
              userInfo: {}
            }
          })

          dispatch({
            type: 'set',
            isAuthenticated: true,
            userID: '',
            userInfo: ''
          })
        }
        return result
      } else {
        this.setState({
          user: {
            isAuthenticated: false,
            userID: null,
            userInfo: {}
          }
        })

        dispatch({
          type: 'set',
          isAuthenticated: true,
          userID: '',
          userInfo: ''
        })
      }
    }).catch(err => {
      dispatch({
        type: 'set',
        isAuthenticated: true,
        userID: '',
        userInfo: ''
      })

      this.setState({
        user: {
          isAuthenticated: false,
          userID: null,
          userInfo: {}
        }
      })
      return Promise.reject(err)
    })
    return requestLogin
  }

  logout() {
    authentication.logout().then(result => {
      const { dispatch } = this.props
      authentication.reset()
      dispatch({
        type: 'set',
        isAuthenticated: true,
        userID: '',
        userInfo: ''
      })
      this.setState({
        user: {
          isAuthenticated: false,
          userID: null,
          userInfo: {}
        }
      })
    })
  }

  render(): JSX.Element {
    return (
      <HashRouter>
        <Suspense fallback={<CSpinner color="primary" />}>
          <Switch>
            <RouteWithProps exact path="/login" component={Login} props={{ login: this.login, logout: this.logout }} />
            <Route exact path='/error' render={() => <Page500 />} />
            <PrivateRoute path='/' name="Home" component={DefaultLayout} props={{ userInfo: this.state.user }} />
          </Switch>
        </Suspense>
      </HashRouter >
    )
  }
}

const mapStateToProps = (user) => ({ user })
export default connect(mapStateToProps)(App)
