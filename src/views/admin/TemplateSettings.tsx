import React, { useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from '@coreui/react-pro'
import LoadingOverlay from 'react-loading-overlay-ts'
const TemplateSettings = (): JSX.Element => {
  const [isLoading] = useState(false)
  return <>
    <CRow>
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>템플릿 설정 관리</strong>
          </CCardHeader>
          <CCardBody>
            <CRow>

            </CRow>
            <CRow>
              <LoadingOverlay
                active={isLoading}
                spinner
                text='로딩중 입니다....'
              >

              </LoadingOverlay>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default TemplateSettings
