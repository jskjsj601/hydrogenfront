import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CCardFooter,
  CButton,
  CFormCheck
} from '@coreui/react-pro'
import {
  Input,
} from '../../components/elements'
import service from "../../modules/service"
import LoadingOverlay from 'react-loading-overlay-ts'
import { useFormik } from "formik"
import * as Yup from 'yup'
const SiteSettings = (): JSX.Element => {
  const history = useHistory()
  const [id, setId] = useState("")
  const [isLoading, setLoading] = useState(false)
  const [isSessionTime, setSessionTime] = useState(false)
  const getData = function () {
    setLoading(true)
    service.request.get(`/admin/props`, {}).then(function (response) {
      setLoading(false)
      const data = response.data.payload
      if (data) {
        setId(data.id)
        formik.setFieldValue("sessionExpiredTime", data.sessionExpiredTime || 4999)
        formik.setFieldValue("loginFailedCnt", data.loginFailedCnt || 10)
        formik.setFieldValue("checkSessionExpired", data.checkSessionExpired || 'UNCHECK')
        setSessionTime(data.checkSessionExpired === 'CHECK' ? true : false)
      }
    })
  }

  const onChangeUseExpiredSessionTime = (check) => {
    formik.setFieldValue("checkSessionExpired", (check? 'CHECK' : 'UNCHECK' ))
    setSessionTime(check)
  }

  const form = Yup.object().shape({
    sessionExpiredTime: Yup.string().required("세션 시간은 필수값입니다."),
    loginFailedCnt: Yup.string().required('로그인 실패 갯수는 필수값 입니다.'),
  })

  const formik = useFormik({
    initialValues: {
      sessionExpiredTime: 4999,
      loginFailedCnt: 10,
      checkSessionExpired: 'UNCHECK'
    },
    enableReinitialize: true,
    validationSchema: form,
    onSubmit: (values) => {
      if (window.confirm("사이트 설정을 수정하시겠습니까?")) {
        setLoading(true)
        service.request.put(`admin/props/${id}`, values).then(function () {
          setLoading(false)
          alert("사이트 수정이 완료되었습니다.\n새로운 설정을 적용하기 위해 로그아웃 합니다.")
          history.push("/login")
        })
      }
    }
  })

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getData()
  }, [])

  return <>
    <CRow>
      <CCol md={8}>
        <CCard className="mb-4">
          <CForm onSubmit={formik.handleSubmit}>
            <CCardHeader>
              <strong>사이트 설정</strong>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <LoadingOverlay
                  active={isLoading}
                  spinner
                  text='로딩중 입니다....'
                >
                  <CCol md={12}>
                    <CRow>
                      <CCol>
                        <div className="mb-3">
                          <CFormLabel>

                            로그인 만료 시간 (해당시간 조작이 없는경우 로그아웃 됨) &nbsp;
                            <CFormCheck
                              type="checkbox"
                              name="checkSessionExpired"
                              id="checkSessionExpired"
                              onChange={(e) => onChangeUseExpiredSessionTime(e.target.checked)}
                              checked={formik.values.checkSessionExpired === "CHECK"} /> 로그인 항상 유지
                          </CFormLabel>
                          <Input
                            idOrName={"sessionExpiredTime"}
                            type={"number"}
                            placeholder={"로그인 만료 시간을 입력하여 주시기 바랍니다."}
                            formik={formik}
                            disabled={isSessionTime}
                            value={formik.values.sessionExpiredTime}
                            error={formik.errors.sessionExpiredTime}
                            touched={formik.touched.sessionExpiredTime} />
                        </div>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol>
                        <div className="mb-3">
                          <CFormLabel>로그인 실패 횟수(5 ~ 10, 설정한 횟수 이상 로그인 실패시 해당 아이디 로그인 잠금상태)</CFormLabel>
                          <Input
                            idOrName={"loginFailedCnt"}
                            type={"number"}
                            placeholder={"로그인 실패 횟수를 입력하여 주시기 바랍니다."}
                            formik={formik}
                            value={formik.values.loginFailedCnt}
                            error={formik.errors.loginFailedCnt}
                            touched={formik.touched.loginFailedCnt} />
                        </div>
                      </CCol>
                    </CRow>
                  </CCol>

                </LoadingOverlay>
              </CRow>
            </CCardBody>
            <CCardFooter>
              <CButton color="primary" type="submit">적용</CButton>
            </CCardFooter>
          </CForm>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default SiteSettings
