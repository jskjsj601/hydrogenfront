import React, { useState, useEffect } from 'react'
import { useTypedSelector } from '../../store'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CCardFooter,
  CForm,
  CFormLabel,
  CInputGroup,
  CButton,
  CFormSelect
} from '@coreui/react-pro'
import {
  Input,
  Select
} from '../../components/elements'
import CIcon from '@coreui/icons-react'
import { cilBackspace } from '@coreui/icons'
import { useParams, useHistory } from "react-router-dom"
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import { useFormik } from "formik"
import * as Yup from 'yup'
import ImageInput from '../../components/elements/ImageInput'
import avatar8 from './../../assets/images/avatars/admin.png'

const AdminDetail = (): JSX.Element => {
  const history = useHistory()
  const { id } = useParams<{ id: string }>()
  const [isLoading, setIsLoading] = useState(false)
  const [checkId, setCheckId] = useState(false)
  const [departments, setDepartments] = useState([])
  const type = (id === "0") ? "create" : "update"
  const title = (id === "0") ? "등록" : "수정"
  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))
  const [stations, setStation] = useState<any>([])
  const [searchStation, setPickStation] = useState("0")
  const [showStation, setShowStation] = useState(false)

  const onBack = function () {
    if (window.confirm("입력을 취소하시겠습니까?")) {
      history.push("/admin-management/admins")
    }
  }

  const getInitData = () => {
    const params = {}
    service.request.get(`admin/sensor_data/init`, params).then(function (response) {
      const data = response.data.payload
      setStation(data?.stationList)
    })
  }

  const onChangeStation = (event) => {
    setPickStation(event.target.value)
  }

  const getData = function (Id) {
    setIsLoading(true)
    service.request.get(`/admin/user/${Id}`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      if (data) {
        if (data.roleType === 'ROLE_ADMIN') {
          setPickStation(data.station?.id)
          setShowStation(true)
        }
        formik.setFieldValue("name", data.name || '')
        formik.setFieldValue("status", data.status || 'USE')
        formik.setFieldValue("username", data.username || '')
        formik.setFieldValue("roleType", data.roleType || 'ROLE_SUPER_MANAGER')
        formik.setFieldValue("email", data.email || '')
        formik.setFieldValue("mobileNumber", data.mobileNumber || '')
        formik.setFieldValue("departmentId", data.department?.id || '')
        formik.setFieldValue("profileImage", data.profileImage || '')
      }
    })
  }

  const getCompany = function () {
    setIsLoading(true)
    service.request.get(`/admin/department/all`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      setDepartments(data)
    })
  }

  const checkUserId = function (username) {
    if (username === "") {
      alert("유저 아이디를 입력하여 주시기 바랍니다.")
      return
    }
    setIsLoading(true)
    service.request.get(`/admin/user/checkId`, { username: username }).then(function (response) {
      setIsLoading(false)
      const data = response.data.status
      if (data === 200) {
        alert("해당 아이디는 사용 가능합니다.")
        setCheckId(true)
      } else {
        alert("중복되는 아이디가 있습니다. \n다른 아이디로 입력하여 주시기 바랍니다.")
        return
      }
    })
  }

  const onChangeImage = function (imagePath) {
    formik.setFieldValue("profileImage", imagePath)
  }

  const onChangeRole = function (val) {
    if (val === 'ROLE_ADMIN') {
      setShowStation(true)
      setPickStation("0")
    } else {
      setShowStation(false)
    }
  }

  const onKeyPressNumber = function (value) {
    value = value
      .replace(/[^0-9]/g, '')
      .replace(/^(\d{0,3})(\d{0,4})(\d{0,4})$/g, "$1-$2-$3").replace(/(-{1,2})$/g, "")
    formik.setFieldValue("mobileNumber", value)
  }

  const form = Yup.object().shape({
    name: Yup.string().required("이름은 필수 입니다."),
    username: Yup.string().required('아이디는 필수 입니다.').matches(/^[a-z0-9A-Z]{5,20}$/, "아이디는 영 대소문자, 숫자 5~20자리로 입력해주시기 바랍니다."),
    departmentId: Yup.string().required("조직 선택은 필수 입니다."),
    mobileNumber: Yup.string().required("전화번호는 필수 입니다.").matches(
      /^(0[1-9][01346-9])-?([1-9]{1}[0-9]{2,3})-?([0-9]{4})$/,
      "전화번호의 형식이 잘못되었습니다.."
    ),
    email: Yup.string()
      .required('이메일은 필수 입니다.')
      .matches(
        /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i,
        "이메일의 형식을 체크하여 주시기 바랍니다."
      ),
    password: Yup.string()
      .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/, "비밀번호는 8자이상 특수문자 포함 영 소문자이어야 합니다."),
    passwordConfirm: Yup.string().when("password", {
      is: val => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "비밀번호와 비밀번호 확인은 같은 값이어야 합니다."
      )
    }),
  })

  const formik = useFormik({
    initialValues: {
      name: "",
      username: "",
      departmentId: "",
      status: "USE",
      roleType: "ROLE_SUPER_MANAGER",
      email: "",
      mobileNumber: "",
      password: "",
      passwordConfirm: "",
      profileImage: "",
      stationId: ""
    },
    enableReinitialize: true,
    validationSchema: form,
    onSubmit: (values, obj) => {
      if (values.profileImage === "") {
        alert("프로필 이미지를 확인하여 주시기 바랍니다.")
        return
      }

      if (values.roleType === 'ROLE_ADMIN') {
        if (searchStation === '0') {
          alert("충전소 관리자 권한은 충전소를 선택해주셔야 합니다.")
          return
        }
        values.stationId = searchStation
      }
      if (type === "update") {
        if (window.confirm("관리자를 수정하시겠습니까?")) {
          setIsLoading(true)
          service.request.put(`admin/user/${id}`, values).then(function () {
            setIsLoading(false)
            if (Number(id) === Number(userInfo.id)) {
              alert("동일 계정을 수정하셨습니다.\n계정정보 변경으로 로그아웃 처리됩니다.")
              history.push("/login")
              return
            }
            alert("관리자 수정이 성공적으로 처리되었습니다.")
            history.push("/admin-management/admins")
          })
        }
      } else {
        if (!checkId) {
          alert("아이디 중복체크를 해주시기 바랍니다.")
          return
        }
        if (values.password === "") {
          obj.setFieldError("password", "비밀번호를 체크하여 주시기 바랍니다.")
          return
        }
        if (values.passwordConfirm === "") {
          obj.setFieldError("passwordConfirm", "비밀번호 확인을 체크하여 주시기 바랍니다.")
          return
        }

        if (window.confirm("관리자를 등록하시겠습니까?")) {
          setIsLoading(true)
          service.request.post("admin/user", values).then(function () {
            setIsLoading(false)
            alert("관리자 정보가 성공적으로 등록 되었습니다.")
            history.push("/admin-management/admins")
          })
        }
      }
    }
  })

  const stationList = stations.map((item, key) => {
    return <option key={key} value={item.id}>{item.name}</option>
  })


  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getCompany()
    getInitData()
    if (type === "update") {
      getData(id)
      setCheckId(true)
    }
  }, [])

  return <>
    <CRow>
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <CButton type="button" color="info" style={{ color: '#FFF' }} id="button-addon2" onClick={() => onBack()}>
              <CIcon icon={cilBackspace} className="me-2" /><strong>관리자 {title}</strong>
            </CButton>
          </CCardHeader>
          <CForm onSubmit={formik.handleSubmit}>
            <LoadingOverlay
              active={isLoading}
              spinner
              text='로딩중 입니다....'
            >
              <CCardBody>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>조직선택</CFormLabel>
                      <Select
                        idOrName={"departmentId"}
                        formik={formik}
                        value={formik.values.departmentId}
                        error={formik.errors.departmentId}
                        touched={formik.touched.departmentId}
                        defaultValue={"조직을 선택하여 주세요"}
                        items={departments} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>상태</CFormLabel>
                      <Select
                        idOrName={"status"}
                        formik={formik}
                        value={formik.values.status}
                        error={formik.errors.status}
                        touched={formik.touched.status}
                        items={[
                          { "id": "USE", "name": "사용중" },
                          { "id": "NOT_USE", "name": "사용중지" },
                          { "id": "FORCE_CHECKOUT", "name": "강제 사용중지" },
                          { "id": "LOCKED", "name": "잠금 상태" },
                        ]} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={4}>
                    <div className="mb-3">
                      <CFormLabel>아이디</CFormLabel>

                      <Input
                        idOrName={"username"}
                        type={"text"}
                        placeholder={"아이디를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.username}
                        error={formik.errors.username}
                        touched={formik.touched.username}
                        disabled={checkId} />
                    </div>
                  </CCol>
                  <CCol md={2}>
                    <CInputGroup className="mb-3">
                      {checkId ?
                        <CButton
                          type="button"
                          color="success"
                          style={{ alignContent: 'right', color: '#FFF', marginTop: "31px" }}
                          disabled={type === 'update'}
                          onClick={() => setCheckId(false)}
                        >다시 입력</CButton>
                        : <CButton
                          type="button"
                          color="success"
                          style={{ alignContent: 'right', color: '#FFF', marginTop: "31px" }}
                          onClick={() => checkUserId(formik.values.username)}
                        >아이디 중복 확인</CButton>}
                    </CInputGroup>
                  </CCol>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>이름</CFormLabel>
                      <Input
                        idOrName={"name"}
                        type={"text"}
                        placeholder={"관리자 이름을 입력하세요."}
                        formik={formik}
                        value={formik.values.name}
                        error={formik.errors.name}
                        touched={formik.touched.name} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>이메일</CFormLabel>
                      <Input
                        idOrName={"email"}
                        type={"text"}
                        placeholder={"이메일을 입력하세요."}
                        formik={formik}
                        value={formik.values.email}
                        error={formik.errors.email}
                        touched={formik.touched.email} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>권한</CFormLabel>
                      <Select
                        idOrName={"roleType"}
                        formik={formik}
                        value={formik.values.roleType}
                        error={formik.errors.roleType}
                        touched={formik.touched.roleType}
                        onChangeFunc={onChangeRole}
                        items={[
                          { "id": "ROLE_USER", "name": "모니터링 관리자 권한" },
                          { "id": "ROLE_ADMIN", "name": "충전소 관리자 권한" },
                          { "id": "ROLE_SUPER_MANAGER", "name": "슈퍼 관리자 권한" }
                        ]} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>연락처</CFormLabel>
                      <Input
                        idOrName={"mobileNumber"}
                        type={"text"}
                        placeholder={"연락처를 입력하세요."}
                        formik={formik}
                        value={formik.values.mobileNumber}
                        error={formik.errors.mobileNumber}
                        touched={formik.touched.mobileNumber}
                        onKeyPressNumber={onKeyPressNumber} />
                    </div>
                  </CCol>
                  <CCol md={6}>
                    {showStation &&
                      (<div className="mb-3">
                        <CFormLabel>관리 충전소&시설</CFormLabel>
                        <CFormSelect
                          value={searchStation || '0'}
                          name="stations"
                          onChange={(e) => onChangeStation(e)}>
                          <option value="0">전체(충전소&시설 목록)</option>
                          {stationList}
                        </CFormSelect>
                      </div>)
                    }
                  </CCol>
                </CRow>

                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>비밀번호</CFormLabel>
                      <Input
                        idOrName={"password"}
                        type={"password"}
                        placeholder={"비밀번호를 입력하세요."}
                        formik={formik}
                        value={formik.values.password}
                        error={formik.errors.password}
                        touched={formik.touched.password} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>비밀번호 확인</CFormLabel>
                      <Input
                        idOrName={"passwordConfirm"}
                        type={"password"}
                        placeholder={"비밀번호 확인을 입력하세요."}
                        formik={formik}
                        value={formik.values.passwordConfirm}
                        error={formik.errors.passwordConfirm}
                        touched={formik.touched.passwordConfirm} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>프로필 이미지</CFormLabel>
                      <ImageInput
                        image={formik.values.profileImage}
                        defaultImage={avatar8}
                        inputName={"profileImage"}
                        callbackImage={onChangeImage} />
                    </div>
                  </CCol>
                </CRow>

              </CCardBody>
            </LoadingOverlay>

            <CCardFooter>
              {(type === 'update') ?
                (<CButton color="primary" type="submit">수정하기</CButton>) :
                (<CButton color="primary" type="submit">저장하기</CButton>)}
            </CCardFooter>
          </CForm>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default AdminDetail
