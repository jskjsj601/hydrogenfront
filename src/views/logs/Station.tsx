import React, { useEffect } from 'react'
import { useHistory } from "react-router-dom"
import {
  CRow,
} from '@coreui/react-pro'

const Station = (): JSX.Element => {
  const history = useHistory()
  const redirectDashboard = function () {
    alert("서비스 준비중입니다. 메인 페이지로 이동합니다.")
    history.push("/")
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    redirectDashboard()
  }, [])

  return <>
    <CRow>

    </CRow>
  </>
}

export default Station
