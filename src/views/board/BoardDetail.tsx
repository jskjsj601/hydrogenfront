import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import { useTypedSelector } from '../../store'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CButton,
  CCol,
  CRow,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect
} from '@coreui/react-pro'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import Editor from '../common/Editor'


const BoardDetail = (): JSX.Element => {
  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))
  const [isLoading, setIsLoading] = useState(false)
  const { id } = useParams<{ id: string }>()
  const [data, setData] = useState("")
  const [title, setTitle] = useState()
  const [priority, setPriority] = useState(1)
  const [errorTitle, setErrorTitle] = useState(false)
  const [errorPriority, setErrorPriority] = useState(false)
  const [status, setStatus] = useState("USE")

  const type = (id === "0") ? "create" : "update"
  const pageTitle = (id === "0") ? "등록" : "수정"


  const onChangeTitle = function (event) {
    setTitle(event.target.value)
    if (!event.target.value) {
      setErrorTitle(true)
    } else {
      setErrorTitle(false)
    }
  }

  const onChangePriority = function (event) {
    setPriority(event.target.value)
    if (!event.target.value) {
      setErrorPriority(true)
    } else {
      setErrorPriority(false)
    }
  }

  const getData = function (nId) {
    setIsLoading(true)
    service.request.get(`/admin/board/${nId}`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      if (data) {
        setTitle(data.title)
        setPriority(data.priority)
        setData(data.description || '')
      }
    })
  }

  const toBackPage = function () {
    window.history.go(-1)
  }

  const update = function () {
    if (errorTitle || errorPriority) {
      alert("입력 파라메터를 확인하여 주시기 바랍니다.")
      return
    }

    if (data === '') {
      alert("공지사항 내용을 압력하여 주시기 바랍니다.")
      return
    }

    if (window.confirm("공지사항을 수정하시겠습니까?")) {
      const params = {
        status: status,
        title: title,
        description: data,
        priority: priority,
        adminId: userInfo.id
      }
      setIsLoading(true)
      service.request.put(`/admin/board/${id}`, params).then(function () {
        setIsLoading(false)
        alert("공지사항이 수정되었습니다.")
        toBackPage()
      })
    }

  }

  const create = function () {
    if (errorTitle || errorPriority) {
      alert("입력 파라메터를 확인하여 주시기 바랍니다.")
      return
    }

    if (data === '') {
      alert("공지사항 내용을 입력하여 주시기 바랍니다.")
      return
    }

    if (window.confirm("공지사항을 입력하시겠습니까?")) {
      const params = {
        status: status,
        title: title,
        description: data,
        priority: priority,
        adminId: userInfo.id
      }
      setIsLoading(true)
      service.request.post('/admin/board', params).then(function () {
        setIsLoading(false)
        alert("공지사항이 입력되었습니다.")
        toBackPage()
      })
    }
  }
  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    if (type === 'update') {
      getData(id)
    }
  }, [])
  return <>
    <CRow>
      <LoadingOverlay
        active={isLoading}
        spinner
        text='로딩중 입니다....'
      >
        <CCol md={12}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>게시판 {pageTitle}</strong>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <div className="mb-3">
                    <CFormLabel>우선순위</CFormLabel>
                    <CFormInput
                      id="priority"
                      name="priority"
                      type="number"
                      value={priority || 1}
                      onChange={onChangePriority}
                      onBlur={onChangePriority}
                      placeholder="우선순위를 입력하여 주세요"
                      className={errorPriority ? 'form-control is-invalid' : 'form-control'}
                    />
                    {errorPriority ? (
                      <CFormFeedback invalid>우선순위는 필수값 입니다.</CFormFeedback>
                    ) : null}
                  </div>
                </CCol>
              </CRow>
              <CRow>
                <CCol md={8}>
                  <div className="mb-3">
                    <CFormLabel>제목</CFormLabel>
                    <CFormInput
                      id="title"
                      name="title"
                      type="text"
                      value={title || ''}
                      onChange={onChangeTitle}
                      onBlur={onChangeTitle}
                      placeholder="제목을 입력하여 주세요"
                      className={errorTitle ? 'form-control is-invalid' : 'form-control'}
                    />
                    {errorTitle ? (
                      <CFormFeedback invalid>제목은 필수값 입니다.</CFormFeedback>
                    ) : null}
                  </div>
                </CCol>
                <CCol md={4}>
                  <div className="mb-3">
                    <CFormLabel>상태</CFormLabel>
                    <CFormSelect
                      id="status"
                      name="status"
                      placeholder="상태"
                      onChange={(e) => setStatus(e.target.value)}
                      value={status}
                      className={'form-control'} >
                      <option value="USE">사용</option>
                      <option value="NOT_USE">사용 안함</option>
                    </CFormSelect>
                  </div>
                </CCol>
                <CCol md={12}>
                  <CFormLabel>내용</CFormLabel>
                  <Editor formData={data} onChange={setData} />
                </CCol>
              </CRow>
            </CCardBody>
            <CCardFooter>
              <CRow>
                <CCol md={12}>
                  <CButton color="secondary" onClick={() => toBackPage()} style={{ "marginRight": "10px" }}>
                    뒤로가기
                  </CButton>
                  {type === 'update' ?
                    (<CButton color="primary" onClick={() => update()}>수정하기</CButton>)
                    : (<CButton color="primary" onClick={() => create()}>추가하기</CButton>)}

                </CCol>
              </CRow>
            </CCardFooter>
          </CCard>
        </CCol>
      </LoadingOverlay>
    </CRow>
  </>
}

export default BoardDetail