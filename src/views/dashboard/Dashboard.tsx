import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import {
  Link
} from "react-router-dom"
import {
  CRow,
  CCol,
  CCard,
  CCardBody,
  CButton,
  CCardHeader,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CAlert,
  CFormCheck,
  CInputGroup,
  CFormSelect
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import {
  cilCloudy,
  cibAddthis,
  cilCheck,
  cilWarning,
  cilXCircle,
  cilArrowThickLeft,
  cilArrowThickRight
} from '@coreui/icons'
import styles from '../../modules/css/apply.module.css'
import { GoogleMaps } from '../../components/common'
import {
  HeaderGraph,
  StationBox
} from '../common'
import service from '../../modules/service'
import filters from '../../modules/filters'





const Dashboard = (): JSX.Element => {
  const [headerData, setHeaderData] = useState<any>([])
  const [middleDataStation, setMiddleDataStation] = useState<any>([])
  const [picMiddleData, setPicMiddleData] = useState({})
  const [noticeData, setNoticeData] = useState<any>([])
  const [boxData, setBoxData] = useState<any>([])
  const [center] = useState<any>(
    { lat: 36.3801686, lng: 127.3646184 }
  )
  const [showGraph, setShowGraph] = useState("0")
  const [activeIndex, setActiveIndex] = useState(0)
  const [searchStation, setSearchStation] = useState("station")
  const [locations, setLocations] = useState<any>([])
  const history = useHistory()

  const prevCard = function () {
    if (activeIndex === 0) {
      return
    }
    const subIndex = activeIndex - 1
    setActiveIndex(subIndex)
    setPicMiddleData(middleDataStation[subIndex])
  }

  const nextCard = function () {
    if ((middleDataStation.length - 1) === activeIndex) {
      return
    }
    const addIndex = activeIndex + 1
    setActiveIndex(addIndex)
    setPicMiddleData(middleDataStation[addIndex])
  }

  const toLicense = function () {
    history.push("/notice-management/notice")
  }

  const getHeader = function () {
    service.request.get("/admin/dashboard/sensorNumber", {}).then(function (response) {
      const data = response.data
      const stationInfo = {
        icon: cilCloudy,
        color: 'primary-gradient',
        title: "현재 총 충전소",
        value: data.payload.numberOfStations
      }
      const confirmInfo = {
        icon: cilCheck,
        color: 'info-gradient',
        title: "확인",
        value: data.payload.numberOfConfirm
      }
      const safetyInfo = {
        icon: cibAddthis,
        color: 'success-gradient',
        title: "안전",
        value: data.payload.numberOfSafety
      }
      const warningInfo = {
        icon: cilWarning,
        color: 'warning-gradient',
        title: "경고",
        value: data.payload.numberOfWarning
      }
      const dangerInfo = {
        icon: cilXCircle,
        color: 'danger-gradient',
        title: "위험",
        value: data.payload.numberOfDanger
      }
      const items = [stationInfo, confirmInfo, safetyInfo, warningInfo, dangerInfo]
      setHeaderData(items)
    })
  }

  const getMiddle = function () {
    service.request.get("/admin/dashboard/dataByStation", {}).then(function (response) {
      const stationData = response.data.payload.stationData
      if (stationData.length > 0) {
        setPicMiddleData(stationData[0])
        setActiveIndex(0)
      }
      setMiddleDataStation(stationData)
      const reportData = response.data.payload.recentNoticeData
      setNoticeData(reportData)
    })
  }

  const getBottom = function (segment) {
    if (segment === 'company') {
      segment = 'equipment'
    }
    service.request.get(`/admin/dashboard/dataBySegment/${segment}`).then(function (response) {
      const payload = response.data.payload
      setBoxData(payload)
      setLocations([])
      payload.forEach(item => {
        setLocations(locations => [...locations, {
          title: item.data.name,
          lat: Number(item.data.latitude ? item.data.latitude : item.data.station.latitude),
          lng: Number(item.data.longitude ? item.data.longitude : item.data.station.longitude)
        }])
      })
    })
  }

  const changeSegment = function (value) {
    setSearchStation(value)
    getBottom(value)
  }

  useEffect(() => {
    getHeader()
    getMiddle()
    getBottom('station')
  }, [])

  let middleData
  if (picMiddleData.data?.length === 0) {
    middleData = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    middleData = picMiddleData.data?.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell style={filters.status.getTypeColor(item.type)} className={styles.tableFont}>
          {filters.status.convertToSensorDataStatus(item.type)}
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.sensor.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.data}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{filters.date.lastModifiedDate(item.sensingDate)}</CTableDataCell>
      </CTableRow>
    })
  }

  let noticeItems
  if (noticeData.length === 0) {
    noticeItems = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    noticeItems = noticeData.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont}>
          <Link to={`/notice-management/notice/${item.id}?view=read`}>
            {item.title}
          </Link>
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.adminUser?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>
          {filters.date.lastModifiedDate(item.createDate, item.updateDate)}
        </CTableDataCell>
      </CTableRow>
    })
  }

  let boxItems
  if (boxData.length === 0) {
    boxItems =
      <CRow>
        <CCol>
          <CCard className="mb-4">
            <CCardHeader className={styles.cardHeader}>
              <strong className={styles.cardHeaderText}>데이터가 없습니다.</strong>
            </CCardHeader>
          </CCard>
        </CCol>
      </CRow>
  } else {
    boxItems = boxData.map((item, key) => {
      return <CCol key={key} xl={4} md={6}>
        <StationBox data={item} link={searchStation} />
      </CCol>
    })
  }

  return (
    <>
      <HeaderGraph dataList={headerData}></HeaderGraph>
      <CRow>
        <CCol md={6}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong style={{ lineHeight: "35px" }}>{picMiddleData?.station?.name}</strong>
              <div style={{ float: "right" }}>
                <CButton color="info" style={{ marginRight: "10px" }} onClick={() => prevCard()}>
                  <CIcon icon={cilArrowThickLeft} style={{ color: "#FFF" }} />
                </CButton>
                <CButton color="info" onClick={() => nextCard()}>
                  <CIcon icon={cilArrowThickRight} style={{ color: "#FFF" }} />
                </CButton>
              </div>
            </CCardHeader>
            <CCardBody style={{ height: "315px" }}>
              <CTable align="middle" className="mb-0" hover responsive bordered>
                <CTableHead color="light">
                  <CTableRow>
                    <CTableHeaderCell className={styles.tableFont} scope="col">상태</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">센서명</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">수치값</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">발생일자</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {middleData}
                </CTableBody>
              </CTable>
              <CRow>
                <CCol style={{ marginTop: "10px" }}>
                  <CAlert color="secondary" style={{ marginBottom: "0px", paddingTop: "10px", paddingBottom: "10px", textAlign: "center" }}>
                    <span>담당자: {picMiddleData.firstUser?.name || '담당자 없음'},  휴대전화 : {picMiddleData.firstUser?.mobileNumber || '담당 전화번호 없음'} </span>
                  </CAlert>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>

        <CCol md={6}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong style={{ lineHeight: "35px" }}>  공지사항</strong>
              <CButton color="link" style={{ float: "right" }} onClick={() => toLicense()}>
                더보기
              </CButton>
            </CCardHeader>
            <CCardBody style={{ height: "315px" }}>
              <CTable align="middle" className="mb-0" hover responsive bordered>
                <CTableHead color="light">
                  <CTableRow>
                    <CTableHeaderCell className={styles.tableFont} scope="col">제목</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">작성자</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">최종 수정일자</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {noticeItems}
                </CTableBody>
              </CTable>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol>
          <CCard className="mb-4">
            <CCardBody>
              <CRow>
                <CCol md={4}>
                  <CInputGroup className="mb-3">
                    <CFormSelect
                      value={searchStation}
                      name="searchStation"
                      onChange={e => changeSegment(e.target.value)}>
                      <option value="station">충전소&시설별</option>
                      <option value="company">기관별</option>
                      <option value="sensor">센서별</option>
                    </CFormSelect>
                  </CInputGroup>

                </CCol>
                <CCol>
                  <div style={{ float: "right" }}>
                    <CFormCheck
                      button={{ color: 'secondary', variant: 'outline' }}
                      type="radio"
                      name="showGraph"
                      id="success-outlined"
                      autoComplete="off"
                      label="그래프로 보기"
                      onChange={() => setShowGraph("0")}
                      checked={showGraph === "0"} />&nbsp; &nbsp;

                    <CFormCheck
                      button={{ color: 'secondary', variant: 'outline' }}
                      type="radio"
                      name="options-outlined"
                      id="danger-outlined"
                      autoComplete="off"
                      label="지도로 보기"
                      onChange={() => setShowGraph("1")}
                      checked={showGraph === "1"} />
                  </div>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>

      {showGraph === "0" && (
        <CRow>
          {boxItems}
        </CRow>
      )}

      {showGraph === "1" && (<CRow>
        <CCol>
          <CCard className="mb-4">
            <CCardBody>
              <GoogleMaps center={center || { lat: 36.3801686, lng: 127.3646184 }} locations={locations || []} height={"1200px"} zoom={8} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>)}
    </>
  )
}

export default Dashboard
