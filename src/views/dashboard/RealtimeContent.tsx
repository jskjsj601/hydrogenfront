import React, { useState, useEffect } from 'react'
import {
  CRow,
  CCol,
  CCard,
  CCardHeader,
  CCardBody,
  CAlert
} from '@coreui/react-pro'
import SensorDoughnutChart from '../common/SensorDoughnutChart'
import station from '../../../src/assets/chart/station.png'
import sensor from '../../../src/assets/chart/sensor.png'
import server from '../../../src/assets/chart/server.png'
import router from '../../../src/assets/chart/router.png'
import moment from 'moment'
import "./border.css"
import LoadingOverlay from 'react-loading-overlay-ts'
import service from '../../modules/service'
import {
  DisplayText,
  StationStatusBox,
  DisconnectBox
} from "../../components/elements"
import {
  SensorRealTimeBox
} from '../common'

const RealtimeContent = (): JSX.Element => {
  const [time, setTime] = useState(moment(new Date()).format("HH:mm:ss").toString())
  const today = moment(new Date()).format("YYYY-MM-DD").toString()
  const weekDay = moment().isoWeekday()
  const [stationData, setStationData] = useState<any>([])
  const [sensorData, setSensorData] = useState<any>([])
  const [routerData, setRouterData] = useState<any>([])
  const [serverData, setServerData] = useState<any>([])
  const [oneData, setOneData] = useState<any>([])
  const [twoData, setTwoData] = useState<any>([])
  const [realTimeData, setRealTimeData] = useState<any>([])
  const [dangerData, setDangerData] = useState<any>([])
  const [disConnectdData, setDisconnectedData] = useState<any>([])
  const weekDays = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingData, setLoadingData] = useState(false)
  const [isLoadingRealData, setLoadingRealData] = useState(false)
  const [isLoadingDangerData, setLoadingDangerData] = useState(false)
  const [isAddClass, setClass] = useState(false)
  const [isLoadingTwoData, setLoadingTwoData] = useState(false)
  const [isLoadingDisconnected, setLoadingDisconnected] = useState(false)

  const convertTime = (time) => {
    const hour = Number(time.split(":")[0])
    if (hour > 12) {
      return "오후"
    }
    return "오전"
  }
  const convertDateTime = (time) => {
    let hour = Number(time.split(":")[0])
    if (hour > 12) {
      hour = hour - 12
    }
    return hour + ":" + time.split(":")[1] + ":" + time.split(":")[2]
  }

  const getHeaderData = () => {
    setLoadingChart(true)
    const url = `/admin/dashboard/realtime/graphData`
    service.request.get(url, {}).then(function (response) {
      setLoadingChart(false)
      const data = response.data
      setStationData(data?.payload?.stationData)
      setSensorData(data?.payload?.sensorData)
      setRouterData(data?.payload?.routerData)
      setServerData(data?.payload?.serverData)
    })
  }

  const getStasticsData = () => {
    setLoadingData(true)
    const url = `/admin/dashboard/realtime/oneData`
    service.request.get(url, {}).then(function (response) {
      setLoadingData(false)
      const data = response.data
      setOneData(data?.payload)

    })
  }

  const getTwoData = () => {
    setLoadingTwoData(true)
    const url = `/admin/dashboard/realtime/twoData`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setTwoData(data?.payload)
      setLoadingTwoData(false)
    })
  }

  const getThreeData = () => {
    setLoadingRealData(true)
    const url = `/admin/dashboard/realtime/threeData`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setRealTimeData(data?.payload)
      setLoadingRealData(false)
    })
  }

  const getFourData = () => {
    setLoadingDangerData(true)
    const url = `/admin/dashboard/realtime/fourData`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setDangerData(data?.payload)
      if (data?.payload?.length > 0) {
        setClass(true)
      }
      setLoadingDangerData(false)
    })
  }

  const getDisconnectData = () => {
    setLoadingDisconnected(true)
    const url = `/admin/dashboard/realtime/notConnect`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setDisconnectedData(data?.payload)
      setLoadingDisconnected(false)
    })
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    const interval = setInterval(() => {
      const deg = 6
      const hr:any = window.document.querySelector('#hr')
      const mn :any= window.document.querySelector('#mn')
      const sc:any = window.document.querySelector('#sc')

      const day = new Date()
      const hh = day.getHours() * 30
      const mm = day.getMinutes() * deg
      const ss = day.getSeconds() * deg

      hr.style.transform = `rotateZ(${(hh) + (mm / 12)}deg)`
      mn.style.transform = `rotateZ(${mm}deg)`
      sc.style.transform = `rotateZ(${ss}deg)`
    }, 1000)
    return () => clearInterval(interval)
  }, [])

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getHeaderData()
    getStasticsData()
    getThreeData()
    getFourData()
    getTwoData()
    getDisconnectData()
  }, [])
  return <>
    <CRow>
      <CCol md={9}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong >전체 그래프</strong>
          </CCardHeader>
          <CCardBody>
            <LoadingOverlay
              active={isLoadingChart}
              spinner
              text='로딩중 입니다....'
            >
              <CRow>
                <CCol className={`mx-auto`} style={{ padding: "10px" }}>
                  <SensorDoughnutChart inputData={stationData || []} displayData={{
                    title: "충전소&시설",
                    img: station
                  }} />
                </CCol>
                <CCol style={{ padding: "10px" }}>
                  <SensorDoughnutChart inputData={sensorData || []} displayData={{
                    title: "센서",
                    img: sensor
                  }} />
                </CCol>
                <CCol style={{ padding: "10px" }}>
                  <SensorDoughnutChart labels={["접속", "연결 불량", "에러"]} inputData={routerData || []} displayData={{
                    title: "중계기",
                    img: router
                  }} />
                </CCol>
                <CCol style={{ padding: "10px" }}>
                  <SensorDoughnutChart labels={["정상"]} inputData={serverData || []} displayData={{
                    title: "모니터링 서버",
                    img: server
                  }} />
                </CCol>

              </CRow>
            </LoadingOverlay>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={3}>
        <CCard className={`mb-4 clock-back`} style={{ height: "272px" }}>
          <CCardHeader>
            <strong>현재 시간</strong>
          </CCardHeader>
          <CCardBody style={{ textAlign: "center" }}>
            <CRow>
              <CCol>
                <div className="clock">
                  <div className="hour">
                    <div id="hr" className="hr"></div>
                  </div>
                  <div className="min">
                    <div id="mn" className="mn"></div>
                  </div>
                  <div className="sec">
                    <div id="sc" className="sc"></div>
                  </div>
                </div>
              </CCol>
            </CRow>
            <CRow>
              <CCol style={{ marginTop: "20px" }}>
                <span style={{ fontWeight: "bold" }}>{today}</span>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <CCol md={3} style={{ height: "65vh" }}>
        <LoadingOverlay
          active={isLoadingData}
          spinner
          text='로딩중 입니다....'
        >
          <CCard className="mb-4" style={{ height: "65vh" }}>
            <CCardHeader>
              <strong>현황</strong> <span style={{ fontSize: "12px" }}>(최근 24시간)</span>
            </CCardHeader>
            <CCardBody style={{ overflow: "auto" }}>
              <CRow>
                <CCol>
                  <DisplayText left={"수신된 총 센서 데이터"} right={`${oneData[0] || 0}회`} />
                  <DisplayText left={"정상"} right={`${oneData[1] || 0}회`} />
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <CCard className="mb-4">
                    <CCardHeader style={{ fontSize: "15px" }}>
                      이상
                    </CCardHeader>
                    <CCardBody>
                      <CRow>
                        <CCol>
                          <span style={{ fontSize: "14px" }}>경고</span>
                        </CCol>
                        <CCol>
                          <span style={{ float: "right", fontWeight: "bold", fontSize: "14px" }}>{oneData[2] || 0}회</span>
                        </CCol>
                      </CRow>
                      <CRow>
                        <CCol>
                          <span style={{ fontSize: "14px" }}>위험</span>
                        </CCol>
                        <CCol>
                          <span style={{ float: "right", fontWeight: "bold", fontSize: "14px" }}>{oneData[3] || 0}회</span>
                        </CCol>
                      </CRow>
                      <CRow>
                        <CCol>
                          <span style={{ fontSize: "14px" }}>센서 통신 이상</span>
                        </CCol>
                        <CCol>
                          <span style={{ float: "right", fontWeight: "bold", fontSize: "14px" }}>{oneData[4] || 0}회</span>
                        </CCol>
                      </CRow>
                      <CRow>
                        <CCol>
                          <span style={{ fontSize: "14px" }}>중계기 접속 불량</span>
                        </CCol>
                        <CCol>
                          <span style={{ float: "right", fontWeight: "bold", fontSize: "14px" }}>{oneData[5] || 0}회</span>
                        </CCol>
                      </CRow>
                    </CCardBody>
                  </CCard>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <DisplayText left={"서버 이상"} right={`${oneData[6] || 0}분`} />
                  <DisplayText left={"이상 충전소&시설"} right={`${oneData[7] || 0}분`} />

                </CCol>
              </CRow>
            </CCardBody>

          </CCard>
        </LoadingOverlay>
      </CCol>
      <CCol md={3} style={{ height: "65vh" }}>
        <CCard className="mb-4" style={{ height: "65vh" }}>
          <CCardHeader>
            <strong>충전소&시설 상태</strong> <span style={{ fontSize: "12px" }}>이상상태 + 데이터 수신 시간 기준 20개 충전소&시설</span>
          </CCardHeader>
          <CCardBody style={{ overflow: "auto" }}>
            <LoadingOverlay
              active={isLoadingTwoData}
              spinner
              text='로딩중 입니다....'
            >
              <CRow>
                <CCol>
                  {twoData.length === 0 &&
                    (<CAlert color="primary">
                      데이터 없음
                    </CAlert>)
                  }
                  {twoData.map((item, key) => {
                    return <StationStatusBox data={item} key={key} />
                  })}
                </CCol>
              </CRow>
            </LoadingOverlay>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={3} style={{ height: "65vh" }}>
        <CCard className="mb-4" style={{ height: "65vh" }}>
          <CCardHeader>
            <strong>실시간 데이터</strong> <span style={{ fontSize: "12px" }}>최근 데이터 20개</span>
          </CCardHeader>
          <CCardBody style={{ overflow: "auto" }}>
            <CRow>
              <CCol>
                <LoadingOverlay
                  active={isLoadingRealData}
                  spinner
                  text='로딩중 입니다....'
                >
                  <SensorRealTimeBox items={realTimeData || []} />
                </LoadingOverlay>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={3} style={{ height: "65vh" }}>
        <CRow>
          <CCol>
            <CCard className={isAddClass ? "alerts-border":""} style={{ height: "30vh" }}>
              <CCardHeader>
                <strong className={isAddClass ? "alert-text":""} >이상 데이터</strong> <span style={{ fontSize: "12px" }}>데이터 수신 시간 기준 정렬</span>
              </CCardHeader>
              <CCardBody style={{ overflow: "auto" }}>
                <CRow>
                  <CCol>
                    <LoadingOverlay
                      active={isLoadingDangerData}
                      spinner
                      text='로딩중 입니다....'
                    >
                      <SensorRealTimeBox items={dangerData || []} />
                    </LoadingOverlay>
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CRow>
          <CCol>
            <CCard style={{ height: "33vh", marginTop: "20px" }}>
              <CCardHeader>
                <strong>연결 불량 중계기 및 센서</strong>
              </CCardHeader>
              <CCardBody style={{ overflow: "auto" }}>
                <LoadingOverlay
                  active={isLoadingDisconnected}
                  spinner
                  text='로딩중 입니다....'>
                  {disConnectdData.map((item, key) => {
                    return <CRow key={key}>
                      <CCol> <DisconnectBox data={item} /></CCol>
                    </CRow>
                  })}
                </LoadingOverlay>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CCol>
    </CRow>
  </>
}

export default RealtimeContent
