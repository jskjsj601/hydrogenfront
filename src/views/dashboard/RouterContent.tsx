import React, { useState, useEffect } from 'react'
import {
  CRow,
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CCardFooter
} from '@coreui/react-pro'
import {
  PingBox
} from '../common'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from '../../modules/service'
import filters from '../../modules/filters'
import styles from '../../modules/css/apply.module.css'
import Pagination from '../../components/Pagination'
const RouterContent = (): JSX.Element => {
  const [routerData, setRouterData] = useState<any>([])
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoading, setIsLoading] = useState(false)
  const [page, setPage] = useState(1)

  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getListData(currentPage)
  }

  const getDateData = async function  () {
    const url = `/admin/router/ping/data`
    await service.request.get(url, {}).then(function (response) {
      const data = response.data
      setRouterData(data?.payload)

    })
  }

  const getListData = function (page) {
    const url = `/admin/router/ping/list`
    service.request.get(url, { page: page, max: max }).then(function (response) {
      const data = response.data.payload
      setList(data.content)
      setCount(data.totalElements)
      setIsLoading(false)
    })
  }

  useEffect(() => {
    getDateData()
    getListData(1)
    const interval = setInterval(() => {
      getDateData()
    }, 60000)
    return () => clearInterval(interval)
  }, [])

  const isEmpty = (count === 0)
  let items
  if (isEmpty) {
    items = <CTableRow><CTableDataCell colSpan={30} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    items = list.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont} scope="row">{item.id}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.router?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.router?.uuid}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.router?.station?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.router?.address1}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.createDate, item.updateDate)}</CTableDataCell>
      </CTableRow >
    })
  }

  return <>
    <CRow>
      <CRow>
        <CCol>
          <CCard className="mb-4">
            <CCardHeader>
              <strong style={{ lineHeight: "35px" }}>중계기 접속 현황</strong>
            </CCardHeader>
            <CCardBody>
              <PingBox items={routerData || []} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol>
          <CCard className="mb-4">
            <CCardHeader>
              <strong style={{ lineHeight: "35px" }}>중계기 Ping 데이터</strong>
            </CCardHeader>
            <CCardBody>

              <CRow>
                <LoadingOverlay
                  active={isLoading}
                  spinner
                  text='로딩중 입니다....'
                >
                  <CCol md={12}>
                    <CTable align="middle" className="mb-0 border" hover responsive bordered>
                      <CTableHead color="light">
                        <CTableRow>
                          <CTableHeaderCell className={styles.tableFont} scope="col">ID</CTableHeaderCell>
                          <CTableHeaderCell className={styles.tableFont} scope="col">중계기명</CTableHeaderCell>
                          <CTableHeaderCell className={styles.tableFont} scope="col">UUID</CTableHeaderCell>
                          <CTableHeaderCell className={styles.tableFont} scope="col">설치 충전소</CTableHeaderCell>
                          <CTableHeaderCell className={styles.tableFont} scope="col">설치 주소</CTableHeaderCell>
                          <CTableHeaderCell className={styles.tableFont} scope="col">핑 타임</CTableHeaderCell>
                        </CTableRow>
                      </CTableHead>
                      <CTableBody>
                        {items}
                      </CTableBody>
                    </CTable>
                  </CCol>
                </LoadingOverlay>
              </CRow>

            </CCardBody>
            <CCardFooter>
              <CRow>
                <CCol xs={5}>
                  ( <strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
                </CCol>
                <CCol md={7} >
                  <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} viewType={'full'} />
                </CCol>
              </CRow>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    </CRow>
  </>
}

export default RouterContent
