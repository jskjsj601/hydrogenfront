import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import {
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CRow,
} from '@coreui/react-pro'
import {
  DetailHeaderBox,
  SensorHeader,
  SensorLineChart,
  SensorFooter,
  SensorDataList,
  StationBox
} from '../common'
import service from '../../modules/service'
import LoadingOverlay from 'react-loading-overlay-ts'
import styles from '../../modules/css/apply.module.css'

const Station = (): JSX.Element => {
  const [isLoadingBox, setLoadingBox] = useState(false)
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingList, setLoadingList] = useState(false)
  const [header, setHeader] = useState({})
  const [chartData, setChartData] = useState<any>([])
  const [countData, setCountData] = useState<any>([])
  const [sensors, setSensorList] = useState<any>([])
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const sensorHeaders = ['상태', '센서명', '수치값', '빌생일자']
  const [page, setPage] = useState(1)
  const { id } = useParams<{ id: string }>()

  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getSensor(currentPage, max)
  }

  const getHeader = function () {
    setLoadingBox(false)
    const url = `/admin/sensorControlByStationDetail/${id}`
    service.request.get(url, {}).then(function (response) {
      setLoadingBox(false)
      const header = response.data.payload
      const list = response.data.payload.sensorList
      setSensorList(list)
      setHeader(header)
    })
  }
  const getDateData = function (type) {
    setLoadingChart(true)
    const url = `/admin/sensorControlBySensorDetail/dataByTimeUnit/${id}/${type}`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setChartData(data.payload.timeUnitDateModels)
      const confirmInfo = {
        label: "금일 확인 숫자",
        color: 'info-gradient',
        count: data.payload.countToConfirmInToday
      }

      const normalInfo = {
        label: "금일 안전 숫자",
        color: 'success-gradient',
        count: data.payload.countToNormalInToday
      }

      const warningInfo = {
        label: "금일 위험 숫자",
        color: 'warning-gradient',
        count: data.payload.countToWarningInToday
      }

      const dangerInfo = {
        label: "금일 위험 숫자",
        color: 'danger-gradient',
        count: data.payload.countToDangerInToday
      }

      const items = [confirmInfo, normalInfo, warningInfo, dangerInfo]
      setCountData(items)
      setLoadingChart(false)
    })
  }

  const getSensor = function (page, max) {
    const params = {
      page: page,
      max: max
    }
    setLoadingList(true)
    service.request.get(`/admin/sensorControlByStationDetail/sensorData/${id}`, params).then(function (response) {
      const sensorData = response.data.payload.content
      const count = response.data.payload.totalElements
      setList(sensorData)
      setCount(count)
      setLoadingList(false)
    })
  }

  const onChangeTimeUnit = function (type) {
    setTimeUnit(type)
    getDateData(type)
  }

  let boxItems
  if (sensors.length === 0) {
    boxItems =
      <CCol>
        <CCard className="mb-4">
          <CCardHeader className={styles.cardHeader}>
            <strong className={styles.cardHeaderText}>데이터가 없습니다.</strong>
          </CCardHeader>
        </CCard>
      </CCol>
  } else {
    boxItems = sensors.map((item, key) => {
      return <CCol key={key} md={3}>
        <StationBox data={item} link={'sensor'} type={'sensor'} />
      </CCol>
    })
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getHeader()
    getDateData('minute')
    getSensor(page, max)
  }, [])
  return <>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <CRow>
          <DetailHeaderBox data={header} type={'station'} />
        </CRow>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <CCol style={{ marginTop: "30px" }}>
        <CCard className="mb-4">
          <LoadingOverlay
            active={isLoadingChart}
            spinner
            text='로딩중 입니다....'
          >
            <CCardBody>
              <SensorHeader data={chooseTimeUnit} onChangeTimeUnit={onChangeTimeUnit} />
              <SensorLineChart data={chartData} />
            </CCardBody>
            <CCardFooter>
              <SensorFooter data={countData} />
            </CCardFooter>
          </LoadingOverlay>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <CRow>
          {boxItems}
        </CRow>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingList}
        spinner
        text='로딩중 입니다....'
      >
        <SensorDataList
          headers={sensorHeaders}
          data={list}
          count={count}
          page={page}
          max={max}
          onChangePage={onChangePage} />
      </LoadingOverlay>
    </CRow>
  </>
}

export default Station
