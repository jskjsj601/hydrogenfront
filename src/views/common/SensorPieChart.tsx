import React, { useState, useEffect } from 'react'
import { Pie } from "react-chartjs-2"
import {
  CRow,
  CCol
} from '@coreui/react-pro'
import {
  Chart,
  ArcElement,
} from 'chart.js'
const options = {
  // responsive 속성을 false로 지정한다.
  plugins: {
    legend: { display: true, position: 'right' },
  },
}

Chart.register(ArcElement)
const SensorPieChart = ({ inputData }): JSX.Element => {
  const [chartData, setChartData] = useState({
    labels: ['안전', '위험', '경고'],
    datasets: [{
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        '#15af26',
        '#F79D10',
        '#DC3D3D',
        '#FF368D',
        '#B1B7C1',
      ],
      borderColor: [
        '#15af26',
        '#F79D10',
        '#DC3D3D',
        '#FF368D',
        '#B1B7C1',
      ],
      borderWidth: 1,
    },
    ],
  })

  useEffect(() => {
    const changeData = {
      labels: ['안전', '위험', '경고'],
      datasets: [{
        data: inputData,
        backgroundColor: [
          '#15af26',
          '#F79D10',
          '#DC3D3D',
          '#FF368D',
          '#B1B7C1',
        ],
        borderColor: [
          '#15af26',
          '#F79D10',
          '#DC3D3D',
          '#FF368D',
          '#B1B7C1',
        ],
        borderWidth: 1,
      },],
    }
    setChartData(changeData)
  }, [inputData])

  return <>
    <CRow>
      <CCol >
        <Pie
          style={{ width: "300px", height: "300px" }}
          data={chartData}
          options={options} />
      </CCol>
    </CRow>
  </>
}

export default SensorPieChart