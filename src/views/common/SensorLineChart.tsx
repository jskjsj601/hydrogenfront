import React, { useState, useEffect } from 'react'
import { Line } from "react-chartjs-2"
import {
  CRow
} from '@coreui/react-pro'
import {
  Chart,
  registerables
} from 'chart.js'
import { getStyle } from '@coreui/utils'

Chart.register(...registerables)
const options = {
  maintainAspectRatio: false,
  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    y: {
      ticks: {
        beginAtZero: true,
        maxTicksLimit: 5,
        stepSize: Math.ceil(400 / 5),
        max: 400,
      },
    },
    x: {
      grid: {
        drawOnChartArea: false,
      },
    }
  },
  elements: {
    line: {
      tension: 0.4,
    },
    point: {
      radius: 2,
      hitRadius: 2,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
}

const SensorLineChart = ({ data, height = "400" }, type = "all"): JSX.Element => {
  const [labels, setLabels] = useState<any>([])
  const [normal, setNormal] = useState<any>([])
  const [warning, setWarning] = useState<any>([])
  const [danger, setDanger] = useState<any>([])

  useEffect(() => {
    const label = data.map(function (item) { return item.label })
    const normal = data.map(function (item) { return item.countToNormal })
    const warning = data.map(function (item) { return item.countToWarning })
    const danger = data.map(function (item) { return item.countToDanger })

    setLabels(label)
    setNormal(normal)
    setWarning(warning)
    setDanger(danger)
  }, [data])
  return <>
    <CRow>
      <Line
        style={{ height: `${height}px` }}
        data={{
          labels: labels,
          datasets: [
            {
              label: '안전 숫자',
              backgroundColor: 'transparent',
              borderColor: getStyle('--cui-success'),
              pointHoverBackgroundColor: getStyle('--cui-success'),
              borderWidth: 2,
              data: normal
            },
            {
              label: '경고 숫자',
              backgroundColor: 'transparent',
              borderColor: getStyle('--cui-warning'),
              pointHoverBackgroundColor: getStyle('--cui-warning'),
              borderWidth: 2,
              data: warning,
            },
            {
              label: '장애 숫자',
              backgroundColor: 'transparent',
              borderColor: getStyle('--cui-danger'),
              pointHoverBackgroundColor: getStyle('--cui-danger'),
              borderWidth: 2,
              data: danger,
            }
          ],
        }}
        options={options} />
    </CRow>
  </>
}

export default SensorLineChart
