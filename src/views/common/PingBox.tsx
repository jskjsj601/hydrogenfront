import React from 'react'
import {
  CAlert,
  CCallout,
  CCol,
  CImage,
  CRow,
  CButton
} from "@coreui/react-pro"
import error from "../../assets/icon/error.png"
import connected from '../../assets/icon/connected.png'
import disconnectd from '../../assets/icon/disconnect.png'
import {
  PingChart,
} from '.'
import '../../components/elements/real-box.css'
const PingBox = ({ items }): JSX.Element => {
  return (
    <>
      {items?.length === 0 ?
        (<CCol>
          <CAlert color="primary">
            데이터 없음
          </CAlert>
        </CCol>) :
        (<>
          {items.map((item, index) => {
            return (
              <CRow key={index}>
                <CCol >
                  <CCallout style={{ height: "250px" }}>
                    <CRow className="align-items-center">
                      <CCol>
                        <span className="real-common-text">
                          {item.name} <br />
                        </span>
                      </CCol>
                    </CRow>
                    <CRow className="align-items-center">
                      {item?.status ?
                        (<CCol md={2}>
                          {item.status === 'USE' && (<CImage width={100} src={connected} alt="" />)}
                          {item.status === 'ERROR' && (<CImage width={100} src={disconnectd} alt="" />)}
                          {(item.status === 'REPAIR' || item.status === 'NOT_USE' || item.status === 'STAND_BY' || item.status === 'REPAIRING') && (<CImage width={100} src={error} alt="" />)}

                        </CCol>) :
                        (<CCol md={2}></CCol>)}
                      <CCol style={{ float: "right" }}>
                        <PingChart data={item?.pingData} height={"200"} />
                      </CCol>
                    </CRow>
                  </CCallout>
                </CCol>
              </CRow>
            )
          })}
        </>)
      }
    </>
  )
}

export default PingBox

