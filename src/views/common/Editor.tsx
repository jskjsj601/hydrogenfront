import React from 'react'
import './app.css'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import service from "../../modules/service"

const Editor = ({ formData, onChange }): JSX.Element => {
  const uploadAdapter = function (loader) {
    return {
      upload: () => {
        return new Promise((resolve, reject) => {
          const formData = new FormData()
          loader.file.then((file) => {
            
            formData.append("file", file)
            service.request.upload_post("/admin/file/upload", formData).then((response) => {
              const filePath = process.env.REACT_APP_SERVER_URL + "/admin/file/download?path=" + response.data.payload
              resolve({
                default: filePath
              })
            }).catch((err) => {
              reject(err)
            })
          })
        })
      }
    }
  }

  const uploadPlugin = function (editor) {
    editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
      return uploadAdapter(loader)
    }
  }

  return <>
    <CKEditor
      editor={ClassicEditor}
      data={formData}
      onReady={_ => {
        console.log("ready for CHEditor")
      }}
      onChange={(_, editor) => {
        const data = editor.getData()
        onChange(data)
      }}
      config={{
        extraPlugins: [uploadPlugin]
      }}
    />
  </>
}

export default Editor
