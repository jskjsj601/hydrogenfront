import React, { useState, useEffect } from 'react'
import filters from '../../modules/filters'
import {
  CRow,
  CCol,
  CButton,
  CButtonGroup
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilReload } from '@coreui/icons'
const SensorHeader = ({ data, onChangeTimeUnit, title = undefined, execRenewal = undefined, lastRenewalDate = "" }): JSX.Element => {
  const [renewalDate, setRenewalDate] = useState(lastRenewalDate || filters.date.lastModifiedDate(new Date()))
  const [check, setCheck] = useState(false)
  const timeUnits = ['minute', 'hour', 'day', 'week', 'month']
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const changeTimeUnit = function (type) {
    onChangeTimeUnit(type, check)
  }
  const changeCheck = function(check) {
    setCheck(check)
    onChangeTimeUnit(chooseTimeUnit, check)

  }

  const doRenewal = function() {
    execRenewal()
  }

  useEffect(() => {
    setTimeUnit(data)
  }, [data])

  useEffect(() => {
    setRenewalDate(lastRenewalDate)
  }, [lastRenewalDate])
  return <>
    <CRow>

      {title === undefined ? (
          <CCol sm={5}>
            <h4 id="traffic" className="card-title mb-0">
              데이터 갱신일시 {renewalDate}
            </h4>
            <div className="small text-medium-emphasis" style={{marginTop: "20px"}}>
              <input type="checkbox" name="checkRenewal" checked={check} onChange={() => changeCheck(!check)} /> 1분마다 갱신  &nbsp;&nbsp;
              <CButton color="info" size={"sm"} style={{color: "#FFF"}} onClick={() => doRenewal()}>
                <CIcon icon={cilReload} size="sm"/> &nbsp; 새로고침
              </CButton>
            </div>
          </CCol>
        ) :
        (
          <CCol sm={5}>
            <h4 id="traffic" className="card-title mb-0">
              {title.station?.name  || '' }
            </h4>
            <div className="small text-medium-emphasis">
              {title.station?.address1} {title.station?.address2} /
              담당자 : {title.firstUser?.name} ({title.firstUser?.mobileNumber})
            </div>
          </CCol>
        )
      }
      <CCol sm={7} className="d-none d-md-block">
        <CButtonGroup className="float-end me-3">
          {timeUnits.map((value) => (
            <CButton
              color="outline-secondary"
              className="mx-0"
              key={filters.status.convertToTimeUnit(value)}
              active={value === chooseTimeUnit}
              onClick={() => changeTimeUnit(value)}
            >
              {filters.status.convertToTimeUnit(value)}
            </CButton>
          ))}
        </CButtonGroup>
      </CCol>
    </CRow>
  </>
}

export default SensorHeader
