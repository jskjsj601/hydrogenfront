import React from 'react'
import { useHistory } from "react-router-dom"
import {
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CImage,
} from '@coreui/react-pro'
import styles from '../../modules/css/apply.module.css'
import danger from '../../assets/icon/danger.png'
import safety from '../../assets/icon/safety.png'
import warning from '../../assets/icon/warning.png'
import filters from '../../modules/filters'

const StationBox = ({ data, link, type = 'normal' }): JSX.Element => {
  const history = useHistory()
  let item = data
  if (type === 'normal') {
    item = data?.data
  }
  const showLimitText = (txt) => {
    if (txt.length > 8) {
      return txt.substring(0, 12) + "..."
    }
    return txt
  }
  const toDetail = function () {
    if (link === 'station') {
      history.push(`/monitoring-management/station-new/${item.id}`)
    } else if (link === 'company') {
      history.push(`/monitoring-management/equipment-new/${item.id}`)
    } else {
      history.push(`/detail/sensor/${item.id}`)
    }

  }
  return (
    <>
      <CCol>
        <CCard className="mb-4" style={{ minWidth: "420px" }}>
          <CCardHeader className={styles.cardHeader} style={{ height: "82px" }}>
            <strong className={styles.cardHeaderText} onClick={() => toDetail()}>{item.name}</strong>
            {data.sensorData?.type === 'NORMAL' && (<CImage src={safety} alt="" style={{ float: "right" }} />)}
            {(data.sensorData?.type === 'LOW_WARNING' || data.sensorData?.type === 'HIGH_WARNING') && (<CImage src={warning} alt="" style={{ float: "right" }} />)}
            {(data.sensorData?.type === 'LOW_DANGER' || data.sensorData?.type === 'HIGH_DANGER') && (<CImage src={danger} alt="" style={{ float: "right" }} />)}

          </CCardHeader>
          <CCardBody style={{ height: "245px" }}>
            <div style={{ fontSize: "14px", fontWeight: "bold" }}>위치 : </div>
            <div style={{ fontSize: "14px", fontWeight: "bold" }}>{item.address1 ? item.address1 : item.station?.address1} {item.address2 ? item.address2 : item.station?.address2}</div>

            <div style={{ fontSize: "14px", fontWeight: "bold", marginTop: "10px" }}>관리자 : </div>
            <div style={{ fontSize: "14px", fontWeight: "bold" }}>{data.firstUser?.name || '등록 관리자 없음'}</div>

            <div style={{ fontSize: "14px", fontWeight: "bold", marginTop: "10px" }}>관리자 연락처 : </div>
            <div style={{ fontSize: "14px", fontWeight: "bold" }}>{data.firstUser?.mobileNumber || '등록 연락처 없음'}</div>

            <div style={{ color: "#464646", fontSize: "14px", fontWeight: "bold", marginTop: "10px" }}>
              최근 상태 : &nbsp;
              <span style={filters.status.getTypeColor(data.sensorData?.type)}>
                {filters.status.convertToSensorDataStatus(data.sensorData?.type)}
              </span>
            </div>
          </CCardBody>
        </CCard>
      </CCol>
    </>
  )
}
export default StationBox
