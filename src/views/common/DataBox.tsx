import React from 'react'
import {
  CAlert,
  CCallout,
  CCol,
  CImage,
  CRow,
  CButton
} from "@coreui/react-pro"
import warning from "../../assets/icon/warning.png"
import danger from "../../assets/icon/danger.png"
import safety from '../../assets/icon/safety.png'
import filters from "../../modules/filters"
import {
  SensorLineChart,
} from '../common'
import '../../components/elements/real-box.css'
import {
  Link
} from "react-router-dom"
const DataBox = ({ items, callback = null, type = 'station' }): JSX.Element => {
  const showSensorData = (e, sensorId) => {
    e.preventDefault()
    if (callback) {
      callback(sensorId)
    }
  }
  return (
    <>
      {items?.length === 0 ?
        (<CCol>
          <CAlert color="primary">
            데이터 없음
          </CAlert>
        </CCol>) :
        (<>
          {items.map((item, index) => {
            return (
              <CRow key={index}>
                <CCol>
                  <CCallout
                    color={filters.status.convertToSensorDataTypeToColor(item.lastSensorData?.type)}
                    className={`real-${filters.status.convertToSensorDataTypeToColor(item.lastSensorData?.type)}-box`}>
                    <CRow className="align-items-center">
                      <CCol>
                        {type === 'station' &&
                          <Link to={`station-new/${item.station?.id}`}>
                            <span className="real-common-text">
                              {item.station?.name} <br />
                            </span>
                          </Link>
                        }

                        {type === 'equipment' &&
                          <Link to={`/monitoring-management/station-new/${item.lastSensorData?.sensor?.station?.id}`}>A
                            <span className="real-common-text">
                              {item.lastSensorData?.sensor?.station?.name} <br />
                            </span>
                          </Link>
                        }

                        {type === 'sensor' &&
                          <Link to={`/monitoring-management/equipment-new/${item.lastSensorData?.sensor?.equipment?.id}`}>
                            <span className="real-common-text">
                              {item.lastSensorData?.sensor?.equipment?.name} <br />
                            </span>
                          </Link>
                        }
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol>
                        {item.lastSensorData ? (
                          <>
                            <span className="real-common-text">
                              {type === 'sensor' &&
                                <span className="real-common-text">
                                  <a href="#" onClick={(e) => showSensorData(e, item.lastSensorData?.sensor?.id)}>
                                    {item.lastSensorData?.sensor?.name || '센서명 없음'}
                                  </a>
                                </span>
                              }

                              {type === 'equipment' &&
                                <a href="#" onClick={(e) => showSensorData(e, item.lastSensorData?.sensor?.id)}>
                                  <span className="real-common-text">
                                    {item.lastSensorData?.sensor?.name || '센서명 없음'}
                                  </span>
                                </a>
                              }
                            </span>
                          </>) :
                          (<span className="real-common-text">데이터 없음</span>)
                        }
                      </CCol>
                    </CRow>
                    <CRow className="align-items-center">
                      {item.lastSensorData ?
                        (<CCol md={2}>
                          {item.lastSensorData?.type === 'NORMAL' && (<CImage src={safety} alt="" />)}
                          {(item.lastSensorData?.type === 'LOW_WARNING' || item.lastSensorData?.type === 'HIGH_WARNING') && (<CImage src={warning} alt="" />)}
                          {(item.lastSensorData?.type === 'LOW_DANGER' || item.lastSensorData?.type === 'HIGH_DANGER') && (<CImage src={danger} alt="" />)}

                        </CCol>) :
                        (<CCol md={2}></CCol>)}
                      <CCol md={7} style={{ float: "right" }}>
                        <SensorLineChart data={item.timeUnitDateModels} height={"120"} />
                      </CCol>
                      <CCol md={3}>
                        {item.lastSensorData &&
                          <CButton size="sm" color={filters.status.getReportColor(item.lastSensorData?.reportStatus)}>
                            {filters.status.convertToReportStatus(item.lastSensorData?.reportStatus)}
                          </CButton>
                        }
                      </CCol>
                    </CRow>
                  </CCallout>
                </CCol>
              </CRow>
            )
          })}
        </>)
      }
    </>
  )
}

export default DataBox

