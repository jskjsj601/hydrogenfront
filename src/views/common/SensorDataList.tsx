import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CTable,
  CTableHead,
  CTableHeaderCell,
  CTableDataCell,
  CTableRow,
  CTableBody,
} from '@coreui/react-pro'
import styles from '../../modules/css/apply.module.css'
import Pagination from '../../components/Pagination'
import filters from '../../modules/filters'

const SensorDataList = ({ headers, link = undefined, data, count, page, max, onChangePage, viewType = "compact" }): JSX.Element => {
  const items = data
  const showName = function (item) {
    if (link === 'station') {
      return item.sensor?.station?.name
    } else if (link === 'company') {
      return item.sensor?.equipment?.name
    } else if (link === 'sensor') {
      return item.sensor?.name
    }
  }

  const headerData = headers.map((item, key) => {
    return <CTableHeaderCell key={key} className={styles.tableFont} scope="col">{item}</CTableHeaderCell>
  })
  let sensorData
  if (items.length === 0) {
    sensorData = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    sensorData = items.map((item, key) => {
      return <CTableRow key={key}>
        {viewType === 'full' && <CTableDataCell className={styles.tableFont}>{item.id}</CTableDataCell>}
        <CTableDataCell className={styles.tableFont}>{filters.date.lastModifiedDate(item.sensingDate)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.data}</CTableDataCell>
        <CTableDataCell style={filters.status.getTypeColor(item.type)} className={styles.tableFont}>
          {filters.status.convertToSensorDataStatus(item.type)}
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{filters.status.convertToReportStatus(item.reportStatus)}</CTableDataCell>

      </CTableRow>
    })
  }
  return <>
    <CCol xs>
      <CCard className="mb-4">
        <CCardHeader>센서 정보 리스트</CCardHeader>
        <CCardBody>
          <CTable align="middle" className="mb-0 border" hover responsive bordered>
            <CTableHead color="light">
              <CTableRow>
                {headerData}
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {sensorData}
            </CTableBody>
          </CTable>
        </CCardBody>
        <CCardFooter>
          <CRow>
            <CCol md={7} >
              ( {viewType === 'full' && (<><strong>총</strong> : {count} 행, </>)}  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
            </CCol>
            <CCol md={5} >
              <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} viewType={viewType} />
            </CCol>
          </CRow>
        </CCardFooter>
      </CCard>
    </CCol>
  </>
}

export default SensorDataList
