import React from 'react'
import {
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CImage,
  CRow,
} from '@coreui/react-pro'
import styles from '../../modules/css/apply.module.css'
import danger from '../../assets/icon/danger.png'
import safety from '../../assets/icon/safety.png'
import warning from '../../assets/icon/warning.png'
import filters from '../../modules/filters'
const DetailHeaderBox = ({ data, type }): JSX.Element => {
  const item = data
  const showTypeName = function () {
    if (type === 'station') {
      return '충전소&시설'
    } else if (type === 'company') {
      return '기관'
    } else if (type === 'sensor') {
      return '센서'
    }
  }
  return <>
    <CCol>
      <CCard>
        <CCardHeader className={styles.cardHeader}>
          <strong className={styles.cardHeaderText}>{showTypeName()}명</strong>
        </CCardHeader>
        <CCardBody className={styles.cardBody}>
          <div style={{ fontSize: "14px", fontWeight: "bold" }}>{item?.name}</div>
        </CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
        <CCardHeader className={styles.cardHeader}>
          <strong className={styles.cardHeaderText}>
            최종 상태
            {item?.sensorData?.type === 'NORMAL' && (<CImage src={safety} alt="" style={{ float: "right" }} />)}
            {(item?.sensorData?.type === 'LOW_WARNING' || item?.sensorData?.type === 'HIGH_WARNING') && (<CImage src={warning} alt="" style={{ float: "right" }} />)}
            {(item?.sensorData?.type === 'LOW_DANGER' || item?.sensorData?.type === 'HIGH_DANGER') && (<CImage src={danger} alt="" style={{ float: "right" }} />)}
          </strong>
        </CCardHeader>
        <CCardBody className={styles.cardBody}>
          <div style={{ fontSize: "14px", fontWeight: "bold" }}>{item?.sensorData?.sensor?.name},
            ({item?.sensorData?.data}) <br /> 마지막 전송 일자 : {filters.date.lastModifiedDate(item?.sensorData?.sensingDate)}</div>
        </CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
        <CCardHeader className={styles.cardHeader}>
          <strong className={styles.cardHeaderText}>{showTypeName()} 주소</strong>
        </CCardHeader>
        <CCardBody className={styles.cardBody}>
          <div style={{ fontSize: "14px", fontWeight: "bold" }}>{item?.address1} {item?.address2}</div>
        </CCardBody>
      </CCard>
    </CCol>
    <CCol>
      <CCard>
        <CCardHeader className={styles.cardHeader}>
          <strong className={styles.cardHeaderText}>담당자 연락처</strong>
        </CCardHeader>
        <CCardBody className={styles.cardBody}>
          {item?.firstUser === null ? (<div style={{ fontSize: "14px", fontWeight: "bold" }}>등록된 담당자 없음</div>) :
            <div style={{ fontSize: "14px", fontWeight: "bold" }}>{item?.firstUser?.name} {item?.firstUser?.mobileNumber}</div>}
        </CCardBody>
      </CCard>
    </CCol>
  </>
}

export default DetailHeaderBox
