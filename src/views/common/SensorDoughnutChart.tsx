import React, { useState, useEffect } from 'react'
import { Doughnut } from "react-chartjs-2"
import {
  CRow,
  CCol,
  CImage,
  CBadge
} from '@coreui/react-pro'
import {
  Chart,
  ArcElement,
  Tooltip,
  Legend
} from 'chart.js'

import stationImage from '../../../src/assets/chart/station.png'
const options = {
  // responsive 속성을 false로 지정한다.
  plugins: {
    legend: { display: false, position: 'center' },
  },
  cutout: 80
}
Chart.register(ArcElement, Tooltip, Legend)
const SensorDoughnutChart = ({ labels = ['안전', '위험', '장애'], inputData, displayData, type = 'greph' }): JSX.Element => {
  const [chartData, setChartData] = useState({
    labels: labels,
    datasets: [{
      data: [12, 19, 3],
      backgroundColor: [
        '#15af26',
        '#F79D10',
        '#DC3D3D',
      ],
      borderColor: [
        '#15af26',
        '#F79D10',
        '#DC3D3D',
      ],
      borderWidth: 1,
    },],
  })
  const [sum, setSum] = useState(0)

  useEffect(() => {
    const sum = inputData.reduce((partialSum, a) => partialSum + a, 0)
    if (sum === 0) {
      inputData = [1, 0, 0]
    }
    const changeData = {
      labels: labels,
      datasets: [{
        data: inputData,
        backgroundColor: [
          '#15af26',
          '#F79D10',
          '#DC3D3D',
        ],
        borderColor: [
          '#15af26',
          '#F79D10',
          '#DC3D3D',
        ],
        borderWidth: 1,
      },],
    }

    setSum(sum)
    setChartData(changeData)
  }, [inputData])

  return <>
    <CRow>
      <CCol >
        <div style={{ width: '200px', height: '200px', position: 'relative', margin: "0 auto" }}>
          <Doughnut
            data={chartData}
            options={options}
            width={200} height={200} />

          <div style={{ position: 'absolute', width: '70%', top: '55px', left: 30, textAlign: 'center', marginTop: '-28px', lineHeight: '20px' }}>
            <CRow>
              <CCol>
                <CImage src={displayData?.img || stationImage} width={40} height={40} />
              </CCol>
            </CRow>
            <CRow>
              <CCol>
                <span style={{ fontWeight: "bold" }}>{displayData?.title || '제목없음'}</span>
              </CCol>
            </CRow>
            <CRow>
              <CCol md={4} style={{ fontSize: "10px" }}>전체 {sum || 0}</CCol>
              <CCol md={8} style={{ fontSize: "10px" }}>
                {inputData[0] > 0 &&
                  <>
                    <CBadge color="success">
                      &nbsp;
                    </CBadge>
                    &nbsp;
                    {labels[0]} {inputData[0] || 0}
                  </>
                }
              </CCol>
            </CRow>

            <CRow>
              <CCol md={4} style={{ fontSize: "10px" }}></CCol>
              <CCol md={8} style={{ fontSize: "10px" }}>
                {inputData[1] > 0 &&
                  <>
                    <CBadge color="warning">
                      &nbsp;
                    </CBadge>
                    &nbsp;
                    {labels[1]}  {inputData[1] || 0}
                  </>
                }
              </CCol>
            </CRow>
            <CRow>
              <CCol md={4} style={{ fontSize: "10px" }}></CCol>
              <CCol md={8} style={{ fontSize: "10px" }}>
                {inputData[2] > 0 &&
                  <>
                    <CBadge color="danger">
                      &nbsp;
                    </CBadge>
                    &nbsp;
                    {labels[2]} {inputData[2] || 0}
                  </>
                }
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
    </CRow>
  </>

}

export default SensorDoughnutChart