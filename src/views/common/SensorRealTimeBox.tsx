import React, { useState, useEffect } from 'react'
import {
  CCol,
  CAlert,
} from '@coreui/react-pro'
import {
  RealTimeBox
} from "../../components/elements"
import filters from "../../modules/filters"
const SensorRealTimeBox = ({ items }): JSX.Element => {
  return <>
    {items?.length === 0 ?
      (<CCol>
        <CAlert color="primary">
          데이터 없음
        </CAlert>
      </CCol>) :
      (<>
        {items.map((item, index) => {
          return (
            <CCol key={index}>
              <RealTimeBox data={item} className={filters.status.convertToSensorDataTypeToColor(item.type)} />
            </CCol>
          )
        })}
      </>)
    }
  </>
}

export default SensorRealTimeBox
