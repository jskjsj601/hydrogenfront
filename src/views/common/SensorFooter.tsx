import React from 'react'
import {
  CRow,
  CCol,
  CProgress
} from '@coreui/react-pro'
const SensorFooter = ({ data }): JSX.Element => {
  const showGrid = data.map((item, key) => {
    return <CCol className="mb-sm-2 mb-0" key={key}>
      <div className="text-medium-emphasis">{item.label}</div>
      <strong>
        {item.count}
      </strong>
      <CProgress
        thin
        className="mt-2"
        color={item.color}
        value={100}
      />
    </CCol>
  })
  return <>
    <CRow className="text-center">
      {showGrid}
    </CRow>
  </>
}

export default SensorFooter