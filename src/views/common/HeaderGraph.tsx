import React, { useState, useEffect } from 'react'
import {
  CRow,
  CCol,
  CWidgetStatsC,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import {
  cilCloudy
} from '@coreui/icons'

const Header = ({ dataList }): JSX.Element => {
  const isEmpty = (dataList.length === 0)
  let list
  if (isEmpty) {
    list = <CCol>
      <CWidgetStatsC
        className="mb-3"
        icon={<CIcon icon={cilCloudy} height={40} />}
        color="primary-gradient"
        inverse
        progress={{ value: 100 }}
        title="헤더값 로딩중"
        value={"0"}
      />
    </CCol>
  } else {
    list = dataList.map((item, key) => {
      return <CCol key={key}>
        <CWidgetStatsC
          className="mb-3"
          icon={<CIcon icon={item.icon} height={40} />}
          color={item.color}
          inverse
          progress={{ value: 100 }}
          title={item.title}
          value={item.value || '0'}
        />
      </CCol>
    })
  }
  return (
    <>
      <CRow>
        {list}
      </CRow>
    </>
  )
}
export default Header
