import React, { useEffect, useState } from 'react'
import { useFormik } from "formik"
import * as Yup from 'yup'
import edge from '../assets/login/edgeIcon.svg'
import logo from '../assets/login/logo.png'
import fLogo from '../assets/ci.png'
import { Form, Col, Button } from 'react-bootstrap'
import mainStyles from '../styles/app.module.css'
import styles from '../styles/login.module.css'
import commonStyles from '../styles/common.module.css'
import LoadingOverlay from 'react-loading-overlay-ts'

const Login = (props) => {
  const [isLoading, setIsLoading] = useState(false)

  const { logout } = props
  useEffect(() => {
    logout()
  }, [logout])

  const loginForm = Yup.object().shape({
    j_username: Yup.string().required('유저 아이디는 필수 입니다.'),
    j_password: Yup.string().required('비밀번호는 필수 입니다.'),
  })

  const formik = useFormik({
    initialValues: {
      j_username: "",
      j_password: "",
    },
    validationSchema: loginForm,
    onSubmit: values => {
      setIsLoading(true)
      props.login(values.j_username, values.j_password).then(function(response) {
        setIsLoading(false)
        const status = response.status
        if(status === 200) {
          props.history.push("/realtime-content")
        } else if(status === 204) {
          alert("로그인에 실패하였습니다. \n계정이 잠겨 있습니다 관리자에게 확인하여 주시기 바랍니다.")
        } else {
          alert("로그인에 실패하였습니다. \n계정정보를 확인 하여 주시기 바랍니다.")
        }
      })
    }
  })
  return (
    <LoadingOverlay active={isLoading} spinner text='로딩중 입니다....'>
    <div className={mainStyles.wrapper}>
      <main className={mainStyles.main}>
        <section className={styles.wrapper}>
          <div className={styles.container}>
            <aside className={styles.info_desk}>
              <img src={logo} alt="" />
              <h1>모니터링 관리자 페이지</h1>
              <p>
                계정을 분실하신 경우 031-304-7050
              </p>
              <span>
                <img src={edge} alt="" />
                수소 충전소 관리자 외에는 사용을 금합니다.
              </span>
            </aside>
            <div className={styles.formBox_wrapper}>
              <div className={styles.formBox_container}>
                <Form noValidate onSubmit={formik.handleSubmit}>
                  <Form.Group
                    as={Col}
                    controlId="validationFormik01"
                    style={{ width: '100%' }}
                  >
                    <Form.Label className={styles.label}>
                      아이디 &nbsp;<span style={{ color: '#E62D47' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="j_username"
                      value={formik.values.j_username}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      placeholder="작성자를 입력하세요."
                      style={{
                        borderRadius: 8,
                        width: '100%',
                        height: 50,
                      }}
                      className={(formik.errors.j_username) ? 'is-invalid' : ''}
                    />
                    {formik.touched.j_username ? (
                      <Form.Control.Feedback type="invalid">{formik.errors.j_username}</Form.Control.Feedback>
                    ) : null}
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    controlId="validationFormik02"
                    style={{ width: '100%' }}
                  >
                    <Form.Label className={styles.label}>
                      비밀번호 &nbsp;
                      <span style={{ color: '#E62D47' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="password"
                      name="j_password"
                      value={formik.values.j_password}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      placeholder="비밀번호를 입력하세요."
                      style={{
                        borderRadius: 8,
                        width: '100%',
                        height: 50,
                      }}
                      className={(formik.errors.j_password) ? 'is-invalid' : ''}
                    />
                    {formik.touched.j_password ? (
                      <Form.Control.Feedback type="invalid">{formik.errors.j_password}</Form.Control.Feedback>
                    ) : null}
                  </Form.Group>

                  <Button
                    type="submit"
                    className={styles.btn}
                    style={{ backgroundColor: '#316bb9', borderRadius: 8, marginTop: "20px" }}
                  >
                    로그인
                  </Button>
                </Form>
              </div>
            </div>

          </div>
        </section>
      </main>

      <footer>
      <div className={commonStyles.container}>
        <img src={fLogo} alt=""  width={180} style={{marginTop: '30px'}}  />
        <p>
          (주) PACOM
          <br /> 전화번호  : | TEL : 031-304-7050
          <br />
          경기도 수원시 장안구 장안로 113, 3층 (정자동) <br /> <br />
          <span>Copyright @ 2022 All Rights Reserved by PACOM Corp.</span>
        </p>
      </div>
    </footer>
    </div>
    </LoadingOverlay>
  )
}

export default Login
