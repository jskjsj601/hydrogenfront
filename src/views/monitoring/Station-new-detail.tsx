import React, { useState, useEffect } from 'react'
import {
  useParams
} from "react-router-dom"

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
} from '@coreui/react-pro'
import { GoogleMaps } from '../../components/common'
import {
  SensorHeader,
  SensorLineChart,
  SensorFooter,
  SensorRealTimeBox,
  DataBox,
  SensorDataList
} from '../common'

import {
  Box,
  ConnectedBox
} from '../../components/elements'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from '../../modules/service'


const StationNewDetail = (): JSX.Element => {
  const { id } = useParams<{ id: string }>()
  const [leftCols, setLeftCols] = useState(7)
  const [rightCols, setRightCols] = useState(0)
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingStationData, setLoadingStationData] = useState(false)
  const [isLoadingSensorData, setLoadingSensorData] = useState(false)
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const [titleData, setTitle] = useState<any>()
  const [chartData, setChartData] = useState<any>([])
  const [countData, setCountData] = useState<any>([])
  const [realTimeData, setRealTimeData] = useState<any>([])
  const [isLoadingRealData, setLoadingRealData] = useState(false)
  const [sensorCount, setSensorCount] = useState<any>()
  const [stationData, setStationData] = useState<any>([])
  const [sensorList, setSensorList] = useState<any>([])
  const [sensorPageCount, setSensorPageCount] = useState(0)
  const [selectedSensorId, setSensorId] = useState(0)
  const [page, setPage] = useState(1)
  const max = 10
  const sensorHeaders = ['수신일시', '데이터', '감지상태', '조치 상태']
  const [center, setCenter] = useState<any>(
    { lat: 36.3801686, lng: 127.3646184 }
  )
  const [locations, setLocations] = useState<any>([])
  const onChangeTimeUnit = function (type) {
    setTimeUnit(type)
    getDateData(type)
  }

  const onChangePage = function (currentPage) {
    setPage(currentPage)
    getSensor(currentPage, max, selectedSensorId)
  }
  

  const getDateData = function (type) {
    setLoadingChart(true)
    const url = `/admin/sensorControlByStationDetail/dataByTimeUnit/${id}/${type}`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setChartData(data.payload.timeUnitDateModels)
      const normalInfo = {
        label: "금일 안전 숫자",
        color: 'success-gradient',
        count: data.payload.countToNormalInToday
      }

      const warningInfo = {
        label: "금일 경고 숫자",
        color: 'warning-gradient',
        count: data.payload.countToWarningInToday
      }

      const dangerInfo = {
        label: "금일 장애 숫자",
        color: 'danger-gradient',
        count: data.payload.countToDangerInToday
      }

      const items = [normalInfo, warningInfo, dangerInfo]
      setCountData(items)
      setLoadingChart(false)
      setTitle(data.payload)
      setCenter({
        lat: Number(data.payload?.station.latitude || 0),
        lng: Number(data.payload?.station.longitude || 0),
      })
      setLocations(locations => [...locations, {
        title: data.payload?.station.name,
        lat: Number(data.payload?.station?.latitude ? data.payload?.station?.latitude : 0),
        lng: Number(data.payload?.station?.longitude ? data.payload?.station?.longitude : 0)
      }])
    })
  }

  const getRealTimeData = () => {
    service.request.get(`/admin/sensor_data/list/station/${id}`).then(function (response) {
      const payload = response.data.payload
      setRealTimeData(payload)
    })
  }

  const getSensorCount = () => {
    service.request.get(`/admin/dashboard/sensorCount/station/${id}`).then(function (response) {
      const payload = response.data.payload
      setSensorCount(payload)
    })
  }

  const getStationData = (segment) => {
    setLoadingStationData(true)
    service.request.get(`/admin/sensorControlByStation/dataByTimeUnit/sensor/${segment}/station/${id}`).then(function (response) {
      const payload = response.data.payload
      setStationData(payload)
      setLoadingStationData(false)
    })
  }

  const getSensor = function (page, max, sensorId) {
    const params = {
      page: page,
      max: max
    }
    setLoadingSensorData(true)
    service.request.get(`/admin/sensorControlBySensorDetail/sensorData/${sensorId}`, params).then(function (response) {
      const sensorData = response.data.payload.content
      const count = response.data.payload.totalElements
      setSensorList(sensorData)
      setSensorPageCount(count)
      setLoadingSensorData(false)
    })
  }

  const showSensorDataTable = (sensorId) => {
    setLeftCols(5)
    setRightCols(3)
    setSensorId(sensorId)
    getSensor(1, max, sensorId)
  }


  useEffect(() => {
    const interval = setInterval(() => {
      getRealTimeData()
    }, 60000)
    return () => clearInterval(interval)
  }, [])

  useEffect(() => {
    getRealTimeData()
  }, [])

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getDateData(chooseTimeUnit)
    getStationData(chooseTimeUnit)
  }, [chooseTimeUnit])

  useEffect(() => {
    getSensorCount()
  }, [])

  return <>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <CCardBody>
            <SensorHeader data={chooseTimeUnit} onChangeTimeUnit={onChangeTimeUnit} title={titleData} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <CCol md={4} style={{ width: "480px" }}>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                상태
              </CCardHeader>
              <CCardBody>
                {!(sensorCount?.numberOfSafety[1] === 0 && sensorCount?.numberOfSafety[2] === 0) &&
                  <CCol>
                    <Box className={"success"} title={"안전"} data={sensorCount?.numberOfSafety || []} type={"station"} />
                  </CCol>
                }
                {!(sensorCount?.numberOfWarning[1] === 0 && sensorCount?.numberOfWarning[2] === 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <Box className={"warning"} title={"위험"} data={sensorCount?.numberOfWarning || []} type={"station"} />
                  </CCol>
                }
                {!(sensorCount?.numberOfDanger[1] === 0 && sensorCount?.numberOfDanger[2] === 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <Box className={"danger"} title={"장애"} data={sensorCount?.numberOfDanger || []} type={"station"} />
                  </CCol>
                }
                {(sensorCount?.numberOfBrokenSensor !== 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <ConnectedBox className={"broken"} title={"이상 센서"} data={sensorCount?.numberOfBrokenSensor} />
                  </CCol>
                }
                {(sensorCount?.numberOfBrokenRouter[0] !== 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <ConnectedBox className={"route"} title={"이상 중계기"} data={sensorCount?.numberOfBrokenRouter} />
                  </CCol>
                }
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                안전 상태 그래프
              </CCardHeader>
              <CCardBody>
                <LoadingOverlay
                  active={isLoadingChart}
                  spinner
                  text='로딩중 입니다....'
                >
                  <SensorLineChart data={chartData} height={"200"} />
                </LoadingOverlay>
              </CCardBody>
              <CCardFooter>
                <SensorFooter data={countData} />
              </CCardFooter>
            </CCard>
          </CCol>

        </CRow>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                설치 위치
              </CCardHeader>
              <CCardBody>
                <GoogleMaps center={center || { lat: 36.3801686, lng: 127.3646184 }} locations={locations || []} height={"400px"} zoom={8} />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                실시간 데이터
              </CCardHeader>
              <CCardBody>
                <LoadingOverlay
                  active={isLoadingRealData}
                  spinner
                  text='로딩중 입니다....'
                >
                  <SensorRealTimeBox items={realTimeData || []} />
                </LoadingOverlay>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CCol>

      <CCol md={leftCols}>
        <CCard>
          <CCardHeader>
            센서별
          </CCardHeader>
          <CCardBody>
            <LoadingOverlay
              active={isLoadingStationData}
              spinner
              text='로딩중 입니다....'
            >
              <DataBox items={stationData || []} type={"sensor"} callback={showSensorDataTable} />
            </LoadingOverlay>
          </CCardBody>
        </CCard>
      </CCol>

      {rightCols !== 0 &&
        <CCol>
          <LoadingOverlay
            active={isLoadingSensorData}
            spinner
            text='로딩중 입니다....'
          >
            <SensorDataList
              headers={sensorHeaders}
              data={sensorList}
              count={sensorPageCount}
              page={page}
              max={max}
              onChangePage={onChangePage} />
          </LoadingOverlay>
        </CCol>
      }

    </CRow>
  </>
}

export default StationNewDetail
