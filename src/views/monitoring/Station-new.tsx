import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow
} from '@coreui/react-pro'

import { GoogleMaps } from '../../components/common'
import {
  SensorHeader,
  SensorLineChart,
  SensorFooter,
  SensorRealTimeBox,
  DataBox
} from '../common'

import {
  Box,
  ConnectedBox
} from '../../components/elements'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from '../../modules/service'
import filters from '../../modules/filters'

const StationNew = (): JSX.Element => {
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingRealData, setLoadingRealData] = useState(false)
  const [isLoadingStationData, setLoadingStationData] = useState(false)
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const [chartData, setChartData] = useState<any>([])
  const [countData, setCountData] = useState<any>([])
  const [locations, setLocations] = useState<any>([])
  const [realTimeData, setRealTimeData] = useState<any>([])
  const [stationData, setStationData] = useState<any>([])
  const [sensorCount, setSensorCount] = useState<any>()
  const [renewal, setRenewal] = useState(false)
  const [lastRenewalDate, setLastRenewalDate] = useState(filters.date.lastModifiedDate(new Date()) || "")
  const [center] = useState<any>(
    { lat: 36.3801686, lng: 127.3646184 }
  )

  const execRenewal = function () {
    getRealTimeData()
    getDateData(chooseTimeUnit)
    getStationData(chooseTimeUnit)
    getBottom('station')
    getSensorCount()
    setLastRenewalDate(filters.date.lastModifiedDate(new Date()))
  }

  const onChangeTimeUnit = function (type, renewal) {
    setTimeUnit(type)
    setRenewal(renewal)
    setLastRenewalDate(filters.date.lastModifiedDate(new Date()))
  }

  const getDateData = function (type) {
    setLoadingChart(true)
    const url = `/admin/sensorControlByStation/dataByTimeUnit/${type}`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setChartData(data.payload.timeUnitDateModels)
      const normalInfo = {
        label: "금일 안전 숫자",
        color: 'success-gradient',
        count: data.payload.countToNormalInToday
      }

      const warningInfo = {
        label: "금일 위험 숫자",
        color: 'warning-gradient',
        count: data.payload.countToWarningInToday
      }

      const dangerInfo = {
        label: "금일 장애 숫자",
        color: 'danger-gradient',
        count: data.payload.countToDangerInToday
      }

      const items = [normalInfo, warningInfo, dangerInfo]
      setCountData(items)
      setLoadingChart(false)
    })
  }

  const getBottom = function (segment) {
    if (segment === 'company') {
      segment = 'equipment'
    }
    service.request.get(`/admin/dashboard/dataBySegment/${segment}`).then(function (response) {
      const payload = response.data.payload
      payload.forEach(item => {
        setLocations(locations => [...locations, {
          title: item.data.name,
          lat: Number(item.data.latitude ? item.data.latitude : item.data.station.latitude),
          lng: Number(item.data.longitude ? item.data.longitude : item.data.station.longitude)
        }])
      })
    })
  }

  const getRealTimeData = () => {
    service.request.get("/admin/sensor_data/list").then(function (response) {
      const payload = response.data.payload
      console.log(payload)
      setRealTimeData(payload)
    })
  }

  const getStationData = (segment) => {
    setLoadingStationData(true)
    service.request.get(`/admin/sensorControlByStation/dataByTimeUnit/station/${segment}`).then(function (response) {
      const payload = response.data.payload
      setStationData(payload)
      setLoadingStationData(false)
    })
  }

  const getSensorCount = () => {
    setLoadingRealData(true)
    service.request.get("/admin/dashboard/sensorCount").then(function (response) {
      const payload = response.data.payload
      setSensorCount(payload)
      setLoadingRealData(false)
    })
  }


  useEffect(() => {
    const interval = setInterval(() => {
      if (renewal) {
        getRealTimeData()
        getDateData(chooseTimeUnit)
        getStationData(chooseTimeUnit)
        getBottom('station')
        getSensorCount()
        setLastRenewalDate(filters.date.lastModifiedDate(new Date()))
      }
    }, 60000)
    return () => clearInterval(interval)
  }, [renewal])

  useEffect(() => {
    const test =async()=>{
    await getRealTimeData()
   await getDateData(chooseTimeUnit)
   await getBottom('station')
   await getSensorCount()
    await getStationData(chooseTimeUnit)
    }
   test()
  }, [chooseTimeUnit])



  return <>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <CCardBody>
            <SensorHeader data={chooseTimeUnit} onChangeTimeUnit={onChangeTimeUnit} execRenewal={execRenewal} lastRenewalDate={lastRenewalDate} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <CCol md={4} style={{ width: "480px" }}>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                상태
              </CCardHeader>
              <CCardBody>
                {!(sensorCount?.numberOfSafety[0] === 0 && sensorCount?.numberOfSafety[1] === 0 && sensorCount?.numberOfSafety[2] === 0) &&
                  <CCol>
                    <Box className={"success"} title={"안전"} data={sensorCount?.numberOfSafety || []} />
                  </CCol>
                }
                {!(sensorCount?.numberOfWarning[0] === 0 && sensorCount?.numberOfWarning[1] === 0 && sensorCount?.numberOfWarning[2] === 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <Box className={"warning"} title={"위험"} data={sensorCount?.numberOfWarning || []} />
                  </CCol>
                }
                {!(sensorCount?.numberOfDanger[0] === 0 && sensorCount?.numberOfDanger[1] === 0 && sensorCount?.numberOfDanger[2] === 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <Box className={"danger"} title={"장애"} data={sensorCount?.numberOfDanger || []} />
                  </CCol>
                }
                {(sensorCount?.numberOfBrokenSensor !== 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <ConnectedBox className={"broken"} title={"이상 센서"} data={sensorCount?.numberOfBrokenSensor} />
                  </CCol>
                }
                {(sensorCount?.numberOfBrokenRouter[0] !== 0) &&
                  <CCol style={{ marginTop: "10px" }}>
                    <ConnectedBox className={"route"} title={"이상 중계기"} data={sensorCount?.numberOfBrokenRouter} />
                  </CCol>
                }
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CRow  >
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                안전 상태 그래프
              </CCardHeader>
              <CCardBody>
                <LoadingOverlay
                  active={isLoadingChart}
                  spinner
                  text='로딩중 입니다....'
                >
                  <SensorLineChart data={chartData} height={"200"} />
                </LoadingOverlay>
              </CCardBody>
              <CCardFooter>
                <SensorFooter data={countData} />
              </CCardFooter>
            </CCard>
          </CCol>

        </CRow>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                실시간 데이터
              </CCardHeader>
              <CCardBody>
                <LoadingOverlay
                  active={isLoadingRealData}
                  spinner
                  text='로딩중 입니다....'
                >
                  <SensorRealTimeBox items={realTimeData || []} />

                </LoadingOverlay>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

      </CCol>
      <CCol md={5} style={{ width: "480px" }}>
        <CCard className="mb-4">
          <CCardHeader>
            충전소&시설별
          </CCardHeader>
          <CCardBody>
            <LoadingOverlay
              active={isLoadingStationData}
              spinner
              text='로딩중 입니다....'
            >
              <DataBox items={stationData || []} />
            </LoadingOverlay>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol>
        <CCard className="mb-4">
          <CCardHeader>
            설치 위치
          </CCardHeader>
          <CCardBody>
            <GoogleMaps center={center || { lat: 36.3801686, lng: 127.3646184 }} locations={locations || []} height={"1200px"} zoom={8} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default StationNew
