import React, { useState, useEffect } from 'react'
import { Link, useHistory } from "react-router-dom"
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CFormSelect,
  CInputGroup,
  CFormInput,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CCardFooter,
} from '@coreui/react-pro'
import LoadingOverlay from 'react-loading-overlay-ts'
import Pagination from '../../components/Pagination'
import service from "../../modules/service"
import filters from "../../modules/filters"
import styles from '../../modules/css/apply.module.css'
import {
  ReportModal
} from '../../components/modal'

const Report = (): JSX.Element => {
  const history = useHistory()
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoading, setIsLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [picUpdateId, setId] = useState(0)
  const [showPopup, setShowPopup] = useState(false)

  const [searchStatus, setSearchStatus] = useState("ALL")
  const [searchType, setSearchType] = useState("ALL")
  const [searchText, setSearchText] = useState("")


  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getList(currentPage, max, searchStatus, searchType, searchText)
  }

  const search = function () {
    setPage(1)
    getList(1, max, searchStatus, searchType, searchText)
  }

  const showReportPopup = function (id) {
    setId(id)
    setShowPopup(true)
  }

  const getList = function (page, max, searchStatus, searchType, searchText) {
    const params = {
      page: page,
      max: max,
      searchStatus: searchStatus,
      searchType: searchType,
      searchText: searchText,
    }
    setIsLoading(true)
    service.request.get("admin/sensorDataReport", params).then(function (response) {
      
      const data = response.data.payload
      console.log(data)
      setList(data.content)
      setCount(data.totalElements)
      setIsLoading(false)
    })
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    search()
  }, [])

  let items
  if ((count === 0)) {
    items = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    items = list.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont} scope="row">{item.id}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.createDate)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensorDataInput?.sensor?.station?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensorDataInput?.sensor?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.description}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensorDataInput?.data}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} style={filters.status.getTypeColor(item.sensorDataInput?.type)} scope="row">
          {filters.status.convertToSensorDataStatus(item.sensorDataInput?.type)}
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">
          <CButton color="link" onClick={() => showReportPopup(item.id)}>
            {filters.status.convertToReportStatus(item.status)}
          </CButton>
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.adminUser?.name}</CTableDataCell>
      </CTableRow >
    })
  }

  return <>
    <CRow>
      <ReportModal
        showPopup={showPopup}
        setShowPopup={setShowPopup}
        updateId={picUpdateId}
        refresh={search} />
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>리포트 관리</strong>
          </CCardHeader>
          <CCardBody>
            <CRow>
              <CCol md={2}></CCol>
              <CCol md={3}></CCol>
              <CCol md={2}>
                <CInputGroup className="mb-3">
                  <CFormSelect
                    value={searchStatus}
                    name="searchStatus"
                    onChange={e => setSearchStatus(e.target.value)}>
                    <option value="ALL">상태</option>
                    <option value="NORMAL">안전</option>
                    <option value="LOW_WARNING">낮은값 경고</option>
                    <option value="HIGH_WARNING">높은값 경고</option>
                    <option value="LOW_DANGER">낮은값 위험</option>
                    <option value="HIGH_DANGER">높은값 위험</option>
                  </CFormSelect>
                </CInputGroup>
              </CCol>
              <CCol md={2}>
                <CInputGroup className="mb-3">
                  <CFormSelect
                    value={searchType}
                    name="searchType"
                    onChange={e => setSearchType(e.target.value)}>
                    <option value="ALL">키워드</option>
                    <option value="STATION_NAME">충전소명</option>
                    <option value="EQUIPMENT_NAME">기관명</option>
                  </CFormSelect>
                </CInputGroup>
              </CCol>
              <CCol md={3}>
                <CInputGroup className="mb-3">
                  <CFormInput
                    placeholder="검색어 입력"
                    aria-describedby="button-addon2"
                    value={searchText}
                    name="searchText"
                    onChange={e => setSearchText(e.target.value)} />
                  <CButton type="button" color="primary" id="button-addon2" onClick={() => search()}>검색</CButton>
                </CInputGroup>
              </CCol>

            </CRow>
            <CRow>
              <LoadingOverlay
                active={isLoading}
                spinner
                text='로딩중 입니다....'
              >
                <CCol md={12}>
                  <CTable align="middle" className="mb-0 border" hover responsive bordered>
                    <CTableHead color="light">
                      <CTableRow>
                        <CTableHeaderCell className={styles.tableFont} scope="col">이력번호</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">발생일자</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">충전소명</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">센서명</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">리포트 내용</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">수치값</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">상태</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">처리여부</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">처리담당자</CTableHeaderCell>
                      </CTableRow>
                    </CTableHead>
                    <CTableBody>
                      {items}
                    </CTableBody>
                  </CTable>
                </CCol>

              </LoadingOverlay>
            </CRow>
          </CCardBody>
          <CCardFooter>
            <CRow>
              <CCol xs={5}>
                ( <strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
              </CCol>
              <CCol md={7} >
                <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} viewType={'full'} />
              </CCol>
            </CRow>
          </CCardFooter>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default Report
