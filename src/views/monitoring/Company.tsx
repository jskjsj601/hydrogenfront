import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CTable,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CTableBody,
  CTableDataCell
} from '@coreui/react-pro'
import {
  cibAddthis,
  cilCheck,
  cilWarning,
  cilXCircle,
} from '@coreui/icons'
import {
  HeaderGraph,
  StationBox,
  SensorHeader,
  SensorLineChart,
  SensorFooter
} from '../common'
import LoadingOverlay from 'react-loading-overlay-ts'
import styles from '../../modules/css/apply.module.css'
import Pagination from '../../components/Pagination'
import service from '../../modules/service'
import filters from '../../modules/filters'

const Company = (): JSX.Element => {
  const [headerData, setHeaderData] = useState<any>([])
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingList, setLoadingList] = useState(false)
  const [isLoadingBox, setLoadingBox] = useState(false)
  const [page, setPage] = useState(1)
  const [stations, setStations] = useState<any>([])
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const [chartData, setChartData] = useState<any>([])
  const [countData, setCountData] = useState<any>([])


  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getSensor(currentPage, max)
  }

  const onChangeTimeUnit = function (type) {
    setTimeUnit(type)
    getDateData(type)
  }

  const getHeader = function () {
    setLoadingBox(true)
    service.request.get("/admin/sensorControlByEquipment/sensorNumber", {}).then(function (response) {
      const data = response.data
      const confirmInfo = {
        icon: cilCheck,
        color: 'info-gradient',
        title: "확인",
        value: data.payload.numberOfVerifiedReport
      }
      const safetyInfo = {
        icon: cibAddthis,
        color: 'success-gradient',
        title: "안전",
        value: data.payload.numberOfSecuredSensors
      }
      const warningInfo = {
        icon: cilWarning,
        color: 'warning-gradient',
        title: "경고",
        value: data.payload.sumOfWaringSensors
      }
      const dangerInfo = {
        icon: cilXCircle,
        color: 'danger-gradient',
        title: "위험",
        value: data.payload.sumOfDangerSensors
      }
      const items = [confirmInfo, safetyInfo, warningInfo, dangerInfo]
      setHeaderData(items)

      const stationList = data.payload.equipmentList
      setStations(stationList)
      setLoadingBox(false)
    })
  }

  const getDateData = function (type) {
    setLoadingChart(true)
    const url = `/admin/sensorControlByEquipment/dataByTimeUnit/${type}`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setChartData(data.payload.timeUnitDateModels)
      const confirmInfo = {
        label: "금일 확인 숫자",
        color: 'info-gradient',
        count: data.payload.countToConfirmInToday
      }

      const normalInfo = {
        label: "금일 안전 숫자",
        color: 'success-gradient',
        count: data.payload.countToNormalInToday
      }

      const warningInfo = {
        label: "금일 경고 숫자",
        color: 'warning-gradient',
        count: data.payload.countToWarningInToday
      }

      const dangerInfo = {
        label: "금일 위험 숫자",
        color: 'danger-gradient',
        count: data.payload.countToDangerInToday
      }

      const items = [confirmInfo, normalInfo, warningInfo, dangerInfo]
      setCountData(items)
      setLoadingChart(false)
    })

  }

  const getSensor = function (page, max) {
    const params = {
      page: page,
      max: max
    }
    setLoadingList(true)
    service.request.get("/admin/sensorControlByEquipment/sensorData", params).then(function (response) {
      const sensorData = response.data.payload.content
      const count = response.data.payload.totalElements
      setList(sensorData)
      setCount(count)
      setLoadingList(false)
    })
  }

  let boxItems
  if (stations.length === 0) {
    boxItems =
      <CCol>
        <CCard className="mb-4">
          <CCardHeader className={styles.cardHeader}>
            <strong className={styles.cardHeaderText}>데이터가 없습니다.</strong>
          </CCardHeader>
        </CCard>
      </CCol>
  } else {
    boxItems = stations.map((item, key) => {
      return <CCol key={key} md={3}>
        <StationBox data={item} link={'company'} />
      </CCol>
    })
  }

  let sensorData
  if (list.length === 0) {
    sensorData = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    sensorData = list.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont}>{item.sensor?.equipment?.name}</CTableDataCell>
        <CTableDataCell style={filters.status.getTypeColor(item.type)} className={styles.tableFont}>
          {filters.status.convertToSensorDataStatus(item.type)}
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.sensor?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{item.data}</CTableDataCell>
        <CTableDataCell className={styles.tableFont}>{filters.date.lastModifiedDate(item.sensingDate)}</CTableDataCell>
      </CTableRow>
    })
  }

  useEffect(() => {
    getHeader()
    getDateData('minute')
    getSensor(page, max)
  }, [page])

  return <>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <HeaderGraph dataList={headerData}></HeaderGraph>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <LoadingOverlay
            active={isLoadingChart}
            spinner
            text='로딩중 입니다....'
          >
            <CCardBody>
              <SensorHeader data={chooseTimeUnit} onChangeTimeUnit={onChangeTimeUnit} />
              <SensorLineChart data={chartData} />
            </CCardBody>
            <CCardFooter>
              <SensorFooter data={countData} />
            </CCardFooter>
          </LoadingOverlay>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <CRow>
          {boxItems}
        </CRow>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingList}
        spinner
        text='로딩중 입니다....'
      >
        <CCol xs>
          <CCard className="mb-4">
            <CCardHeader>센서 정보 리스트
              <CButton color="link">
                전체 보기
              </CButton>
            </CCardHeader>
            <CCardBody>
              <CTable align="middle" className="mb-0 border" hover responsive bordered>
                <CTableHead color="light">
                  <CTableRow>
                    <CTableHeaderCell className={styles.tableFont} scope="col">기관명</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">상태</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">센서명</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">수치값</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">발생일자</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {sensorData}
                </CTableBody>
              </CTable>
            </CCardBody>
            <CCardFooter>
              <CRow>
                <CCol xs={5}>
                  ( <strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
                </CCol>
                <CCol md={7} >
                  <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} />
                </CCol>
              </CRow>
            </CCardFooter>
          </CCard>
        </CCol>
      </LoadingOverlay>
    </CRow>
  </>
}

export default Company
