import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
} from '@coreui/react-pro'
import {
  cibAddthis,
  cilCheck,
  cilWarning,
  cilXCircle,
} from '@coreui/icons'
import {
  HeaderGraph,
  StationBox,
  SensorHeader,
  SensorLineChart,
  SensorFooter,
  SensorDataList
} from '../common'
import LoadingOverlay from 'react-loading-overlay-ts'
import styles from '../../modules/css/apply.module.css'
import service from '../../modules/service'

const Sensor = (): JSX.Element => {
  const [headerData, setHeaderData] = useState<any>([])
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const sensorHeaders = ['센서명', '상태', '센서명', '수치값', '빌생일자']
  const [isLoadingChart, setLoadingChart] = useState(false)
  const [isLoadingList, setLoadingList] = useState(false)
  const [isLoadingBox, setLoadingBox] = useState(false)
  const [page, setPage] = useState(1)
  const [stations, setStations] = useState<any>([])
  const [chooseTimeUnit, setTimeUnit] = useState('minute')
  const [chartData, setChartData] = useState<any>([])
  const [countData, setCountData] = useState<any>([])


  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getSensor(currentPage, max)
  }

  const onChangeTimeUnit = function (type) {
    setTimeUnit(type)
    getDateData(type)
  }

  const getHeader = function () {
    setLoadingBox(true)
    service.request.get("/admin/sensorControlBySensor/sensorNumber", {}).then(function (response) {
      const data = response.data
      const confirmInfo = {
        icon: cilCheck,
        color: 'info-gradient',
        title: "확인",
        value: data.payload.numberOfVerifiedReport
      }
      const safetyInfo = {
        icon: cibAddthis,
        color: 'success-gradient',
        title: "안전",
        value: data.payload.numberOfSecuredSensors
      }
      const warningInfo = {
        icon: cilWarning,
        color: 'warning-gradient',
        title: "경고",
        value: data.payload.sumOfWaringSensors
      }
      const dangerInfo = {
        icon: cilXCircle,
        color: 'danger-gradient',
        title: "위험",
        value: data.payload.sumOfDangerSensors
      }
      const items = [confirmInfo, safetyInfo, warningInfo, dangerInfo]
      setHeaderData(items)

      const stationList = data.payload.stationList
      setStations(stationList)
      setLoadingBox(false)
    })
  }

  const getDateData = function (type) {
    setLoadingChart(true)
    const url = `/admin/sensorControlBySensor/dataByTimeUnit/${type}`
    service.request.get(url, {}).then(function (response) {
      const data = response.data
      setChartData(data.payload.timeUnitDateModels)
      const confirmInfo = {
        label: "금일 확인 숫자",
        color: 'info-gradient',
        count: data.payload.countToConfirmInToday
      }

      const normalInfo = {
        label: "금일 안전 숫자",
        color: 'success-gradient',
        count: data.payload.countToNormalInToday
      }

      const warningInfo = {
        label: "금일 경고 숫자",
        color: 'warning-gradient',
        count: data.payload.countToWarningInToday
      }

      const dangerInfo = {
        label: "금일 위험 숫자",
        color: 'danger-gradient',
        count: data.payload.countToDangerInToday
      }

      const items = [confirmInfo, normalInfo, warningInfo, dangerInfo]
      setCountData(items)
      setLoadingChart(false)
    })

  }

  const getSensor = function (page, max) {
    const params = {
      page: page,
      max: max
    }
    setLoadingList(true)
    service.request.get("/admin/sensorControlBySensor/sensorData", params).then(function (response) {
      const sensorData = response.data.payload.content
      const count = response.data.payload.totalElements
      setList(sensorData)
      setCount(count)
      setLoadingList(false)
    })
  }

  let boxItems
  if (stations.length === 0) {
    boxItems =
      <CCol>
        <CCard className="mb-4">
          <CCardHeader className={styles.cardHeader}>
            <strong className={styles.cardHeaderText}>데이터가 없습니다.</strong>
          </CCardHeader>
        </CCard>
      </CCol>
  } else {
    boxItems = stations.map((item, key) => {
      return <CCol key={key} md={3}>
        <StationBox data={item} link={"sensor"} />
      </CCol>
    })
  }

  useEffect(() => {
    getHeader()
    getDateData('minute')
    getSensor(page, max)
  }, [page])

  return <>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <HeaderGraph dataList={headerData}></HeaderGraph>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <LoadingOverlay
            active={isLoadingChart}
            spinner
            text='로딩중 입니다....'
          >
            <CCardBody>
              <SensorHeader data={chooseTimeUnit} onChangeTimeUnit={onChangeTimeUnit} />
              <SensorLineChart data={chartData} />
            </CCardBody>
            <CCardFooter>
              <SensorFooter data={countData} />
            </CCardFooter>
          </LoadingOverlay>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingBox}
        spinner
        text='로딩중 입니다....'
      >
        <CRow>
          {boxItems}
        </CRow>
      </LoadingOverlay>
    </CRow>
    <CRow>
      <LoadingOverlay
        active={isLoadingList}
        spinner
        text='로딩중 입니다....'
      >
        <SensorDataList
          headers={sensorHeaders}
          data={list}
          count={count}
          page={page}
          max={max}
          link={'sensor'}
          onChangePage={onChangePage} />
      </LoadingOverlay>
    </CRow>
  </>
}

export default Sensor
