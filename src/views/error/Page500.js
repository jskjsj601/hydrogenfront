import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCol,
  CContainer,
  CInputGroup,
  CRow,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilExitToApp } from '@coreui/icons'

const Page500 = () => {
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <span className="clearfix">
              <h1 className="float-start display-3 me-4">500</h1>
              <h4 className="pt-3">에러 발생!!!!</h4>
              <p className="text-medium-emphasis float-start">
                이 페이지는 에러가 발생했을때 도출되는 페이지 입니다.
              </p>
            </span>
            <CInputGroup className="input-prepend">
              <Link to="/login">
                <CButton color="info" size="lg">
                  <CIcon icon={cilExitToApp} className="me-2" />
                  로그인 페이지로 이동
                </CButton>
              </Link>
            </CInputGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Page500
