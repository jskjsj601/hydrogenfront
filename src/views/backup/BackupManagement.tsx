import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CFormSelect,
  CInputGroup,
  CFormInput,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CCardFooter,
  CLoadingButton
} from '@coreui/react-pro'
import LoadingOverlay from 'react-loading-overlay-ts'
import Pagination from '../../components/Pagination'
import service from "../../modules/service"
import filters from "../../modules/filters"
import styles from '../../modules/css/apply.module.css'
import { off } from 'process'


const BackupManagement = (): JSX.Element => {
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoading, setIsLoading] = useState(false)
  const [execLoading, setExecLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [searchStatus, setSearchStatus] = useState("ALL")

  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getList(currentPage, max, searchStatus)
  }

  const getList = function (page, max, searchStatus) {
    const params = {
      page: page,
      max: max,
      searchStatus: searchStatus,
    }
    setIsLoading(true)
    service.request.get("admin/backup", params).then(function (response) {
      const data = response.data.payload
      setList(data.content)
      setCount(data.totalElements)
      setIsLoading(false)
    })
  }

  const convertToStatus = function (status) {
    if (status === 'SUCCESS') {
      return "성공"
    } else {
      return "실패"
    }
  }

  const search = function () {
    setPage(1)
    getList(1, max, searchStatus)
  }

  const execBackup = function () {
    if (window.confirm("백업을 실행하시겠습니까?")) {
      setExecLoading(true)
      service.request.post("admin/backup", null).then(function (response) {
        setExecLoading(false)
        const data = response.data.status
        alert("백업이 실행되었습니다.")
        getList(1, max, 'ALL')
      })
    }
  }

  const getDownload = function (path) {
    const filePath = process.env.REACT_APP_SERVER_URL + "/admin/file/download?path=" + path
    return filePath
  }


  let items
  if (count === 0) {
    items = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    items = list.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont} scope="row">{item.id}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{(item.status === 'SUCCESS') ? (<a href={getDownload(item.path)}>{item.path} </a>) : ("파일없음")}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{convertToStatus(item.status)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.createDate, item.updateDate)}</CTableDataCell>
      </CTableRow >
    })
  }


  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    search()
  }, [])

  return <>
    <CRow>
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>백업 정보 관리</strong>
          </CCardHeader>
          <CCardBody>
            <CRow>
              <CCol md={4}>
                <CLoadingButton loading={execLoading} onClick={() => execBackup()}>
                  현재 일자로 백업 실행하기
                </CLoadingButton>
              </CCol>
              <CCol md={6}></CCol>
              <CCol md={2}>
                <CInputGroup className="mb-3">
                  <CFormSelect
                    value={searchStatus}
                    name="searchStatus"
                    onChange={e => setSearchStatus(e.target.value)}>
                    <option value="ALL">상태</option>
                    <option value="SUCCESS">백업 성공</option>
                    <option value="FAILED">백업 실패</option>
                  </CFormSelect>
                  <CButton type="button" color="primary" id="button-addon2" onClick={() => search()}>검색</CButton>
                </CInputGroup>
              </CCol>
            </CRow>
            <CRow>
              <LoadingOverlay
                active={isLoading}
                spinner
                text='로딩중 입니다....'
              >
                <CCol md={12}>
                  <CTable align="middle" className="mb-0 border" hover responsive bordered>
                    <CTableHead color="light">
                      <CTableRow>
                        <CTableHeaderCell className={styles.tableFont} scope="col">ID</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">다운로드</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">백업 상태</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">백업일</CTableHeaderCell>
                      </CTableRow>
                    </CTableHead>
                    <CTableBody>
                      {items}
                    </CTableBody>
                  </CTable>
                </CCol>
              </LoadingOverlay>
            </CRow>
          </CCardBody>
          <CCardFooter>
            <CRow>
              <CCol xs={5}>
                ( <strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
              </CCol>
              <CCol md={7} >
                <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} />
              </CCol>
            </CRow>
          </CCardFooter>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default BackupManagement
