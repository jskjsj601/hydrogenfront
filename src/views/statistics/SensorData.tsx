import React, { useState, useEffect } from 'react'
import { useTypedSelector } from '../../store'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CTable,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CTableBody,
  CTableDataCell,
  CFormInput,
  CFormLabel,
  CFormCheck,
  CFormSelect,
} from '@coreui/react-pro'

import Select from 'react-select'
import LoadingOverlay from 'react-loading-overlay-ts'
import styles from '../../modules/css/apply.module.css'
import Pagination from '../../components/Pagination'
import service from '../../modules/service'
import filters from '../../modules/filters'
import moment from 'moment'
import {
  ReportModal
} from '../../components/modal'

import * as ExcelJS from "exceljs"
import { saveAs } from "file-saver"

const SensorData = (): JSX.Element => {
  const [leftCols, setLeftCols] = useState(12)
  const [rightCols, setRightCols] = useState(0)

  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoading, setIsLoading] = useState(false)
  const [isSubDataLoading, setSubDataLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [stations, setStation] = useState<any>([])
  const [equipments, setEquipment] = useState<any>([])
  const [sensors, setSensor] = useState<any>([])
  const [sensingDate, setSensingDate] = useState("DAY")
  const [showPopup, setShowPopup] = useState(false)
  const [checks, setChecks] = useState<any>(['ALL'])
  const [checkAll, setCheckAll] = useState("ALL")
  const [check1, setCheck1] = useState("")
  const [check2, setCheck2] = useState("")
  const [check3, setCheck3] = useState("")
  const [check4, setCheck4] = useState("")
  const [check5, setCheck5] = useState("")
  const [reportType, setReportType] = useState("ALL")

  const [searchStation, setPickStation] = useState("0")
  const [searchEquipment, setPickEquipment] = useState("0")
  const [searchSensorIds, setSensorIds] = useState<any>([])
  const [startDate, setStartDate] = useState(moment().subtract(1, "days").format("YYYY-MM-DD"))
  const [endDate, setEndDate] = useState(moment(new Date()).format("YYYY-MM-DD"))

  const [picData, setPicData] = useState({})
  const [picSubData, setSubData] = useState({})

  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))

  const onChangePage = (currentPage) => {
    setPage(currentPage)
    search(currentPage)

    setLeftCols(12)
    setRightCols(0)
    setPicData({})
  }
console.log(userInfo)
  const getInitData = () => {
    const params = {}
    service.request.get(`admin/sensor_data/init`, params).then(function (response) {
      const data = response.data.payload
      if (userInfo.roleType === 'ROLE_ADMIN') {
        const arr = []
        arr.push(userInfo.station)
        setStation(arr)
        setPickStation(userInfo.station.id)
        setEquipment(data?.equipmentList)
        const params = {
          station: userInfo.station?.id,
          equipment: searchEquipment
        }
        service.request.get(`admin/sensor_data/sensor`, params).then(function (response) {
          const data = response.data.payload
          setSensor(data)
          search(1)
        })
      } else {
        setStation(data?.stationList)
        setSensor(data?.sensorList)
        search(1)
      }
    })
  }

  const onChangeStation = (event) => {
    setPickStation(event.target.value)
    const params = {
      station: event.target.value,
      equipment: searchEquipment
    }
    
    service.request.get(`admin/sensor_data/sensor`, params).then(function (response) {
      const data = response.data.payload
      console.log(data)
      setSensor(data)
    })
  }

  const onChangeEquipment = (event) => {
    setPickEquipment(event.target.value)

    const params = {
      station: searchStation,
      equipment: event.target.value
    }
    service.request.get(`admin/sensor_data/sensor`, params).then(function (response) {
      const data = response.data.payload
      setSensor(data)
    })
  }

  const onChangeMultiple = (event) => {
    const sensorIds = event.map((item, _) => {
      return item.value
    })
    setSensorIds(sensorIds)
  }

  const changeSensingDate = (type) => {
    if (type === 'ALL') {
      setStartDate(moment().subtract(10, "years").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    } else if (type === 'DAY') {
      setStartDate(moment().subtract(1, "days").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    } else if (type === 'WEEK') {
      setStartDate(moment().subtract(1, "week").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    } else if (type === 'MONTH') {
      setStartDate(moment().subtract(1, "month").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    } else if (type === 'HALF') {
      setStartDate(moment().subtract(6, "month").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    } else if (type === 'YEAR') {
      setStartDate(moment().subtract(1, "year").format("YYYY-MM-DD"))
      setEndDate(moment(new Date()).format("YYYY-MM-DD"))
    }
    setSensingDate(type)
  }

  const setCheckType = (event) => {
    const checkType = event.target.value
    if (event.target.checked) {
      if (checkType === 'ALL') {
        setCheck1("")
        setCheck2("")
        setCheck3("")
        setCheck4("")
        setCheck5("")
        setCheckAll("ALL")
      } else if (checkType === 'NORMAL') {
        setCheck1("")
        setCheck1(checkType)
      } else if (checkType === 'WARNING') {
        setCheck2("")
        setCheck2(checkType)
      } else if (checkType === 'DANGER') {
        setCheck3("")
        setCheck3(checkType)
      } else if (checkType === 'SERVER_ERROR') {
        setCheck4("")
        setCheck4(checkType)
      } else if (checkType === 'ROUTE_ERROR') {
        setCheck5("")
        setCheck5(checkType)
      }
    } else {
      if (checkType === 'ALL') {
        setCheckAll("")
      } else if (checkType === 'NORMAL') {
        setCheck1("")
      } else if (checkType === 'WARNING') {
        setCheck2("")
      } else if (checkType === 'DANGER') {
        setCheck3("")
      } else if (checkType === 'SERVER_ERROR') {
        setCheck4("")
      } else if (checkType === 'ROUTE_ERROR') {
        setCheck5("")
      }
    }

    if (event.target.checked) {
      const index = checks.indexOf(checkType)
      if (index !== -1) {
        checks.splice(index, 1)
      }

      checks.push(checkType)
    } else {
      const index = checks.indexOf(checkType)
      if (index !== -1) {
        checks.splice(index, 1)
      }
    }
    setChecks(checks)
  }

  const changeReportType = (value) => {
    setReportType(value)
  }

  const showSideDataSub = () => {
    const item = picData
    showSideData(item)
  }

  const showSideData = (item) => {
    setLeftCols(8)
    setRightCols(4)
    setPicData(item)
    setSubDataLoading(true)
    const params = { sensorId: item?.sensor?.id, dataId: item?.id }
    service.request.get(`admin/sensor_data/detail`, params).then(function (response) {
      const data = response.data.payload
      setSubData(data)
      setPicData(data.sensorData)
      setSubDataLoading(false)
    })
  }

  const search = (page) => {
    setLeftCols(12)
    setRightCols(0)
    setPicData({})
    const params = {
      page: page,
      max: max,
      startDate: startDate,
      endDate: endDate,
      checks: checks.join(","),
      reportType: reportType,
      station: searchStation,
      equipment: searchEquipment,
      sensorIds: searchSensorIds.join(",")
    }
    setIsLoading(true)
    service.request.get(`admin/sensor_data/page`, params).then(function (response) {
      const data = response.data.payload
      setList(data.content)
      setCount(data.totalElements)
      setIsLoading(false)
    })
  }

  const execPopup = (e) => {
    e.preventDefault()
    setShowPopup(true)
  }

  const download = async () => {
    if (window.confirm("데이터를 엑셀로 다운로드 하시겠습니까?")) {
      setIsLoading(true)
      search(1)
      const params = {
        page: 1,
        max: 9999,
        startDate: startDate,
        endDate: endDate,
        checks: checks.join(","),
        reportType: reportType,
        station: searchStation,
        equipment: searchEquipment,
        sensorIds: searchSensorIds.join(",")
      }

      const data = await service.request.get("admin/sensor_data/excel", params)
      const downloadData: any = data.data?.payload?.content.map((item, _) => {
        return {
          sensingDate: filters.date.lastModifiedDate(item.sensingDate),
          sensorId: item.sensor?.id,
          sensorName: item.sensor?.name,
          data: item.data,
          sensorUnit: item.sensor?.unit,
          type: filters.status.convertToSensorDataStatus(item.type),
          reportStatus: filters.status.convertToReportStatus(item.reportStatus),
          stationName: item.sensor?.station?.name,
          equipmentName: item.sensor?.equipment?.name
        }
      })

      const workbook = new ExcelJS.Workbook()
      const worksheet = workbook.addWorksheet("SensorData (Monit)")
      worksheet.columns = [
        {
          header: "수신일시", key: "sensingDate", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "센서 ID", key: "sensorId", width: 10, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "센서명", key: "sensorName", width: 50, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "데이터", key: "data", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center',
              wrapText: true
            }
          },
        },
        {
          header: "단위", key: "sensorUnit", width: 10, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "감지상태", key: "type", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "조치 상태", key: "reportStatus", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "충전소&시설명", key: "stationName", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
        {
          header: "기관명", key: "equipmentName", width: 30, style: {
            alignment: {
              vertical: 'middle',
              horizontal: 'center'
            },
          }
        },
      ]

      worksheet.addRows(downloadData)
      // 다운로드 
      const mimeType = { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }
      const buffer = await workbook.xlsx.writeBuffer();const blob = new Blob([buffer], mimeType)
      saveAs(blob, "Monit data list.xlsx")
      setIsLoading(false)

    }
  }

  useEffect(() => {
    getInitData()
  }, [])

  const isEmpty = (count === 0)
  let items
  if (isEmpty) {
    items = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    items = list.map((item, key) => {
      return <CTableRow key={key} style={{ cursor: "pointer" }} onClick={() => showSideData(item)}>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.sensingDate)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensor?.id}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensor?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.data}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensor?.unit}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} style={filters.status.getTypeColor(item.type)} scope="row">
          {filters.status.convertToSensorDataStatus(item.type)}
        </CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.status.convertToReportStatus(item.reportStatus)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensor?.station?.name}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.sensor?.equipment?.name}</CTableDataCell>
      </CTableRow >
    })
  }

  const stationList = stations.map((item, key) => {
    return <option key={key} value={item?.id}>{item?.name}</option>
  })

  const equipmentList = equipments.map((item, key) => {
    return <option key={key} value={item.id}>{item.name}</option>
  })

  const sensorList = sensors.map((item, _) => {
    return { "value": item.id, "label": item.name }
  })

  return <>
    <CRow>
      <CCol md={leftCols}>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                <strong>검색 필터</strong>
              </CCardHeader>
              <CCardBody>
                <CRow className="g-3 align-items-center" >
                  <CCol xs={2}>
                    <CFormLabel className="col-form-label">
                      기간
                    </CFormLabel>
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput type="date" value={startDate} onChange={(e) => setStartDate(e.target.value)} />
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput type="date" value={endDate} onChange={(e) => setEndDate(e.target.value)} />
                  </CCol>
                  <CCol>
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="전체"
                      id="ALL_SENSING"
                      checked={sensingDate === 'ALL'}
                      onChange={() => changeSensingDate("ALL")}
                      onClick={() => changeSensingDate("ALL")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="1일"
                      id="DAY_SENSING"
                      checked={sensingDate === 'DAY'}
                      onChange={() => changeSensingDate("DAY")}
                      onClick={() => changeSensingDate("DAY")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="1주"
                      value="WEEK"
                      id="WEEK_SENSING"
                      checked={sensingDate === 'WEEK'}
                      onChange={() => changeSensingDate("WEEK")}
                      onClick={() => changeSensingDate("WEEK")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="1개월"
                      value="MONTH"
                      id="MONTH_SENSING"
                      checked={sensingDate === 'MONTH'}
                      onChange={() => changeSensingDate("MONTH")}
                      onClick={() => changeSensingDate("MONTH")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="6개월"
                      value="HALF"
                      id="HALF_SENSING"
                      checked={sensingDate === 'HALF'}
                      onChange={() => changeSensingDate("HALF")}
                      onClick={() => changeSensingDate("HALF")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="sensingDate"
                      autoComplete="off"
                      label="1년"
                      value="YEAR"
                      id="YEAR_SENSING"
                      checked={sensingDate === 'YEAR'}
                      onChange={() => changeSensingDate("YEAR")}
                      onClick={() => changeSensingDate("YEAR")} />
                  </CCol>
                </CRow>
                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                <CRow className="g-3 align-items-center">
                  <CCol xs={2}>
                    <CFormLabel className="col-form-label">
                      감지상태
                    </CFormLabel>
                  </CCol>
                  <CCol xs="auto">
                    <CFormCheck type="checkbox" inline id="typeCheckbox1" value="ALL" checked={checkAll === 'ALL'} label="전체" onChange={(e) => setCheckType(e)} />
                    <CFormCheck type="checkbox" inline id="typeCheckbox2" value="NORMAL" disabled={checkAll === 'ALL'} checked={check1 === 'NORMAL'} label="안전" onChange={(e) => setCheckType(e)} />
                    <CFormCheck type="checkbox" inline id="typeCheckbox3" value="WARNING" disabled={checkAll === 'ALL'} checked={check2 === 'WARNING'} label="경고" onChange={(e) => setCheckType(e)} />
                    <CFormCheck type="checkbox" inline id="typeCheckbox4" value="DANGER" disabled={checkAll === 'ALL'} checked={check3 === 'DANGER'} label="장애" onChange={(e) => setCheckType(e)} />
                    <CFormCheck type="checkbox" inline id="typeCheckbox5" value="SERVER_ERROR" disabled={checkAll === 'ALL'} checked={check4 === 'SERVER_ERROR'} label="서버오류" onChange={(e) => setCheckType(e)} />
                    <CFormCheck type="checkbox" inline id="typeCheckbox6" value="ROUTE_ERROR" disabled={checkAll === 'ALL'} checked={check5 === 'ROUTE_ERROR'} label="중계기 오류" onChange={(e) => setCheckType(e)} />
                  </CCol>
                </CRow>
                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                <CRow className="g-3 align-items-center">
                  <CCol xs={2}>
                    <CFormLabel className="col-form-label">
                      조치상태
                    </CFormLabel>
                  </CCol>
                  <CCol>
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="전체"
                      value="ALL"
                      id="ALL"
                      defaultChecked={reportType === 'ALL'}
                      onChange={() => changeReportType("ALL")}
                      onClick={() => changeReportType("ALL")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="처리전"
                      value="INCOMPLETE"
                      id="INCOMPLETE"
                      defaultChecked={reportType === 'INCOMPLETE'}
                      onChange={() => changeReportType("INCOMPLETE")}
                      onClick={() => changeReportType("INCOMPLETE")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="처리중"
                      value="CHECKING"
                      id="CHECKING"
                      defaultChecked={reportType === 'CHECKING'}
                      onChange={() => changeReportType("CHECKING")}
                      onClick={() => changeReportType("CHECKING")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="처리완료"
                      value="COMPLETE"
                      id="COMPLETE"
                      defaultChecked={reportType === 'COMPLETE'}
                      onChange={() => changeReportType("COMPLETE")}
                      onClick={() => changeReportType("COMPLETE")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="조치 보류"
                      value="DENIED"
                      id="DENIED"
                      defaultChecked={reportType === 'DENIED'}
                      onChange={() => changeReportType("DENIED")}
                      onClick={() => changeReportType("DENIED")} />
                    &nbsp;&nbsp;
                    <CFormCheck
                      button={{ color: 'info', variant: 'outline' }}
                      type="radio"
                      name="reportType"
                      autoComplete="off"
                      label="긴급상황"
                      value="EMERGENCY"
                      id="EMERGENCY"
                      defaultChecked={reportType === 'EMERGENCY'}
                      onChange={() => changeReportType("EMERGENCY")}
                      onClick={() => changeReportType("EMERGENCY")} />
                  </CCol>
                </CRow>
                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                {userInfo.roleType !== 'ROLE_ADMIN' &&
                  <CRow className="g-3 align-items-center">
                    <CCol xs={2}>
                      <CFormLabel className="col-form-label">
                        충전소&시설별
                      </CFormLabel>
                    </CCol>
                    <CCol xs="auto">
                      <CFormSelect
                        value={searchStation || '0'}
                        name="stations"
                        onChange={(e) => onChangeStation(e)}>
                        <option value="0">전체(충전소&시설 목록)</option>
                        {stationList}
                      </CFormSelect>
                    </CCol>
                  </CRow>
                }

                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                {userInfo.roleType !== 'ROLE_ADMIN' &&
                  <CRow className="g-3 align-items-center">
                    <CCol xs={2}>
                      <CFormLabel className="col-form-label">
                        기관별
                      </CFormLabel>
                    </CCol>
                    <CCol xs="auto">
                      <CFormSelect
                        value={searchEquipment || 'ALL'}
                        name="equipments"
                        onChange={(e) => onChangeEquipment(e)}>
                        <option value="0">전체(기관 목록)</option>
                        {equipmentList}
                      </CFormSelect>
                    </CCol>
                  </CRow>
                }
                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                <CRow className="g-3 align-items-center">
                  <CCol xs={2}>
                    <CFormLabel className="col-form-label">
                      센서별
                    </CFormLabel>
                  </CCol>
                  <CCol>
                    <Select
                      isMulti
                      onChange={(e) => onChangeMultiple(e)}
                      options={sensorList || []} />
                  </CCol>
                </CRow>
                <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
                <CRow className="g-3 align-items-center">
                  <CCol xs={2}>
                  </CCol>
                  <CCol xs={2}>
                    <CButton type="button" color="primary" onClick={() => search(1)}>검색</CButton>
                  </CCol>
                  <CCol xs="auto">
                    <CButton type="button" color="success" onClick={() => download()}>CSV Download</CButton>
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CRow>
          <CCol>
            <CCard className="mb-4">
              <CCardHeader>
                <strong>검색 결과</strong>
              </CCardHeader>
              <CCardBody>
                <CRow>
                  <LoadingOverlay
                    active={isLoading}
                    spinner
                    text='로딩중 입니다....'
                  >
                    <CCol md={12}>
                      <CTable align="middle" className="mb-0 border" hover responsive bordered>
                        <CTableHead color="light">
                          <CTableRow>
                            <CTableHeaderCell className={styles.tableFont} scope="col">수신일시</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">센서ID</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">센서명</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">데이터</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">단위</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">감지상태</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">조치상태</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">충전소&시설</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">기관</CTableHeaderCell>
                          </CTableRow>
                        </CTableHead>
                        <CTableBody>
                          {items}
                        </CTableBody>
                      </CTable>
                    </CCol>

                  </LoadingOverlay>
                </CRow>
              </CCardBody>
              <CCardFooter>
                <CRow>
                  <CCol md={7} >
                    (<strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
                  </CCol>
                  <CCol md={5} >
                    <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} viewType={"full"} />
                  </CCol>
                </CRow>
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>
      </CCol>
      {
        rightCols !== 0 &&
        <CCol>
          <CRow>
            <ReportModal
              showPopup={showPopup}
              setShowPopup={setShowPopup}
              updateId={0}
              refresh={showSideDataSub}
              sensorId={picData.id} />
            <LoadingOverlay
              active={isSubDataLoading}
              spinner
              text='로딩중 입니다....'
            >
              <CCol>
                <CCard className="mb-4">
                  <CCardHeader>
                    <strong>{picData?.sensor?.name}</strong>
                  </CCardHeader>
                  <CCardBody>
                    <CCol md={12}>
                      수신 데이터 : {picData?.data} {picData?.sensor?.unit}
                    </CCol>
                    <CCol md={12}>
                      감지상태 :  <span style={filters.status.getTypeColor(picData?.type)}>{filters.status.convertToSensorDataStatus(picData?.type)}</span>
                    </CCol>
                    <CCol md={12}>
                      조치상태 :
                      <a href="#" onClick={(e) => execPopup(e)}>
                        {filters.status.convertToReportStatus(picData?.reportStatus)}
                      </a>
                    </CCol>
                    <CCol md={12}>
                      사유 : {picData?.reportData || '없음'}
                    </CCol>
                    <CCol md={12} style={{ marginTop: "15px" }}>
                      최근 {picSubData?.dataList?.length || 0}건
                    </CCol>
                    <CCol md={12}>
                      <CTable align="middle" className="mb-0 border" hover responsive bordered>
                        <CTableHead color="light">
                          <CTableRow>
                            <CTableHeaderCell className={styles.tableFont} scope="col">수신일시</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">데이터</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">감지상태</CTableHeaderCell>
                          </CTableRow>
                        </CTableHead>
                        <CTableBody>
                          {
                            (picSubData?.dataList?.length === 0) ?
                              (<CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>) :
                              (
                                picSubData?.dataList?.map((item, key) => {
                                  return <CTableRow key={key}>
                                    <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.sensingDate)}</CTableDataCell>
                                    <CTableDataCell className={styles.tableFont} scope="row">{item.data}</CTableDataCell>
                                    <CTableDataCell className={styles.tableFont} scope="row" style={filters.status.getTypeColor(item.type)}>
                                      {filters.status.convertToSensorDataStatus(item.type)}
                                    </CTableDataCell>
                                  </CTableRow >
                                })
                              )
                          }
                        </CTableBody>
                      </CTable>
                    </CCol>
                    <CCol md={12} style={{ marginTop: "15px" }}>
                      1시간 : 총 {picSubData?.countAtAHour?.total}건 / 안전 {picSubData?.countAtAHour?.normal}건 / 경고 {picSubData?.countAtAHour?.warning}건 / 위험 {picSubData?.countAtAHour?.danger}건
                    </CCol>
                    <CCol md={12}>
                      24시간 : 총 {picSubData?.countAtADay?.total}건 / 안전 {picSubData?.countAtADay?.normal}건 / 경고 {picSubData?.countAtADay?.warning}건 / 위험 {picSubData?.countAtADay?.danger}건
                    </CCol>
                    <CCol md={12}>
                      7일 : 총 {picSubData?.countAtAWeek?.total}건 / 안전 {picSubData?.countAtAWeek?.normal}건 / 경고 {picSubData?.countAtAWeek?.warning}건 / 위험 {picSubData?.countAtAWeek?.danger}건
                    </CCol>
                    <CCol md={12} style={{ marginTop: "15px" }}>
                      센서 정보
                    </CCol>
                    <CCol md={12}>
                      위험 수치 : {picData?.sensor?.min} / {picData?.sensor?.max}
                    </CCol>
                    <CCol md={12}>
                      경고 수치 : {picData?.sensor?.warnMin} / {picData?.sensor?.warnMax}
                    </CCol>

                    <CCol md={12} style={{ marginTop: "15px" }}>
                      충전소&시설 : {picData?.sensor?.station?.name}
                    </CCol>
                    <CCol md={12}>
                      기관 : {picData?.sensor?.equipment?.name}
                    </CCol>
                    <CCol md={12}>
                      설치 장소 : {picData?.sensor?.station?.address1} {picData?.sensor?.station?.address2}
                    </CCol>
                    <CCol md={12}>
                      연락처 : {picSubData?.manager?.name} (연락처 : {picSubData?.manager?.mobileNumber})
                    </CCol>
                  </CCardBody>
                </CCard>
              </CCol>
            </LoadingOverlay>
          </CRow>
        </CCol>
      }
    </CRow >
  </>
}

export default SensorData
