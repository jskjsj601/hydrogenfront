import React, { useState, useEffect, useRef } from 'react'
import {
  CRow,
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CFormLabel,
  CFormInput,
  CFormSelect,
  CFormCheck,
  CButton,
  CTable,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CTableBody,
  CTableDataCell,
} from '@coreui/react-pro'
import {
  SensorLineChart,
  SensorPieChart
} from '../common'
import moment from 'moment'
import service from '../../modules/service'
import Select from 'react-select'
import LoadingOverlay from 'react-loading-overlay-ts'
import styles from '../../modules/css/apply.module.css'
import { DownloadTableExcel } from 'react-export-table-to-excel'

const DateData = (): JSX.Element => {
  const [isLoading, setIsLoading] = useState(false)
  const [stations, setStation] = useState<any>([])
  const [equipments, setEquipment] = useState<any>([])
  const [sensors, setSensor] = useState<any>([])
  const [dataList, setDataList] = useState<any>([])
  const [startDate, setStartDate] = useState(moment().subtract(3, "month").format("YYYY-MM"))
  const [endDate, setEndDate] = useState(moment(new Date()).format("YYYY-MM"))
  const [searchAllCheck, setCheckAll] = useState(false)
  const [searchStatistics, setStatistics] = useState("ALL")
  const [searchStatisticsType, setStatisticsType] = useState("STATION")
  const [searchDate, setSearchDate] = useState("MONTH")
  const [disableDate, setDisableDate] = useState(false)
  const [searchDataList, setSearchDataList] = useState<any>([])
  const [stasticsData, setStatisticsData] = useState<any>({})
  const [disabledCountType, setDisabledCountType] = useState(true)
  const [countType, setCountType] = useState("data")
  const [lineChartData, setLineChartData] = useState<any>([])
  const [pieChart, setPieChart] = useState<any>([])
  const tableRef = useRef(null)
  const onChangeDate = (event) => {
    setSearchDate(event.target.value)
    if (event.target.value === 'YEAR') {
      setStartDate(moment().subtract(3, "years").format("YYYY").toString())
      setEndDate(moment(new Date()).format("YYYY").toString())
    } else if (event.target.value === 'MONTH') {
      setStartDate(moment().subtract(3, "month").format("YYYY-MM").toString())
      setEndDate(moment(new Date()).format("YYYY-MM").toString())
    } else if (event.target.value === 'DAY') {
      setStartDate(moment().subtract(1, "day").format("YYYY-MM-DD").toString())
      setEndDate(moment(new Date()).format("YYYY-MM-DD").toString())
    } else if (event.target.value === 'QUARTER') {
      setStartDate(moment(new Date()).format("YYYY").toString())
      setEndDate("")
    } else if (event.target.value === 'HALF') {
      setStartDate(moment(new Date()).format("YYYY").toString())
      setEndDate("")
    }
  }

  const emptyFnc = () => {
    console.log("emptyFnc")
  }

  const onChangeMultiple = (multipleData) => {
    setSearchDataList(multipleData)
  }

  const onChangeCheckAll = (check) => {
    setCheckAll(check)
    setDisableDate(check)
    if (check) {
      setSearchDate("YEAR")
      setStartDate("2019")
      setEndDate("2021")
    }
  }

  const onChangeStatisticsType = (value) => {
    setSearchDataList([])
    setStatisticsType(value)
    if (value === 'STATION') {
      setDataList(stations.map((item, _) => {
        return { "value": item.id, "label": item.name }
      }))
    } else if (value === 'EQUIPMENT') {
      setDataList(equipments.map((item, _) => {
        return { "value": item.id, "label": item.name }
      }))
    } else if (value === 'SENSOR') {
      setDataList(sensors.map((item, _) => {
        return { "value": item.id, "label": item.name }
      }))
    }
  }

  const convertToDate = (value) => {
    if (value === 'YEAR') {
      return "년"
    } else if (value === 'MONTH') {
      return "월"
    } else if (value === 'DAY') {
      return "일"
    } else if (value === 'QUARTER') {
      return "반기"
    } else if (value === 'HALF') {
      return "분기"
    }
  }

  const convertToStatistics = (value) => {
    if (value === 'ALL') {
      return "전체 통계"
    } else if (value === 'PIC') {
      return "상세 통계"
    }
  }

  const showChart = (index) => {
    setLineChartDataFun(index, stasticsData, countType)
  }

  const setLineChartDataFun = (index, data, countType) => {
    const getData = data.dataList.map((item) => {
      return {
        label: item.date,
        countToNormal: countType === 'data' ? item.countList[index].countNormal : item.countList[index].countSensorNormal,
        countToWarning: countType === 'data' ? item.countList[index].countWarning : item.countList[index].countSensorWarning,
        countToDanger: countType === 'data' ? item.countList[index].countDanger : item.countList[index].countSensorDanger
      }
    })

    setLineChartData(getData)

    const getPieData = [
      getData.map((item) => item.countToNormal).reduce((a, b) => a + b, 0),
      getData.map((item) => item.countToWarning).reduce((a, b) => a + b, 0),
      getData.map((item) => item.countToDanger).reduce((a, b) => a + b, 0),
      0,
      0
    ]

    setPieChart(getPieData)
  }

  const setCountTypeFunc = (func) => {
    setCountType(func)
    setLineChartDataFun(0, stasticsData, func)

  }

  const search = () => {
    if (!searchAllCheck) {
      if (moment(startDate).valueOf() > moment(endDate).valueOf()) {
        alert("검색 시작일이 끝일보다 작을수 없습니다.")
        return
      }
    }

    if (searchStatistics === 'PIC') {
      if (searchDataList?.length === 0) {
        alert("상세 검색을 선택하시기 바랍니다.")
        return
      }
    }
    const params = {
      searchDate: searchDate,
      startDate: startDate,
      endDate: endDate,
      searchAllCheck: searchAllCheck,
      searchStatistics: searchStatistics,
      searchStatisticsType: searchStatisticsType,
      searchDataList: searchDataList.map((item, _) => {
        return item.value
      }).join(",")
    }
    setIsLoading(true)
    service.request.get(`admin/sensorDataStatistics/data`, params).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      setStatisticsData(data)
      setDisabledCountType(false)
      const picIndex = 0
      setLineChartDataFun(picIndex, data, countType)
    })

  }

  const download = () => {
    console.log("test")
  }

  const getInitData = () => {
    const params = {}
    service.request.get(`admin/sensor_data/init`, params).then(function (response) {
      const data = response.data.payload
      setStation(data?.stationList)
      setEquipment(data?.equipmentList)
      setSensor(data?.sensorList)
      setDataList(data?.stationList.map((item, _) => {
        return { "value": item.id, "label": item.name }
      }))
      
      search()
    })
  }

  useEffect(() => {
    getInitData()
  }, [])

  return <>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>검색 필터</strong>
          </CCardHeader>
          <CCardBody>
            <CRow className="g-3 align-items-center" >
              <CCol xs={2}>
                <CFormLabel className="col-form-label">
                  기간
                </CFormLabel>
              </CCol>
              <CCol xs={2}>
                <CFormSelect
                  value={searchDate || 'MONTH'}
                  name="searchDate"
                  onChange={(e) => onChangeDate(e)}
                  disabled={disableDate}>
                  <option value="YEAR">년</option>
                  <option value="MONTH">월</option>
                  <option value="DAY">일</option>
                  <option value="QUARTER">분기</option>
                  <option value="HALF">반기</option>
                </CFormSelect>
              </CCol>
              {(searchDate === 'YEAR') &&
                (<>
                  <CCol xs={2}>
                    <CFormSelect
                      value={startDate}
                      name="startDate"
                      onChange={(e) => setStartDate(e.target.value)}
                      disabled={disableDate}>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                    </CFormSelect>
                  </CCol>
                  <CCol xs={2}>
                    <CFormSelect
                      value={endDate}
                      name="endDate"
                      onChange={(e) => setEndDate(e.target.value)}
                      disabled={disableDate}>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                    </CFormSelect>
                  </CCol>
                </>)
              }
              {(searchDate === 'MONTH') &&
                (<>
                  <CCol xs={2}><CFormInput type="month" value={startDate} onChange={(e) => setStartDate(e.target.value)} /></CCol>
                  <CCol xs={2}><CFormInput type="month" value={endDate} onChange={(e) => setEndDate(e.target.value)} /></CCol>
                </>)
              }
              {(searchDate === 'DAY') &&
                (<>
                  <CCol xs={2}><CFormInput type="date" value={startDate} onChange={(e) => setStartDate(e.target.value)} /></CCol>
                  <CCol xs={2}><CFormInput type="date" value={endDate} onChange={(e) => setEndDate(e.target.value)} /></CCol>
                </>)
              }
              {(searchDate === 'QUARTER') &&
                (<>
                  <CCol xs={2}>
                    <CFormSelect
                      value={startDate}
                      name="startDate"
                      onChange={(e) => setStartDate(e.target.value)}
                      disabled={disableDate}>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                    </CFormSelect>
                  </CCol>
                </>)
              }
              {(searchDate === 'HALF') &&
                (<>
                  <CCol xs={2}>
                    <CFormSelect
                      value={startDate}
                      name="startDate"
                      onChange={(e) => setStartDate(e.target.value)}
                      disabled={disableDate}>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                    </CFormSelect>
                  </CCol>
                </>)
              }
              <CCol xs={2}>
                <CFormCheck type="checkbox" inline id="typeCheckbox1" value="ALL" label="전체" checked={searchAllCheck} onClick={() => onChangeCheckAll(!searchAllCheck)} onChange={() => emptyFnc()} />
              </CCol>
            </CRow>
            <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
            <CRow className="g-3 align-items-center" >
              <CCol xs={2}>
                <CFormLabel className="col-form-label">
                  감지상태
                </CFormLabel>
              </CCol>
              <CCol xs={4}>
                <CFormCheck
                  button={{ color: 'info', variant: 'outline' }}
                  type="radio"
                  name="statisticsAll"
                  autoComplete="off"
                  label="전체 통계"
                  id="ALL_SENSING"
                  checked={searchStatistics === 'ALL'}
                  onChange={() => setStatistics("ALL")}
                  onClick={() => setStatistics("ALL")}
                />
                &nbsp;&nbsp;
                <CFormCheck
                  button={{ color: 'info', variant: 'outline' }}
                  type="radio"
                  name="statisticsSpecific"
                  autoComplete="off"
                  label="상세 통계"
                  id="DAY_SENSING"
                  checked={searchStatistics === 'PIC'}
                  onChange={() => setStatistics("PIC")}
                  onClick={() => setStatistics("PIC")}
                />
                &nbsp;&nbsp;
              </CCol>
              {(searchStatistics === 'PIC') &&
                <CCol xs={3}>
                  <CFormSelect
                    value={searchStatisticsType}
                    name="searchStatisticsType"
                    onChange={(e) => onChangeStatisticsType(e.target.value)}>
                    <option value="STATION">충전소&시설별</option>
                    <option value="EQUIPMENT">기관별</option>
                    <option value="SENSOR">센서별</option>
                  </CFormSelect>
                </CCol>
              }
            </CRow>
            <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
            {(searchStatistics === 'PIC') &&
              <CRow className="g-3 align-items-center">
                <CCol xs={2}>
                  <CFormLabel className="col-form-label">
                    센서별
                  </CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <Select
                    isMulti
                    onChange={(e) => onChangeMultiple(e)}
                    options={dataList || []}
                    value={searchDataList} />
                </CCol>
              </CRow>
            }
            <CRow><CCol style={{ marginTop: "15px" }}></CCol></CRow>
            <CRow className="g-3 align-items-center">
              <CCol xs={2}>
              </CCol>
              <CCol xs={2}>
                <CButton type="button" color="primary" onClick={() => search()}>검색</CButton>
              </CCol>
              <CCol xs={2}>
                <DownloadTableExcel
                  filename="Monit Excel"
                  sheet="statistics"
                  currentTableRef={tableRef.current}
                >
                  <CButton type="button" color="success" onClick={() => download()}>CSV Download</CButton>
                </DownloadTableExcel>
              </CCol>
            </CRow>
          </CCardBody>
          <CCardFooter>
            <CRow className="g-3 align-items-center" >
              <CCol xs={2}>
                <CFormLabel className="col-form-label">
                  검색조건
                </CFormLabel>
              </CCol>
              <CCol xs={8}>
                기간 : {convertToDate(searchDate)}({startDate} ~ {endDate}), {convertToStatistics(searchStatistics)}
              </CCol>
            </CRow>
          </CCardFooter>
        </CCard>
      </CCol>
    </CRow>

    <CRow>
      <CCol>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>데이터 통계</strong>
          </CCardHeader>
          <CCardBody>
            <CRow>
              <CCol md={4} style={{ width: "260px" }}>
                <SensorPieChart inputData={pieChart} />
              </CCol>
              <CCol>
                <SensorLineChart data={lineChartData} height={"200"} />
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    <CRow>
      <CCol>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>차트값</strong>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <CFormCheck inline type="radio" name="dataCountType" id="dataCountType1" value="data" label="데이터 수" disabled={disabledCountType} checked={countType === 'data'} onClick={() => setCountTypeFunc("data")} />
            <CFormCheck inline type="radio" name="dataCountType" id="dataCountType2" value="sn" label="센서 수" disabled={disabledCountType} checked={countType === 'sn'} onClick={() => setCountTypeFunc("sn")} />
          </CCardHeader>
          <CCardBody>
            <CRow>
              <LoadingOverlay
                active={isLoading}
                spinner
                text='로딩중 입니다....'
              >
                <CCol md={12}>
                  <CTable align="middle" className="mb-0 border" hover responsive bordered ref={tableRef}>
                    <CTableHead color="light">
                      <CTableRow>
                        <CTableHeaderCell className={styles.tableFont} scope="col" style={{ verticalAlign: "middle" }} rowSpan={4}>기간</CTableHeaderCell>
                      </CTableRow>
                      <CTableRow>
                        {stasticsData?.header?.map((item, key) => {
                          return (<CTableHeaderCell key={key} onClick={() => showChart(key)} style={{ cursor: "pointer" }} className={styles.tableFont} scope="col" colSpan={6}>{item}</CTableHeaderCell>)
                        })}
                      </CTableRow>
                      <CTableRow>
                        {stasticsData?.header?.map((item, key1) => {
                          return (<React.Fragment key={key1}>
                            <CTableHeaderCell className={styles.tableFont} scope="col" colSpan={2}>정상</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col" colSpan={2}>경고</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col" colSpan={2}>장애</CTableHeaderCell>
                          </React.Fragment>)
                        })}
                      </CTableRow>
                      <CTableRow>
                        {stasticsData?.header?.map((item, key1) => {
                          return (<React.Fragment key={key1}>
                            <CTableHeaderCell className={styles.tableFont} scope="col">데이터</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">센서수</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">데이터</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">센서수</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">데이터</CTableHeaderCell>
                            <CTableHeaderCell className={styles.tableFont} scope="col">센서수</CTableHeaderCell>
                          </React.Fragment>)
                        })}
                      </CTableRow>
                    </CTableHead>
                    <CTableBody>
                      {stasticsData?.dataList?.map((item, key) => {
                        return (<CTableRow key={key}>
                          <CTableDataCell className={styles.tableFont} scope="row">{item.date}</CTableDataCell>
                          {item?.countList.map((sub, key_sub) => {
                            return (<React.Fragment key={key_sub}>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countNormal}</CTableDataCell>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countSensorNormal}</CTableDataCell>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countWarning}</CTableDataCell>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countSensorWarning}</CTableDataCell>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countDanger}</CTableDataCell>
                              <CTableDataCell className={styles.tableFont} scope="row">{sub.countSensorDanger}</CTableDataCell>
                            </React.Fragment>)
                          })}
                        </CTableRow>)
                      })}
                    </CTableBody>
                  </CTable>
                </CCol>

              </LoadingOverlay>
            </CRow>

          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default DateData
