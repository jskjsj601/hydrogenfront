import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CCardFooter,
  CForm,
  CFormLabel,
  CButton
} from '@coreui/react-pro'
import {
  Input,
  Select
} from '../../components/elements'
import CIcon from '@coreui/icons-react'
import { cilBackspace } from '@coreui/icons'
import { useParams, useHistory } from "react-router-dom"
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import { useFormik } from "formik"
import * as Yup from 'yup'
import { GoogleMaps } from '../../components/common'

const SensorDetail = (): JSX.Element => {
  const history = useHistory()
  const { id } = useParams<{ id: string }>()
  const [isLoading, setIsLoading] = useState(false)
  const [stationCenter, setStationCenter] = useState<any>(
    { lat: 37.2939663609542, lng: 127.000384919761 }
  )
  const [equipmentCenter, setEquipmentCenter] = useState<any>(
    { lat: 37.2939663609542, lng: 127.000384919761 }
  )
  const [stationLocations, setStationLocations] = useState<any>([])
  const [equipmentLocations, setEquipmentLocations] = useState<any>([])
  const [stationList, setStationList] = useState<any>([])
  const [equipmentList, setEquipmentList] = useState<any>([])
  const type = (id === "0") ? "create" : "update"
  const title = (id === "0") ? "등록" : "수정"


  const onBack = function () {
    if (window.confirm("입력을 취소하시겠습니까?")) {
      history.push("/department-management/sensor")
    }
  }


  const getData = function (Id) {
    setIsLoading(true)
    service.request.get(`/admin/sensor/${Id}`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      if (data) {
        formik.setFieldValue("name", data.name || '')
        formik.setFieldValue("status", data.status || 'USE')
        formik.setFieldValue("uuid", data.uuid || '')
        formik.setFieldValue("stationId", data.station?.id || '')
        formik.setFieldValue("equipmentId", data.equipment?.id || '')
        formik.setFieldValue("description", data.description || '')
        formik.setFieldValue("type", data.type || 'WARNING')
        formik.setFieldValue("min", data.min || 0)
        formik.setFieldValue("max", data.max || 0)
        formik.setFieldValue("warnMin", data.warnMin || 0)
        formik.setFieldValue("warnMax", data.warnMax || 0)
        formik.setFieldValue("unit", data.unit || '')

        const station = {
          lat: Number(data.station?.latitude || 0),
          lng: Number(data.station?.longitude || 0),
          title: data.station?.name || "",
        }
        const stationArray = [station]
        setStationCenter(station)
        setStationLocations(stationArray)

        const equipment = {
          lat: Number(data.equipment?.latitude || 0),
          lng: Number(data.equipment?.longitude || 0),
          title: data.equipment?.name || "",
        }
        const equipmentArray = [equipment]
        setEquipmentCenter(equipment)
        setEquipmentLocations(equipmentArray)

      }
    })
  }

  const getBasementData = function () {
    setIsLoading(true)
    service.request.get(`/admin/sensor/data`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      setEquipmentList(data.equipmentList || [])
      setStationList(data.stationList || [])
    })
  }


  const form = Yup.object().shape({
    name: Yup.string().required("센서명은 필수 입니다."),
    status: Yup.string().required("상태는 필수 입니다."),
    min: Yup.string().required("센서 최소값은 필수 입니다.").matches(
      /^[^0]\d*|^[^0]\d*\.{1}\d*[^0]$|^(0.)\d*[^0]$/,
      "센서 최소값의 형식이 잘못되었습니다."
    ),
    max: Yup.string().required("센서 최댓값은 필수 입니다.").matches(
      /^[^0]\d*|^[^0]\d*\.{1}\d*[^0]$|^(0.)\d*[^0]$/,
      "센서 최댓값의 형식이 잘못되었습니다."
    ),
    warnMin: Yup.string().required("센서 경고 최소값은 필수 입니다.").matches(
      /^[^0]\d*|^[^0]\d*\.{1}\d*[^0]$|^(0.)\d*[^0]$/,
      "센서 경고 최소값의 형식이 잘못되었습니다."
    ),
    warnMax: Yup.string().required("센서 경고 최댓값은 필수 입니다.").matches(
      /^[^0]\d*|^[^0]\d*\.{1}\d*[^0]$|^(0.)\d*[^0]$/,
      "센서 경고 최댓값의 형식이 잘못되었습니다."
    ),
    stationId: Yup.string().required("충전소&시설은 필수 입니다."),
    equipmentId: Yup.string().required("기관 회사는 필수 입니다."),
    unit: Yup.string().required("단위는 필수 입니다."),
  })

  const formik = useFormik({
    initialValues: {
      name: "",
      description: "",
      uuid: "",
      type: "WARNING",
      status: "USE",
      min: 0,
      max: 0,
      warnMin: 0,
      warnMax: 0,
      stationId: "",
      equipmentId: "",
      unit: ""
    },
    enableReinitialize: true,
    validationSchema: form,
    onSubmit: (values) => {
      if (type === "update") {
        if (window.confirm("센서를 수정하시겠습니까?")) {
          setIsLoading(true)
          service.request.put(`admin/sensor/${id}`, values).then(function () {
            setIsLoading(false)
            alert("센서 수정이 성공적으로 처리되었습니다.")
            getData(id)
          })
        }
      } else {
        if (window.confirm("센서를 등록하시겠습니까?")) {
          setIsLoading(true)
          service.request.post(`admin/sensor`, values).then(function () {
            setIsLoading(false)
            alert("센서가 성공적으로 처리되었습니다.")
            history.push("/department-management/sensor")
          })
        }
      }
    }
  })


  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getBasementData()
    if (type === "update") {
      getData(id)
    }

  }, [])

  return <>
    <CRow>
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <CButton type="button" color="info" style={{ color: '#FFF' }} id="button-addon2" onClick={() => onBack()}>
              <CIcon icon={cilBackspace} className="me-2" /><strong>센서 {title}</strong>
            </CButton>
          </CCardHeader>
          <CForm onSubmit={formik.handleSubmit}>
            <LoadingOverlay
              active={isLoading}
              spinner
              text='로딩중 입니다....'
            >
              <CCardBody>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>충전소&시설</CFormLabel>
                      <Select
                        idOrName={"stationId"}
                        formik={formik}
                        value={formik.values.stationId}
                        error={formik.errors.stationId}
                        touched={formik.touched.stationId}
                        defaultValue={"충전소&시설을 선택하여 주시기 바랍니다."}
                        items={stationList} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>기관회사</CFormLabel>
                      <Select
                        idOrName={"equipmentId"}
                        formik={formik}
                        value={formik.values.equipmentId}
                        error={formik.errors.equipmentId}
                        touched={formik.touched.equipmentId}
                        defaultValue={"기관회사를 선택하여 주시기 바랍니다."}
                        items={equipmentList} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서명</CFormLabel>
                      <Input
                        idOrName={"name"}
                        type={"text"}
                        placeholder={"센서명을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.name}
                        error={formik.errors.name}
                        touched={formik.touched.name} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서 설명</CFormLabel>
                      <Input
                        idOrName={"description"}
                        type={"text"}
                        placeholder={"센서 설명을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.description}
                        error={formik.errors.description}
                        touched={formik.touched.description} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>센서 아이디</CFormLabel>
                      <Input
                        idOrName={"uuid"}
                        type={"text"}
                        placeholder={"자동 입력 입니다."}
                        formik={formik}
                        value={formik.values.uuid}
                        error={formik.errors.uuid}
                        touched={formik.touched.uuid}
                        disabled={true} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>타입</CFormLabel>
                      <Select
                        idOrName={"type"}
                        formik={formik}
                        value={formik.values.type}
                        error={formik.errors.type}
                        touched={formik.touched.type}
                        items={[
                          { "id": "WARNING", "name": "경고 처리 센서" },
                          { "id": "DETECTING", "name": "감지 타입 센서" }
                        ]} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>상태</CFormLabel>
                      <Select
                        idOrName={"status"}
                        formik={formik}
                        value={formik.values.status}
                        error={formik.errors.status}
                        touched={formik.touched.status}
                        items={[
                          { "id": "USE", "name": "사용중" },
                          { "id": "STAND_BY", "name": "사용 대기중" },
                          { "id": "REPAIRING", "name": "수리중" },
                          { "id": "NOT_USE", "name": "사용중지" },
                          { "id": "ERROR", "name": "접속 불량" },
                        ]} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서 최소값</CFormLabel>
                      <Input
                        idOrName={"min"}
                        type={"number"}
                        placeholder={"센서 최소값을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.min}
                        error={formik.errors.min}
                        touched={formik.touched.min} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서 최대값</CFormLabel>
                      <Input
                        idOrName={"max"}
                        type={"number"}
                        placeholder={"센서 최대값을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.max}
                        error={formik.errors.max}
                        touched={formik.touched.max} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서 최소 경고값</CFormLabel>
                      <Input
                        idOrName={"warnMin"}
                        type={"number"}
                        placeholder={"센서 최소 경고값을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.warnMin}
                        error={formik.errors.warnMin}
                        touched={formik.touched.warnMin} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>센서 최대 경고값</CFormLabel>
                      <Input
                        idOrName={"warnMax"}
                        type={"number"}
                        placeholder={"센서 최대 경고값을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.warnMax}
                        error={formik.errors.warnMax}
                        touched={formik.touched.warnMax} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>단위 (CC,SP....)</CFormLabel>
                      <Input
                        idOrName={"unit"}
                        type={"text"}
                        placeholder={"단위를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.unit}
                        error={formik.errors.unit}
                        touched={formik.touched.unit} />
                    </div>
                  </CCol>

                </CRow>
              </CCardBody>
            </LoadingOverlay>

            <CCardFooter>
              {(type === 'update') ?
                (<CButton color="primary" type="submit">수정하기</CButton>) :
                (<CButton color="primary" type="submit">저장하기</CButton>)}
            </CCardFooter>
          </CForm>
        </CCard>
      </CCol>
    </CRow>
    {(type === 'update') ?
      <CRow>
        <CCol md={6}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>충전소&시설 위치</strong>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <GoogleMaps center={stationCenter || { lat: 37.2939663609542, lng: 127.000384919761 }} locations={stationLocations || []} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol md={6}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>기관 위치</strong>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <GoogleMaps center={equipmentCenter || { lat: 37.2939663609542, lng: 127.000384919761 }} locations={equipmentLocations || []} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      : (null)}
  </>
}

export default SensorDetail
