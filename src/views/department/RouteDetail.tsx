import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CCardFooter,
  CForm,
  CFormLabel,
  CButton,
  CInputGroup
} from '@coreui/react-pro'
import {
  Input,
  Select
} from '../../components/elements'
import CIcon from '@coreui/icons-react'
import { cilBackspace } from '@coreui/icons'
import { useParams, useHistory } from "react-router-dom"
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import { useFormik } from "formik"
import * as Yup from 'yup'
import { GoogleMaps } from '../../components/common'
import PostCodeModal from '../../components/modal/PostCodeModal'

const RouteDetail = (): JSX.Element => {
  const history = useHistory()
  const { id } = useParams<{ id: string }>()
  const [isLoading, setIsLoading] = useState(false)
  const [stationCenter, setStationCenter] = useState<any>(
    { lat: 37.2939663609542, lng: 127.000384919761 }
  )
  const [routerCenter, setRouterCenter] = useState<any>(
    { lat: 37.2939663609542, lng: 127.000384919761 }
  )
  const [stationList, setStationList] = useState<any>([])
  const [stationLocationList, setStationLocationList] = useState<any>([])
  const [routerList, setRouterList] = useState<any>([])
  const type = (id === "0") ? "create" : "update"
  const title = (id === "0") ? "등록" : "수정"
  const [showPostPopup, setShowPostPopup] = useState(false)

  const form = Yup.object().shape({
    name: Yup.string().required("중계기명은 필수 입니다."),
    status: Yup.string().required("상태는 필수 입니다."),
    type: Yup.string().required("타입은 필수 입니다."),
    address1: Yup.string().required("주소는 필수 입니다."),
    stationId: Yup.string().required("설치 장소는 필수 입니다."),
  })

  const onBack = function () {
    if (window.confirm("입력을 취소하시겠습니까?")) {
      history.push("/department-management/route")
    }
  }

  const getBasementData = function () {
    setIsLoading(true)
    service.request.get(`/admin/sensor/data`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      setStationList(data.stationList || [])
    })
  }

  const openSearchZipCode = function () {
    setShowPostPopup(true)
  }

  const handlePostcode = function (data) {
    formik.setFieldValue("address1", data.address)
    setShowPostPopup(false)
  }

  const getData = function (Id) {
    setIsLoading(true)
    service.request.get(`/admin/router/${Id}`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      if (data) {
        console.log(data)
        const centerInfo = {
          lat: Number(data.latitude),
          lng: Number(data.longitude),
          title: data.name,
        }
        const locationArray = [centerInfo]
        setRouterList(locationArray)
        setRouterCenter(centerInfo)

        const stationCenterInfo = {
          lat: Number(data?.station?.latitude),
          lng: Number(data?.station?.longitude),
          title: data?.station?.name,
        }

        const locationStationArray = [stationCenterInfo]
        setStationLocationList(locationStationArray)
        setStationCenter(stationCenterInfo)

        formik.setFieldValue("name", data.name || '')
        formik.setFieldValue("status", data.status || 'USE')
        formik.setFieldValue("type", data.type || 'SOFTWARE')
        formik.setFieldValue("address1", data.address1 || '')
        formik.setFieldValue("address2", data.address2 || '')
        formik.setFieldValue("stationId", data.station?.id || '')

        formik.setFieldValue("description", data.description || '')
        formik.setFieldValue("uuid", data.uuid || '')
      }
    })
  }

  const formik = useFormik({
    initialValues: {
      name: "",
      description: "",
      uuid: "",
      type: "SOFTWARE",
      status: "USE",
      stationId: "",
      address1: "",
      address2: ""
    },
    enableReinitialize: true,
    validationSchema: form,
    onSubmit: (values) => {
      console.log(values)
      if (type === "update") {
        if (window.confirm("중계기를 수정하시겠습니까?")) {
          setIsLoading(true)
          service.request.put(`admin/router/${id}`, values).then(function () {
            setIsLoading(false)
            alert("중계기 수정이 성공적으로 처리되었습니다.")
          })
        }
      } else {
        if (window.confirm("중계기를 등록하시겠습니까?")) {
          setIsLoading(true)
          service.request.post(`admin/router`, values).then(function () {
            setIsLoading(false)
            alert("중계기 등록이 처리되었습니다.")
            history.push("/department-management/route")
          })
        }
      }
    }
  })

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    getBasementData()
    if (type === "update") {
      getData(id)
    }
  }, [])

  return <>
    <CRow>
      <PostCodeModal
        showPopup={showPostPopup}
        setShowPopup={setShowPostPopup}
        handlePostcode={handlePostcode}
      />
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <CButton type="button" color="info" style={{ color: '#FFF' }} id="button-addon2" onClick={() => onBack()}>
              <CIcon icon={cilBackspace} className="me-2" /><strong>뒤로가기</strong>
            </CButton>
          </CCardHeader>
          <CForm onSubmit={formik.handleSubmit}>
            <LoadingOverlay
              active={isLoading}
              spinner
              text='로딩중 입니다....'
            >
              <CCardBody>
                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>충전소</CFormLabel>
                      <Select
                        idOrName={"stationId"}
                        formik={formik}
                        value={formik.values.stationId}
                        error={formik.errors.stationId}
                        touched={formik.touched.stationId}
                        defaultValue={"충전소를 선택하여 주시기 바랍니다."}
                        items={stationList} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>중계기 명</CFormLabel>
                      <Input
                        idOrName={"name"}
                        type={"text"}
                        placeholder={"중계기명을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.name}
                        error={formik.errors.name}
                        touched={formik.touched.name} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>중계기 설명</CFormLabel>
                      <Input
                        idOrName={"description"}
                        type={"text"}
                        placeholder={"중계기 설명을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.description}
                        error={formik.errors.description}
                        touched={formik.touched.description} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>중계기 아이디</CFormLabel>
                      <Input
                        idOrName={"uuid"}
                        type={"text"}
                        placeholder={"자동 입력 입니다."}
                        formik={formik}
                        value={formik.values.uuid}
                        error={formik.errors.uuid}
                        touched={formik.touched.uuid}
                        disabled={true} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>타입</CFormLabel>
                      <Select
                        idOrName={"type"}
                        formik={formik}
                        value={formik.values.type}
                        error={formik.errors.type}
                        touched={formik.touched.type}
                        items={[
                          { "id": "SOFTWARE", "name": "소프트웨어 타입" },
                          { "id": "HARDWARE", "name": "하드웨어 타입" },
                          { "id": "ETC", "name": "기타" }
                        ]} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>상태</CFormLabel>
                      <Select
                        idOrName={"status"}
                        formik={formik}
                        value={formik.values.status}
                        error={formik.errors.status}
                        touched={formik.touched.status}
                        items={[
                          { "id": "USE", "name": "사용중" },
                          { "id": "REPAIR", "name": "수리중" },
                          { "id": "ERROR", "name": "중계기 에러" },
                          { "id": "NOT_USE", "name": "사용중지" }
                        ]} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>설치 주소</CFormLabel>
                      <CInputGroup className="mb-3">
                        <Input
                          idOrName={"address1"}
                          type={"text"}
                          placeholder={"설치 주소를 선택하여 주세요"}
                          formik={formik}
                          value={formik.values.address1}
                          error={formik.errors.address1}
                          touched={formik.touched.address1}
                          disabled={true} />
                        <CButton
                          type="button"
                          color="success"
                          style={{ alignContent: 'right', color: '#FFF' }}
                          onClick={openSearchZipCode} >주소 불러오기</CButton>
                      </CInputGroup>
                    </div>
                  </CCol>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>상세 주소</CFormLabel>
                      <Input
                        idOrName={"address2"}
                        type={"text"}
                        placeholder={"상세주소를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.address2}
                        error={formik.errors.address2}
                        touched={formik.touched.address2} />

                    </div>
                  </CCol>
                </CRow>
                <CRow>


                </CRow>
              </CCardBody>
            </LoadingOverlay>

            <CCardFooter>
              {(type === 'update') ?
                (<CButton color="primary" type="submit">수정하기</CButton>) :
                (<CButton color="primary" type="submit">저장하기</CButton>)}
            </CCardFooter>
          </CForm>
        </CCard>
      </CCol>
    </CRow>
    {
      (type === 'update') ?
        <CRow>
          <CCol md={6}>
            <CCard className="mb-4">
              <CCardHeader>
                <strong>충전소 위치</strong>
              </CCardHeader>
              <CCardBody>
                <CRow>
                  <CCol>
                    <GoogleMaps center={stationCenter || { lat: 37.2939663609542, lng: 127.000384919761 }} locations={stationLocationList || [
                      { lat: 37.2939663609542, lng: 127.000384919761 }
                    ]} />
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
          <CCol md={6}>
            <CCard className="mb-4">
              <CCardHeader>
                <strong>실제 설치 위치</strong>
              </CCardHeader>
              <CCardBody>
                <CRow>
                  <CCol>
                    <GoogleMaps center={routerCenter || { lat: 37.2939663609542, lng: 127.000384919761 }} locations={routerList || [
                      { lat: 37.2939663609542, lng: 127.000384919761 }
                    ]} />
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        : (null)
    }
  </>
}

export default RouteDetail