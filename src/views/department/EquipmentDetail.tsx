import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CCardFooter,
  CForm,
  CFormLabel,
  CInputGroup,
  CButton
} from '@coreui/react-pro'
import {
  Input,
  Select
} from '../../components/elements'
import CIcon from '@coreui/icons-react'
import { cilBackspace } from '@coreui/icons'
import { useParams, useHistory } from "react-router-dom"
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import PostCodeModal from '../../components/modal/PostCodeModal'
import { useFormik } from "formik"
import * as Yup from 'yup'
import { GoogleMaps } from '../../components/common'
import UserListComponent from './UserListComponent'

const EquipmentDetail = (): JSX.Element => {
  const history = useHistory()
  const { id } = useParams<{ id: string }>()
  const [showPostPopup, setShowPostPopup] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [center, setCenter] = useState<any>(
    { lat: 37.2939663609542, lng: 127.000384919761 }
  )
  const [locations, setLocations] = useState<any>([])
  const type = (id === "0") ? "create" : "update"
  const title = (id === "0") ? "등록" : "수정"


  const onBack = function () {
    if (window.confirm("입력을 취소하시겠습니까?")) {
      history.push("/department-management/equipment")
    }
  }

  const openSearchZipCode = function () {
    setShowPostPopup(true)
  }

  const getData = function (Id) {
    setIsLoading(true)
    service.request.get(`/admin/equipment/${Id}`, {}).then(function (response) {
      setIsLoading(false)
      const data = response.data.payload
      if (data) {
        const centerInfo = {
          lat: Number(data.latitude),
          lng: Number(data.longitude),
          title: data.name,
        }
        const locationArray = [centerInfo]
        setLocations(locationArray)
        setCenter(centerInfo)
        formik.setFieldValue("name", data.name || '')
        formik.setFieldValue("status", data.status || 'USE')
        formik.setFieldValue("address1", data.address1 || '')
        formik.setFieldValue("address2", data.address2 || '')
        formik.setFieldValue("postCode", data.postCode || '')
        formik.setFieldValue("phoneNumber", data.phoneNumber || '')
      }
    })
  }

  const onKeyPressNumber = function (value) {
    value = value
      .replace(/[^0-9]/g, '')
      .replace(/^(\d{0,3})(\d{0,4})(\d{0,4})$/g, "$1-$2-$3").replace(/(-{1,2})$/g, "")
    formik.setFieldValue("phoneNumber", value)
  }

  const form = Yup.object().shape({
    name: Yup.string().required("기관명은 필수 입니다."),
    phoneNumber: Yup.string().required("전화번호는 필수 입니다.").matches(
      /^(0[1-9][01346-9])-?([1-9]{1}[0-9]{2,3})-?([0-9]{4})$/,
      "연락처의 형식이 잘못되었습니다.."
    ),
    address1: Yup.string().required("주소는 필수 입니다."),
    status: Yup.string().required("주소는 필수 입니다."),
  })

  const formik = useFormik({
    initialValues: {
      name: "",
      phoneNumber: "",
      address1: "",
      address2: "",
      postCode: "",
      status: "USE",
    },
    enableReinitialize: true,
    validationSchema: form,
    onSubmit: (values) => {
      if (type === "update") {
        if (window.confirm("기관명을 수정하시겠습니까?")) {
          setIsLoading(true)
          service.request.put(`admin/equipment/${id}`, values).then(function () {
            setIsLoading(false)
            alert("기관명 수정이 성공적으로 처리되었습니다.")
          })
        }
      } else {
        if (window.confirm("기관명을 등록하시겠습니까?")) {
          setIsLoading(true)
          service.request.post(`admin/equipment`, values).then(function () {
            setIsLoading(false)
            alert("기관명을 성공적으로 처리되었습니다.")
            history.push("/department-management/equipment")
          })
        }
      }
    }
  })

  const handlePostcode = function (data) {
    formik.setFieldValue("postCode", data.zonecode)
    formik.setFieldValue("address1", data.address)
    setShowPostPopup(false)
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    if (type === "update") {
      getData(id)
    }
  }, [])

  return <>
    <CRow>
      <PostCodeModal
        showPopup={showPostPopup}
        setShowPopup={setShowPostPopup}
        handlePostcode={handlePostcode}
      />
      <CCol md={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <CButton type="button" color="info" style={{ color: '#FFF' }} id="button-addon2" onClick={() => onBack()}>
              <CIcon icon={cilBackspace} className="me-2" /><strong>기관 {title}</strong>
            </CButton>
          </CCardHeader>
          <CForm onSubmit={formik.handleSubmit}>
            <LoadingOverlay
              active={isLoading}
              spinner
              text='로딩중 입니다....'
            >
              <CCardBody>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>기관명</CFormLabel>
                      <Input
                        idOrName={"name"}
                        type={"text"}
                        placeholder={"기관 회사명을 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.name}
                        error={formik.errors.name}
                        touched={formik.touched.name} />
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>연락처</CFormLabel>
                      <Input
                        idOrName={"phoneNumber"}
                        type={"text"}
                        placeholder={"연락처를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.phoneNumber}
                        error={formik.errors.phoneNumber}
                        touched={formik.touched.phoneNumber}
                        onKeyPressNumber={onKeyPressNumber} />
                    </div>

                  </CCol>
                </CRow>
                <CRow>
                  <CCol >
                    <div className="mb-3">
                      <CFormLabel>주소</CFormLabel>
                      <CInputGroup className="mb-3">
                        <Input
                          idOrName={"address1"}
                          type={"text"}
                          placeholder={"주소를 입력하여 주세요"}
                          formik={formik}
                          value={formik.values.address1}
                          error={formik.errors.address1}
                          touched={formik.touched.address1}
                          disabled={true} />

                        <CButton
                          type="button"
                          color="success"
                          style={{ alignContent: 'right', color: '#FFF' }}
                          onClick={openSearchZipCode} >주소 불러오기</CButton>
                      </CInputGroup>
                    </div>
                  </CCol>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>우편번호</CFormLabel>
                      <Input
                        idOrName={"postCode"}
                        type={"text"}
                        placeholder={"우편번호를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.postCode}
                        error={formik.errors.postCode}
                        touched={formik.touched.postCode}
                        disabled={true} />
                    </div>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol>
                    <div className="mb-3">
                      <CFormLabel>상세 주소</CFormLabel>
                      <Input
                        idOrName={"address2"}
                        type={"text"}
                        placeholder={"상세주소를 입력하여 주세요"}
                        formik={formik}
                        value={formik.values.address2}
                        error={formik.errors.address2}
                        touched={formik.touched.address2} />
                    </div>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol md={6}>
                    <div className="mb-3">
                      <CFormLabel>상태</CFormLabel>
                      <Select
                        idOrName={"status"}
                        formik={formik}
                        value={formik.values.status}
                        error={formik.errors.status}
                        touched={formik.touched.status}
                        items={[
                          { "id": "USE", "name": "사용중" },
                          { "id": "NOT_USE", "name": "사용중지" }
                        ]} />
                    </div>
                  </CCol>
                </CRow>
              </CCardBody>
            </LoadingOverlay>

            <CCardFooter>
              {(type === 'update') ?
                (<CButton color="primary" type="submit">수정하기</CButton>) :
                (<CButton color="primary" type="submit">저장하기</CButton>)}
            </CCardFooter>
          </CForm>
        </CCard>
      </CCol>
    </CRow>
    {(type === 'update') ?
      <CRow>
        <CCol md={6}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>기관 회사 위치</strong>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <GoogleMaps center={center || { lat: 37.2939663609542, lng: 127.000384919761 }} locations={locations || []} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol md={6}>
          <UserListComponent type={'equipment'} id={id} />
        </CCol>
      </CRow>
      : (null)}
  </>
}

export default EquipmentDetail
