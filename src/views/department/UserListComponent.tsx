import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CFormSelect,
  CInputGroup,
  CFormInput,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CCardFooter,
} from '@coreui/react-pro'
import LoadingOverlay from 'react-loading-overlay-ts'
import Pagination from '../../components/Pagination'
import service from "../../modules/service"
import filters from "../../modules/filters"
import styles from '../../modules/css/apply.module.css'
import { ManagerModal } from '../../components/modal'

const UserListComponent = ({ type, id }): JSX.Element => {
  const max = 10
  const [count, setCount] = useState(0)
  const [list, setList] = useState<any>([])
  const [isLoading, setIsLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [showPopup, setShowPopup] = useState(false)
  const [updateId, setUpdateId] = useState(0)

  const [searchStatus, setSearchStatus] = useState("ALL")
  const [searchType, setSearchType] = useState("ALL")
  const [searchText, setSearchText] = useState("")


  const onChangePage = (currentPage) => {
    setPage(currentPage)
    getList(currentPage, max, searchStatus, searchType, searchText)
  }

  const search = function () {
    setPage(1)
    getList(1, max, searchStatus, searchType, searchText)
  }

  const toDetail = function (e, id) {
    e.preventDefault()
    setUpdateId(id)
    setShowPopup(true)
  }

  const getList = function (page, max, searchStatus, searchType, searchText) {
    const params = {
      page: page,
      max: max,
      searchStatus: searchStatus,
      searchType: searchType,
      searchText: searchText,
    }
    setIsLoading(true)
    service.request.get(`admin/manager/${type}/${id}`, params).then(function (response) {
      const data = response.data.payload
      setList(data.content)
      setCount(data.totalElements)
      setIsLoading(false)
    })
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    search()
  }, [])

  const isEmpty = (count === 0)
  let items
  if (isEmpty) {
    items = <CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>
  } else {
    items = list.map((item, key) => {
      return <CTableRow key={key}>
        <CTableDataCell className={styles.tableFont} scope="row">{item.id}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row"><Link to="#" onClick={(e) => toDetail(e, item.id)}>{item.name}</Link></CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.levelName}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.email}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{item.mobileNumber}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.status.convertToStatus(item.status)}</CTableDataCell>
        <CTableDataCell className={styles.tableFont} scope="row">{filters.date.lastModifiedDate(item.createDate, item.updateDate)}</CTableDataCell>
      </CTableRow >
    })
  }

  return <>
    <ManagerModal
      showPopup={showPopup}
      setShowPopup={setShowPopup}
      updateId={updateId}
      search={search}
      setUpdateId={setUpdateId}
      type={type}
      baseId={id} />
    <CCard className="mb-4">
      <CCardHeader>
        <strong>관리자 관리</strong>
      </CCardHeader>
      <CCardBody>
        <CRow>
          <CCol md={2}>
            <CButton component="input" type="button" color="success" style={{ color: "#FFF" }} value="관리자 추가" onClick={(e) => toDetail(e, 0)} />
          </CCol>
          <CCol md={3}></CCol>
          <CCol md={2}>
            <CInputGroup className="mb-3">
              <CFormSelect
                value={searchStatus}
                name="searchStatus"
                onChange={e => setSearchStatus(e.target.value)}>
                <option value="ALL">상태</option>
                <option value="USE">사용중</option>
                <option value="NOT_USE">사용중지</option>
              </CFormSelect>
            </CInputGroup>
          </CCol>
          <CCol md={2}>
            <CInputGroup className="mb-3">
              <CFormSelect
                value={searchType}
                name="searchType"
                onChange={e => setSearchType(e.target.value)}>
                <option value="ALL">키워드</option>
                <option value="NAME">이름</option>
                <option value="MOBILE_NUMBER">전화번호</option>
                <option value="EMAIL">이메일</option>
              </CFormSelect>
            </CInputGroup>
          </CCol>
          <CCol md={3}>
            <CInputGroup className="mb-3">
              <CFormInput
                placeholder="검색어 입력"
                value={searchText}
                name="searchText"
                onChange={e => setSearchText(e.target.value)} />
              <CButton type="button" color="primary" id="button-addon2" onClick={() => search()}>검색</CButton>
            </CInputGroup>
          </CCol>

        </CRow>
        <CRow>
          <LoadingOverlay
            active={isLoading}
            spinner
            text='로딩중 입니다....'
          >
            <CCol md={12}>
              <CTable align="middle" className="mb-0 border" hover responsive bordered>
                <CTableHead color="light">
                  <CTableRow>
                    <CTableHeaderCell className={styles.tableFont} scope="col">ID</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">이름</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">직급</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">이메일</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">전화번호</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">상태</CTableHeaderCell>
                    <CTableHeaderCell className={styles.tableFont} scope="col">최종 업데이트 일</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {items}
                </CTableBody>
              </CTable>
            </CCol>

          </LoadingOverlay>
        </CRow>
      </CCardBody>
      <CCardFooter>
        <CRow>
          <CCol xs={5}>
            ( <strong>총</strong> : {count} 행,  <strong>현재 페이지</strong> {page} / {Math.ceil(count / max)} Pages)
          </CCol>
          <CCol md={7} >
            <Pagination currentPage={page} totalElements={count} onChangePage={onChangePage} />
          </CCol>
        </CRow>
      </CCardFooter>
    </CCard>
  </>
}

export default UserListComponent
