import service from "../service"
const getAuthState = function() {
  let isLogined = false
  let userInfo = {}
  if(localStorage.getItem("isLogined")) {
    if(localStorage.getItem('isLogined')  === 'true') {
      isLogined = true
    }
  }

  if(localStorage.getItem('userInfo')) {
    userInfo = localStorage.getItem('userInfo')
  }
  return {
    isLogined: isLogined,
    userID: localStorage.getItem('userID'),
    userInfo: userInfo
  }
}

const login = function(username, password) {
  const url = "/admin/auth"
  const params = new URLSearchParams()
  params.append('j_username', username)  
  params.append('j_password', password)

  return service.request.post_params(url, params).then(function (response) {
    if(response) {
      const data = response.data
      if(data.status === 200) {
        const userInfo = data.payload
        localStorage.removeItem("checkTime")
        localStorage.removeItem('isLogined')
        localStorage.removeItem('userID')
        localStorage.removeItem('userInfo')
        localStorage.setItem('isLogined', true)
        localStorage.setItem('userID', userInfo.username)
        localStorage.setItem('userInfo', JSON.stringify(userInfo))
      } else {
        localStorage.removeItem('isLogined')
        localStorage.removeItem('userID')
        localStorage.removeItem('userInfo')
      }
      return data
    } else {
      localStorage.removeItem("checkTime")
      localStorage.removeItem('isLogined')
      localStorage.removeItem('userID')
      localStorage.removeItem('userInfo')
      return null
    }
    
  })
}

const logout = function() {
  const url = "/admin/logout"
  return service.request.post(url).then(function (response) {
    if(response) {
      const data = response.data
      return data
    } 
    return null
  })
}

const reset = function() {
  localStorage.removeItem("checkTime")
  localStorage.removeItem('isLogined')
  localStorage.removeItem('userID')
  localStorage.removeItem('userInfo')
  
}

const authentication = {
  getAuthState,
  login,
  logout,
  reset
}

export default authentication