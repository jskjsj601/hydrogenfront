import moment from 'moment'

const lastModifiedDate = function (createDate, updateDate) {
    const dateFormat = 'YYYY-MM-DD HH:mm:ss'
    if (updateDate) {
        return moment(updateDate).format(dateFormat)
    }
    if (createDate) {
        return moment(createDate).format(dateFormat)
    } else {
        return ''
    }
}

const sensingDate = function (createDate) {
  const dateFormat = 'MM-DD HH:mm:ss'
  if (createDate) {
    return moment(createDate).format(dateFormat)
  } else {
    return ''
  }
}


const date = {
    lastModifiedDate,
  sensingDate
}

export default date

