import DateFilters from './dateFilter'
import StatusFilters from './statusFilter'

const filters = {
  date: DateFilters,
  status: StatusFilters
}

export default filters
