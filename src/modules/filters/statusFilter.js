import {
  safetyCSS,
  warningCSS,
  dangerCSS
} from '../styles/styles'

const convertToStatus  = function (status) {
  let str = ""
  if(status === 'USE') {
    str = "사용중"
  } else if(status === 'NOT_USE') {
    str = "사용중지"
  }
  return str
}

const convertToUserStatus = function(status) {
  let str = ""
  if(status === 'USE') {
    str = "사용중"
  } else if(status === 'NOT_USE') {
    str = "사용중지"
  } else if(status === 'FORCE_CHECKOUT') {
    str = "강제 사용중지"
  } else if(status === 'LOCKED') {
    str = "잠금상태"
  }
  return str
}

const convertToSensorStatus = function(status) {
  let str = ""
  if(status === 'USE') {
    str = "사용중"
  } else if(status === 'NOT_USE') {
    str = "사용중지"
  } else if(status === 'STAND_BY') {
    str = "사용 대기중"
  } else if(status === 'REPARING') {
    str = "수리중"
  } else if(status === 'ERROR') {
    str = "접속 불량"
  }
  return str
}

const convertToRouterStatus = function(status) {
  let str = ""
  if(status === 'USE') {
    str = "사용중"
  } else if(status === 'NOT_USE') {
    str = "사용중지"
  } else if(status === 'REPAIR') {
    str = "수리중"
  } else if(status === 'ERROR') {
    str = "접속 불량"
  }
  return str
}
const convertToRoleType = function(status) {
  let str = ""
  if(status === 'ROLE_USER') {
    str = "일반 유저"
  } else if(status === 'ROLE_ADMIN') {
    str = "관리자"
  } else if(status === 'ROLE_SUPER_MANAGER') {
    str = "슈퍼 관리자"
  }
  return str
}

const convertToSensorType = function(status) {
  let str = ""
  if(status === 'WARNING') {
    str = "경고 처리 센서"
  } else if(status === 'DETECTING') {
    str = "감지 타입 센서"
  }
  return str
}

const convertToRouterType = function(status) {
  let str = "기타"
  if(status === 'SOFTWARE') {
    str = "소프트웨어 타입"
  } else if(status === 'HARDWARE') {
    str = "하드웨어 타입"
  }
  return str
}

const convertToSensorDataStatus = function(status) {
  let str = "없음"
  if(status === 'LOW_WARNING') {
    str = '낮은값 위험'
  } else if (status === 'HIGH_WARNING') {
    str = '높은값 위험'
  } else if (status === 'LOW_DANGER') {
    str = "낮은값 장애"
  } else if (status === 'HIGH_DANGER')  {
    str = "높은값 장애"
  } else if (status === 'NORMAL') {
    str = "안전"
  }
  return str
}

const convertToSensorDataTypeToColor = function(status) {
  let str = "없음"
  if(status === 'LOW_WARNING' || status === 'HIGH_WARNING') {
    str = 'warning'
  }  else if (status === 'LOW_DANGER' || status === 'HIGH_DANGER') {
    str = "danger"
  } else if (status === 'NORMAL') {
    str = "success"
  }
  return str
}


const getTypeColor = function(status) {
  if(status === 'LOW_WARNING' || status === 'HIGH_WARNING') {
    return warningCSS
  } else if (status === 'LOW_DANGER' || status === 'HIGH_DANGER') {
    return dangerCSS
  }
  return safetyCSS
}

const convertToReportStatus = function(status) {
  let str = ""
  if(status === 'INCOMPLETE') {
    str = "처리전"
  } else if (status === 'CHECKING') {
    str = "처리중"
  } else if(status === 'COMPLETE') {
    str = "처리완료"
  } else if(status === 'WARNING_COMPLETE') {
    str = "경고 처리 완료"
  } else if(status === 'DANGER_COMPLETE') {
    str = "위험 처리 완료"
  } else if(status === 'EMERGENCY') {
    str = "긴급상황"
  } else if(status === 'DENIED') {
    str = "조치 보류"
  }
  return str
}

const getReportColor = function(status) {
  if(status === 'INCOMPLETE') {
    return "light"
  } else if(status === 'COMPLETE') {
    return "success"
  } else if(status === 'EMERGENCY') {
    return "danger"
  }
}

const convertToTimeUnit = function(type) {
  let str = ""
  if(type === 'minute') {
    str = "30분 이내"
  } else if(type === 'hour') {
    str = "1시간 이내"
  } else if(type === 'day') {
    str = "오늘자"
  } else if(type === 'week') {
    str = "이번주"
  } else if(type === 'month') {
    str = "이번달"
  }
  return str
}

const status = {
  convertToStatus,
  convertToUserStatus,
  convertToRoleType,
  convertToSensorStatus,
  convertToSensorType,
  convertToSensorDataStatus,
  getTypeColor,
  convertToReportStatus,
  convertToTimeUnit,
  convertToSensorDataTypeToColor,
  getReportColor,
  convertToRouterType,
  convertToRouterStatus
}

export default status

