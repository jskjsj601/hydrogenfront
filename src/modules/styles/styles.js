const safetyCSS = {
  color: "#0faf24",
  fontSize: "14px", 
  fontWeight: "bold",
}

const warningCSS = {
  color: "#F8A711",
  fontSize: "14px", 
  fontWeight: "bold", 
}

const dangerCSS = {
  color: "#E04646",
  fontSize: "14px", 
  fontWeight: "bold", 
}


export {
  safetyCSS,
  warningCSS,
  dangerCSS
}