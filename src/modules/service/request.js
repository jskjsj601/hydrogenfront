import axios from 'axios'
const LOGIN_STATUS_CODE = {
  SUCCESSFUL: 200,                      // 성공
  UNAUTHORIZED: 401,                    // 인증실패
  USER_NOT_USED: 406,                   // 유저 사용 불가
  USER_IS_LOCK: 402,                    // 유저 잠금 상태
  USER_IS_NOT_ALLOWS: 405,              // 유저 사용 허용 불가
  ERROR_IS_DB_FAILED: 501,              // DB 등록,수정,삭제 중 에러
  ERROR_IS_SERVER_ERROR: 500,           // 서버에러
  ERROR_IS_INPUT_PARAMS_INVALID: 503    // 입력 파라메터 오류
}

const instanceAxios = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL,
  withCredentials: true
})

// Add a response interceptor
instanceAxios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  // Do something with response error
  if (!error.response) {
    window.location.href = "/#/error"
    return
  }
  if(!error.response.status) {
    window.location.href = "/#/error"
  }
  const status = error.response.status
  if (status === LOGIN_STATUS_CODE.UNAUTHORIZED) {
    window.location.href = "/#/login"
  } else if (status === LOGIN_STATUS_CODE.ERROR_IS_INPUT_PARAMS_INVALID) {
    alert('입력된 파라메터의 검증오류로 인해 로그아웃을 실행합니다. \r\n 관리자에게 문의하여 주시기 바랍니다.')
    window.location.href = "/#/error"
  } else if (status === LOGIN_STATUS_CODE.ERROR_IS_DB_FAILED) {
    alert('데이터 입력중 오류가 발생하였습니다. \r\n 관리자에게 문의하여 주시기 바랍니다.')
    window.location.href = "/#/error"
  } else {
    // alert('서버와의 통신중 에러가 발생하였습니다. \r\n 관리자에게 문의하여 주시기 바랍니다.')
    // window.location.href = "/#/error"
  }
  return Promise.reject(error)
})

const methods = {
  post (url, params) {
    return instanceAxios.post(url, params, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
  },
  post_params (url, params) {
    return instanceAxios.post(url, params)
  },
  get (url, params) {
    return instanceAxios.get(url, {params: params},  {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
  },
  put (url, params) {
    return instanceAxios.put(url, params, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
  },
  delete (url, params) {
    return instanceAxios.delete(url, params, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
  },
  upload_post(url, form) {
    return instanceAxios.post(url, form, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  baseURL(path) {
    return process.env.REACT_APP_SERVER_URL + "/admin/file/download?path=" + path
  }
}

export default methods
