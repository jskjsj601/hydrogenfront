import React, { ReactNode } from 'react'

export type route = {
  component?: ReactNode
  exact?: boolean
  name?: string
  path?: string
  routes?: route[]
}

// 모니터링
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const RealtimeContent = React.lazy(() => import('./views/dashboard/RealtimeContent'))
const RouterContent = React.lazy(() => import('./views/dashboard/RouterContent'))
const SensorContent = React.lazy(() => import('./views/dashboard/SensorContent'))

// 센서 관제
const StationMonitoring = React.lazy(() => import('./views/monitoring/Station'))
const StationNewMonitoring = React.lazy(() => import('./views/monitoring/Station-new'))
const StationNewDetail = React.lazy(() => import('./views/monitoring/Station-new-detail'))
const CompanyMonitoring = React.lazy(() => import('./views/monitoring/Company'))
const SensorMonitoring = React.lazy(() => import('./views/monitoring/Sensor'))
const ReportMonitoring = React.lazy(() => import('./views/monitoring/Report'))
const EquipmentDetailNew = React.lazy(() => import('./views/department/EquipmentDetailNew'))

// 이력관리
const StationLog = React.lazy(() => import('./views/logs/Station'))
const CompanyLog = React.lazy(() => import('./views/logs/Company'))
const SensorLog = React.lazy(() => import('./views/logs/Sensor'))

// 게시판
const Board = React.lazy(() => import('./views/board/Board'))
const BoardDetail = React.lazy(() => import('./views/board/BoardDetail'))
const Notice = React.lazy(() => import('./views/board/Notice'))
const NoticeDetail = React.lazy(() => import('./views/board/NoticeDetail'))

// KPI
const Kpi = React.lazy(() => import('./views/kpi/Kpi'))
const Report = React.lazy(() => import('./views/kpi/Report'))

// 기준 정보 관리
const StationList = React.lazy(() => import('./views/department/StationList'))
const StationDetail = React.lazy(() => import('./views/department/StationDetail'))
const EquipmentList = React.lazy(() => import('./views/department/EquipmentList'))
const EquipmentDetail = React.lazy(() => import('./views/department/EquipmentDetail'))
const RouteList = React.lazy(() => import('./views/department/RouteList'))
const RouteDetail = React.lazy(() => import('./views/department/RouteDetail'))
const SensorList = React.lazy(() => import('./views/department/SensorList'))
const SensorDetail = React.lazy(() => import('./views/department/SensorDetail'))


// 사이트 관리
const DepartmentList = React.lazy(() => import('./views/admin/DepartmentList'))
const DepartmentDetail = React.lazy(() => import('./views/admin/DepartmentDetail'))
const AdminList = React.lazy(() => import('./views/admin/AdminList'))
const AdminDetail = React.lazy(() => import('./views/admin/AdminDetail'))
const TemplateSettings = React.lazy(() => import('./views/admin/TemplateSettings'))
const SiteSettings = React.lazy(() => import('./views/admin/SiteSettings'))


// 디테일
const StationDataDetail = React.lazy(() => import('./views/detail/Station'))
const CompanyDataDetail = React.lazy(() => import('./views/detail/Company'))
const SensorDataDetail = React.lazy(() => import('./views/detail/Sensor'))

// 통계 
const SensorDataStatistics = React.lazy(() => import('./views/statistics/SensorData'))
const DateStatistics = React.lazy(() => import('./views/statistics/DateData'))

const BackupManagement = React.lazy(() => import('./views/backup/BackupManagement'))


/**
 * See {@link https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config GitHub}.
 */
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: '정보 현황판', component: Dashboard },
  { path: '/realtime-content', name: '대시보드', component: RealtimeContent },
  { path: '/router-content', name: '중계기 현황판', component: RouterContent },
  { path: '/sensor-content', name: '센서 현황판', component: SensorContent },


  // 센서 관제
  { path: '/monitoring-management/station', exact: true, name: '충전소&시설별 관제', component: StationMonitoring },
  { path: '/monitoring-management/station-new', exact: true, name: '충전소&시설별 관제', component: StationNewMonitoring },
  { path: '/monitoring-management/station-new/:id', exact: true, name: '충전소&시설별 자세히', component: StationNewDetail },
  { path: '/monitoring-management/equipment-new/:id', exact: true, name: '충전소&시설별 자세히', component: EquipmentDetailNew },
  { path: '/monitoring-management/company', exact: true, name: '기관별 관제', component: CompanyMonitoring },
  { path: '/monitoring-management/sensor', exact: true, name: '센서 연동 관제', component: SensorMonitoring },
  { path: '/monitoring-management/report', exact: true, name: '리포트 확인', component: ReportMonitoring },


  // 이력 관리
  { path: '/logs-management/station', exact: true, name: '충전소별 이력관리', component: StationLog },
  { path: '/logs-management/company', exact: true, name: '기관별 이력관리', component: CompanyLog },
  { path: '/logs-management/sensor', exact: true, name: '센서별 이력관리', component: SensorLog },

  // 게시판
  { path: '/notice-management/board', exact: true, name: '게시판 관리', component: Board },
  { path: '/notice-management/notice', exact: true, name: '공지사항 관리', component: Notice },
  { path: '/notice-management/notice/:id', exact: true, name: '공지사항 자세히', component: NoticeDetail },
  { path: '/notice-management/board/:id', exact: true, name: '게시판 자세히', component: BoardDetail },

  // KPI
  { path: '/kpi-management/kpi', exact: true, name: 'KPI 관리', component: Kpi },
  { path: '/kpi-management/report', exact: true, name: 'KPI 리포트 관리', component: Report },

  // 기준 정보 관리
  { path: '/department-management/station', exact: true, name: '충전소&시설 관리', component: StationList },
  { path: '/department-management/station/:id', name: '충전소&시설 자세히', component: StationDetail },
  { path: '/department-management/equipment', exact: true, name: '기관 관리', component: EquipmentList },
  { path: '/department-management/equipment/:id', name: '기관 자세히', component: EquipmentDetail },
  { path: '/department-management/sensor', exact: true, name: '센서 관리', component: SensorList },
  { path: '/department-management/sensor/:id', name: '센서 자세히', component: SensorDetail },
  { path: '/department-management/route', exact: true, name: '중계기 관리', component: RouteList },
  { path: '/department-management/route/:id', name: '중계기 자세히', component: RouteDetail },


  // 사이트 관리
  { path: '/admin-management/department', exact: true, name: '조직 관리', component: DepartmentList },
  { path: '/admin-management/department/:id', name: '조직 등록/수정', component: DepartmentDetail },

  { path: '/admin-management/admins', exact: true, name: '관리자 관리', component: AdminList },
  { path: '/admin-management/admins/:id', exact: true, name: '관리자 등록/수정', component: AdminDetail },

  { path: '/admin-management/templates', name: '템플릿 설정', component: TemplateSettings },
  { path: '/admin-management/settings', name: '사이트 설정', component: SiteSettings },

  // 디테일
  { path: '/detail/station/:id', exact: true, name: '충전소 자세히', component: StationDataDetail },
  { path: '/detail/company/:id', exact: true, name: '기관별 자세히', component: CompanyDataDetail },
  { path: '/detail/sensor/:id', exact: true, name: '센서별 자세히', component: SensorDataDetail },

  //수신/통계
  { path: '/statistics/sensor-data', exact: true, name: '센서데이터 수신 목록', component: SensorDataStatistics },
  { path: '/statistics/date-data', exact: true, name: '통계', component: DateStatistics },

  { path: '/backupManagement', exact: true, name: '백업 정보 관리', component: BackupManagement },


]

export default routes
