import React from 'react'
import {
  cilSpeedometer,
  cilAlignCenter,
  cilCog,
  cilGroup,
  cilChartLine,
  cilBarcode,
  cilBook,
  cibAudible,
  cibAlgolia,
  cilFile
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import {
  CNavItem,
  CNavGroup
} from '@coreui/react-pro'
import { ElementType } from 'react'

export type Badge = {
  color: string
  text: string
}

export type NavItem = {
  component: string | ElementType
  name: string | JSX.Element
  icon?: string | JSX.Element
  badge?: Badge
  to: string
  items?: NavItem[]
}

const _nav = [
  {
    component: CNavItem,
    name: '대시보드',
    icon: <CIcon icon={cilBook} customClassName="nav-icon" />,
    to: '/realtime-content',
  },
  {
    component: CNavItem,
    name: '정보 현황판',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    to: '/dashboard',
  },
  {
    component: CNavItem,
    name: '중계기 현황판',
    icon: <CIcon icon={cibAudible} customClassName="nav-icon" />,
    to: '/router-content',
  },
  {
    component: CNavItem,
    name: '센서 현황판',
    icon: <CIcon icon={cibAlgolia} customClassName="nav-icon" />,
    to: '/sensor-content',
  },
  {
    component: CNavGroup,
    name: '센서 관제',
    to: '/monitoring-management',
    icon: <CIcon icon={cilChartLine} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '충전소&시설',
        to: '/monitoring-management/station-new',
      },
      {
        component: CNavItem,
        name: '리포트 확인',
        to: '/monitoring-management/report',
      },
    ],
  },
  {
    component: CNavGroup,
    name: '데이터 수신 / 통계',
    to: '/statistics',
    icon: <CIcon icon={cilAlignCenter} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '데이터 수신 목록',
        to: '/statistics/sensor-data',
      },
      {
        component: CNavItem,
        name: '데이터 통계',
        to: '/statistics/date-data',
      },
    ],
  },
  {
    component: CNavGroup,
    name: '게시판 관리',
    to: '/notice-management',
    icon: <CIcon icon={cilBarcode} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '게시판',
        to: '/notice-management/board',
      },
      {
        component: CNavItem,
        name: '공지사항',
        to: '/notice-management/notice',
      },
    ],
  },
  {
    component: CNavGroup,
    name: '기준 정보 등록',
    to: '/department-management',
    icon: <CIcon icon={cilCog} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '충전소&시설  관리',
        to: '/department-management/station',
      },
      {
        component: CNavItem,
        name: '기관 관리',
        to: '/department-management/equipment',
      },
      {
        component: CNavItem,
        name: '센서 관리',
        to: '/department-management/sensor',
      },
      {
        component: CNavItem,
        name: '중계기 관리',
        to: '/department-management/route',
      },
    ],
  },
  {
    component: CNavItem,
    name: '백업 정보 관리',
    icon: <CIcon icon={cilFile} customClassName="nav-icon" />,
    to: '/backupManagement',
  },
  {
    component: CNavGroup,
    name: '사이트 설정',
    to: '/admin-management',
    icon: <CIcon icon={cilGroup} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: '조직 설정',
        to: '/admin-management/department',
      },
      {
        component: CNavItem,
        name: '관리자 설정',
        to: '/admin-management/admins',
      },
      {
        component: CNavItem,
        name: '사이트 설정',
        to: '/admin-management/settings',
      },
    ],
  },
]

export default _nav
