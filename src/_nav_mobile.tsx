import React from 'react'
import {
  cilBook,
  cibCircle
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import {
  CNavItem,
} from '@coreui/react-pro'
import { ElementType } from 'react'

export type Badge = {
  color: string
  text: string
}

export type NavItem = {
  component: string | ElementType
  name: string | JSX.Element
  icon?: string | JSX.Element
  badge?: Badge
  to: string
  items?: NavItem[]
}

const _nav_mobile = [
  {
    component: CNavItem,
    name: '대시보드',
    icon: <CIcon icon={cilBook} customClassName="nav-icon" />,
    to: '/realtime-content',
  },
  {
    component: CNavItem,
    name: '충전소&시설',
    icon: <CIcon icon={cibCircle} customClassName="nav-icon" />,
    to: '/monitoring-management/station-new',
  },
]

export default _nav_mobile
