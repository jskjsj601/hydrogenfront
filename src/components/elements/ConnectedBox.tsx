import React from 'react'
import './box.css'
const ConnectedBox = ({ data = "0", title, className, type = "all" }) => {
  return <>
    <div className={`common-box ${className}-box`}>
      <div>
        <span className={`common-box-left-text ${className}-left-text`}>{title}</span>
        <span className={`common-box-right-text ${className}-right-text`}>{data}</span>
      </div>
    </div>
  </>
}

export default ConnectedBox
