import React from 'react'
import './box.css'
const Box = ({ data = "0", title, className, type = "all" }) => {
  return <>
    <div className={`common-box ${className}-box`}>
      <div>
        <span className={`common-box-left-text ${className}-left-text`}>{title}</span>
        <span className={`common-box-right-text ${className}-right-text`}>{(type !== 'station') && <span>충전소&시설 {data[0]}/</span>} {(type !== 'equipment') && <span>기관 {data[1]}/</span>} 센서 {data[2]}</span>
      </div>
    </div>
  </>
}

export default Box
