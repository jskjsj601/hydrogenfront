import React from 'react'
import {
  CAlert,
  CCallout,
  CCol,
  CImage,
  CRow,
  CButton
} from "@coreui/react-pro"

import {
  Link
} from "react-router-dom"

import warning from "../../assets/icon/warning.png"
import danger from "../../assets/icon/danger.png"
import safety from '../../assets/icon/safety.png'

const StationStatusBox = ({ data }) => {
  return <>

    <CCallout>
      <CRow className="align-items-center">
        <CCol>
          <Link to={`monitoring-management/station-new/${data?.station?.id}`}>
            <span className="real-common-text">
              {data?.station?.name} <br />
            </span>
          </Link>
        </CCol>
      </CRow>
      <CRow className="align-items-center">

        <CCol md={3}>
          {data?.lastSensorData ?
            (<CCol md={2}>
              {data?.lastSensorData?.type === 'NORMAL' && (<CImage src={safety} alt="" />)}
              {(data?.lastSensorData?.type === 'LOW_WARNING' || data?.lastSensorData?.type === 'HIGH_WARNING') && (<CImage src={warning} alt="" />)}
              {(data?.lastSensorData?.type === 'LOW_DANGER' || data?.lastSensorData?.type === 'HIGH_DANGER') && (<CImage src={danger} alt="" />)}

            </CCol>) :
            (<CCol md={2}></CCol>)}
        </CCol>
        <CCol style={{ fontSize: "14px" }}>
          최근 24시간 : 총 {data?.totalCount} {data?.dangerCount > 0 ? <span>중 이상 <span style={{ color: "red" }}>{data?.dangerCount}회 감지</span> </span> : <span>개</span>}
        </CCol>
      </CRow>
    </CCallout>
  </>
}

export default StationStatusBox