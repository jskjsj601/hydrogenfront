import React from 'react'
import {
  CFormTextarea,
  CFormFeedback,
} from '@coreui/react-pro'
const InputArea = ({ idOrName, type, placeholder, formik, value, error, touched, disabled = false,  onKeyPressNumber = (value) => { return value } }) => {
  const onKeyPress = function (e) {
    onKeyPressNumber(e.target.value)
  }
  return <>
    <CFormTextarea
      id={idOrName}
      name={idOrName}
      placeholder={placeholder}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
      value={value}
      rows="6"
      disabled={disabled}
      onKeyPress={onKeyPress}
      className={(error) ? 'form-control is-invalid' : 'form-control'} />
    {
      touched ? (
        <CFormFeedback invalid>{error}</CFormFeedback>
      ) : null
    }
  </>
}
export default InputArea
