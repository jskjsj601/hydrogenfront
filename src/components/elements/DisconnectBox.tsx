import React from 'react'
import './real-box.css'
import {
  CCallout
} from "@coreui/react-pro"
import filters from "../../modules/filters"
const DisconnectBox = ({ data, className = 'danger' }) => {
  const showLimitText = (txt) => {
    if (txt?.length > 6) {
      return txt.substring(0, 6) + "..."
    }
    return txt
  }
  return <>
    <CCallout color={className} className={`real-${className}-box`}>
      <div>
        <span className="real-common-text">{data?.name}</span>
      </div>
      <div style={{ marginTop: "8px" }}>
        <span className={`real-common-text real-${className}-font`}>마지막 접속 시간</span> &nbsp;{filters.date.sensingDate(data?.time)}
      </div>
      <div className="margin-top">
        <span className="real-common-text">(타입) {data?.type}</span>
      </div>
    </CCallout>
  </>
}

export default DisconnectBox
