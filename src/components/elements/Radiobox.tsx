import React from 'react'
import {
  CFormCheck
} from '@coreui/react-pro'
const Radiobox = ({ idOrName, onChange, value, label, isDefault }) => {
  return <>
    <CFormCheck
      inline
      name={idOrName}
      value={value}
      type="radio"
      onChange={onChange}
      label={label}
      checked={isDefault || false} />
  </>
}
export default Radiobox