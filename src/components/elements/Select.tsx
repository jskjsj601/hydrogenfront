import React from 'react'
import {
  CFormSelect,
  CFormFeedback,
} from '@coreui/react-pro'
const Select = ({ idOrName, formik, value, error, touched, items, defaultValue = "", onChangeFunc = (value) => { return value }, disabled = false }) => {
  const drawingItem = items?.map((obj, key) => {
    return <option value={obj.id} key={key}>{obj.name}</option>
  })

  const onChange = function (e) {
    onChangeFunc(e.target.value)
    formik.handleChange(e)
  }
  return <>
    <CFormSelect
      disabled={disabled}
      id={idOrName}
      name={idOrName}
      onChange={onChange}
      onBlur={formik.handleBlur}
      value={value}
      className={(error) ? 'form-control is-invalid' : 'form-control'}>
      {defaultValue ? <option value="">{defaultValue}</option> : (null)}
      {drawingItem}
    </CFormSelect>
    {
      touched ? (
        <CFormFeedback invalid>{error}</CFormFeedback>
      ) : null
    }
  </>
}
export default Select
