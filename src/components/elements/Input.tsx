import React from 'react'
import {
  CFormInput,
  CFormFeedback,
} from '@coreui/react-pro'
const Input = ({ idOrName, type, placeholder, formik, value, error, touched, disabled = false, onKeyPressNumber = (value) => { return value } }) => {
  const onKeyPress = function (e) {
    onKeyPressNumber(e.target.value)
  }
  return <>
    <CFormInput
      id={idOrName}
      name={idOrName}
      type={type}
      placeholder={placeholder}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
      value={value}
      disabled={disabled}
      onKeyPress={onKeyPress}
      className={(error) ? 'form-control is-invalid' : 'form-control'} />
    {
      touched ? (
        <CFormFeedback invalid>{error}</CFormFeedback>
      ) : null
    }
  </>
}
export default Input