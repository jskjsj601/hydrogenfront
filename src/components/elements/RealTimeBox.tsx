import React from 'react'
import './real-box.css'
import {
  CCallout, CRow, CCol
} from "@coreui/react-pro"
import filters from "../../modules/filters"
const RealTimeBox = ({ data, className }) => {
  const showLimitText = (txt) => {
    if (txt?.length > 6) {
      return txt.substring(0, 6) + "..."
    }
    return txt
  }
  return <>
    <CCallout color={className} className={`real-${className}-box`}>
      <CRow>
        <CCol md={6}>
          <div className="real-common-text over-text">{data?.sensor?.name || '등록된 센서 없음'}</div>
        </CCol>
        <CCol >
          <div className={`over-text`} style={{ float: "right" }}>
            <span className={`real-common-text real-${className}-font`} style={{ textOverflow: 'ellipsis' }}>{filters.status.convertToSensorDataStatus(data?.type)} &nbsp;&nbsp;</span>
            <span className={`real-common-text`}>{filters.date.sensingDate(data?.sensingDate)}</span>
          </div>
        </CCol>
      </CRow>

      <CRow>
        <CCol md={7}>
          <div className="real-common-text over-text">{data?.sensor?.station?.name || '등록된 충전소 없음'}</div>
        </CCol>
        <CCol>
          <span className="real-common-text float-right">{data?.data}{data?.sensor?.unit}</span>
        </CCol>
      </CRow>
    </CCallout>
  </>
}

export default RealTimeBox
