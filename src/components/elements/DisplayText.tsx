import React from 'react'
import {
  CCallout
} from "@coreui/react-pro"
import './real-box.css'

const DisplayText = ({ left, right }): JSX.Element => {
  return <>
    <CCallout style={{ paddingTop: "5px", paddingBottom: "5px" }}>
      <div>
        <span className="real-common-text">{left}</span>
        <span className="real-common-text float-right">
          <span className={`real-common-text`}>{right}</span>
        </span>
      </div>
    </CCallout>
  </>
}

export default DisplayText