import React, { useState, useEffect } from 'react'
import {
  CFormInput,
  CImage
} from "@coreui/react-pro"
import service from "../../modules/service"
import LoadingOverlay from 'react-loading-overlay-ts'

const ImageInput = ({ image, inputName, defaultImage, callbackImage }): JSX.Element => {
  const [isLoading, setIsLoading] = useState(false)
  
  const buildImageUrl = (image: string) => {
    if (image?.startsWith("http")) {
      return image
    } else {
      return service.request.baseURL(image)
    }
  }

  const [imageUrl, setImageUrl] = useState("")


  const checkExt = function (ext) {
    const allowExt = ["jpg", "jpeg", "png"]
    return allowExt.find((e) => e === ext)
  }

  const getFileExt = function (fileName) {
    return (
      fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length) ||
      fileName
    )
  }

  const onChangeImage = function (e) {
    if (e.target.files[0]) {
      const fileName = e.target.files[0].name
      const file = e.target.files[0]
      const ext = getFileExt(fileName)
      if (!checkExt(ext)) {
        alert("이미지 확장자를 체크하여 주시기 바랍니다.\nPNG, JPG, JPEG 만 허용합니다.")
        e.target.value = null
        return
      }

      setIsLoading(true)
      const formData = new FormData()
      formData.append("file", file)
      service.request.upload_post("/admin/file/upload", formData).then((response) => {
        setIsLoading(false)
        const data = response.data
        const path = data.payload
        const imageURL = buildImageUrl(path)
        setImageUrl(imageURL)
        callbackImage(path)

      })
    }
  }

  const deleteImage = function () {
    if (window.confirm("이미지를 삭제하시겠습니까?")) {
      setImageUrl("")
      callbackImage("")
    }
  }

  useEffect(() => {
    if (image) {
      setImageUrl(buildImageUrl(image))
    }
  }, [image])

  return <>
    <LoadingOverlay
      active={isLoading}
      spinner
      text='로딩중 입니다....'
    >
      <CFormInput
        type="file"
        id={inputName}
        name={inputName}
        onChange={onChangeImage}
        accept=".png,.jpg,.jpeg" />
      <div style={{ padding: "10px" }}>
        {imageUrl ?
          <CImage fluid
            src={imageUrl}
            align={"start"}
            width={"120px"}
            height={"50px"}
            onClick={() => deleteImage()}
            style={{ cursor: 'pointer' }} />
          :
          <CImage fluid
            src={defaultImage}
            width={"120px"}
            align={"start"}
            style={{ zIndex: 1 }}
            height={"50px"} />
        }
      </div>
    </LoadingOverlay>
  </>
}

export default ImageInput