import React from 'react'
import { useDispatch } from 'react-redux'
import {
  CImage,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarToggler,
} from '@coreui/react-pro'

import { useTypedSelector } from '../store'

import { AppSidebarNav } from './AppSidebarNav'

import CIcon from '@coreui/icons-react'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'


import logo from '../assets/logo.svg'
import { sygnet } from '../assets/brand/sygnet'
// sidebar nav config
import navigation from '../_nav'
import navigation_station from '../_nav_station'

import navigation_user from '../_nav_user'
import navigation_mobile from '../_nav_mobile'
import { isMobile } from "react-device-detect"

const AppSidebar = () => {
  const dispatch = useDispatch()
  const unfoldable = useTypedSelector((state) => state.sidebarUnfoldable)
  const sidebarShow = useTypedSelector((state) => state.sidebarShow)
  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))
  const getNavigation = function () {
    if (isMobile) {
      return navigation_mobile
    }
    if (userInfo.roleType === "ROLE_SUPER_MANAGER") {
      return navigation
    } else if (userInfo.roleType === "ROLE_USER") {
      return navigation_user
    } else if (userInfo.roleType === "ROLE_ADMIN") {
      return navigation_station
    }
  }

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
      onVisibleChange={(visible) => {
        dispatch({ type: 'set', sidebarShow: visible })
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" style={{ justifyContent: "start", paddingLeft: "15px" }}>
        <CImage className="sidebar-brand-full" src={logo} height={32} />
        <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={getNavigation()} />
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      />
    </CSidebar>
  )
}

export default React.memo(AppSidebar)
