/* eslint-disable react/prop-types */
import React from 'react'
import { CNavLink } from '@coreui/react-pro'
import { GoogleMap, InfoWindow, Marker, useJsApiLoader } from '@react-google-maps/api'

// To use the Google Maps JavaScript API, you must register your app project on the Google API Console and get a Google API key which you can add to your app
const apiKey = 'AIzaSyAyCSNABtGK0HYXMHsecai2WmHaaecnUv0'

const defaultCenter = { lat: 36.3801686, lng: 127.3646184 }

const MarkerWithInfoWindow = ({ location }) => {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <Marker
      onClick={() => setIsOpen(!isOpen)}
      position={location}
      title={location.title}
      label={location.label}
    >
      {isOpen && (
        <InfoWindow>
          <CNavLink href={location.www} target="_blank">
            {location.title}
          </CNavLink>
        </InfoWindow>
      )}
    </Marker>
  )
}

const MarkerList = ({locations}) => {
  return locations.map((location, index) => {
    return <MarkerWithInfoWindow key={index.toString()} location={location} />
  })
}

const GoogleMapsComponent = ({center, locations, height, zoom }) => {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: apiKey,
  })

  return (
    isLoaded && (
      <GoogleMap mapContainerStyle={{ height: height }} center={center || defaultCenter} zoom={zoom}>
        {<MarkerList locations={locations || []} />}
        <></>
      </GoogleMap>
    )
  )
}

const GoogleMaps = ({center, locations, height="600px", zoom=16}) => {
  return (
    <GoogleMapsComponent center={center} locations={locations} height={height} zoom={zoom} />
  )
}

export default GoogleMaps
