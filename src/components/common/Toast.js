import React, { useState, useRef, useImperativeHandle, useEffect } from 'react'
import {
  CToaster,
  CToast,
  CToastBody,
  CToastHeader,
} from '@coreui/react-pro'


const Toast = React.forwardRef(({title, date, contents, color}: IToast, ref) => {
  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const exampleToast = (
    <CToast>
      <CToastHeader closeButton>
        <svg
          className="rounded me-2"
          width="20"
          height="20"
          xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice"
          focusable="false"
          role="img"
        >
          <rect width="100%" height="100%" fill={color}></rect>
        </svg>
        <strong className="me-auto">{title}</strong>
        <small>{date}</small>
      </CToastHeader>
      <CToastBody>{contents}</CToastBody>
    </CToast>
  )

  useImperativeHandle(ref, () => ({
    add(title, date, contents) {
      addToast(exampleToast)
    }
  }))

  return <>
    <CToaster ref={toaster} push={toast} placement="top-end" />
  </>
})

Toast.displayName = "Toast"

interface IToast {
  title: any,
  date: any,
  contents: any,
  color: any
}
export default Toast