import React, { useState, useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import {
  CAvatar,
  CCloseButton,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CListGroup,
  CListGroupItem,
  CSidebar,
  CSidebarHeader,
  CCol,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { useTypedSelector } from '../store'
import {
  cilSpeech,
  cilMobile,
  cilBell
} from '@coreui/icons'

import service from '../modules/service'
import filters from '../modules/filters'
import { Toast } from './common'

const AppAside = () => {
  const dispatch = useDispatch()
  const asideShow = useTypedSelector((state) => state.asideShow)
  const [activeKey, setActiveKey] = useState(1)
  const [userList, setUserList] = useState([])
  const [noticeList, setNoticeList] = useState([])
  const [reportList, setReportList] = useState([])
  const [title, setTitle] = useState("")
  const [contents, setContents] = useState("")
  const toastRef = useRef<any>()

  const getUserData = function () {
    const url = `/admin/header/manager`
    service.request.get(url, {}).then(function (response) {
      const data = response.data.payload
      setUserList(data)
    })
  }

  const getColorCode = function (type) {
    if (type === 'NORMAL') {
      return "#00B015"
    } else if (type === 'LOW_WARNING' || type === 'HIGH_WARNING') {
      return '#F79D0F'
    } else if (type === 'LOW_DANGER' || type === 'HIGH_DANGER') {
      return '#FD0000'
    }
  }

  const getColor = function (type) {
    if (type === 'NORMAL') {
      return "success"
    } else if (type === 'LOW_WARNING' || type === 'HIGH_WARNING') {
      return 'warning'
    } else if (type === 'LOW_DANGER' || type === 'HIGH_DANGER') {
      return 'danger'
    }
  }

  const showToast = function () {
    toastRef.current.add()
    dispatch({ type: 'set', asideShow: true })
  }

  const getData = function () {
    const url = `/admin/header/board`
    service.request.get(url, {}).then(function (response) {
      const data = response.data.payload
      setNoticeList(data?.noticeList.filter((element, index) => index < 8))
      if (data?.unReadReport > 0) {
        setTitle("위험 감지.")
        setContents(`경고 및 위험 ${data?.unReadReport}건이 감지되었습니다.`)
        showToast()
      }
      setReportList(data?.reportList.filter((element, index) => index < 4))
    })
  }

  useEffect(() => {
    getUserData()
    getData()
    const interval = setInterval(() => {
      getData()
    }, 6000)
    return () => clearInterval(interval)
  }, [])

  

  return (
    <CSidebar
      colorScheme="light"
      size="xl"
      overlaid
      placement="end"
      visible={asideShow}
      onVisibleChange={(visible) => {
        dispatch({ type: 'set', asideShow: visible })
      }}
    >
      <Toast
        ref={toastRef}
        title={title}
        date={filters.date.lastModifiedDate(new Date())}
        contents={contents}
        color={"#ff1919"} />
      <CSidebarHeader className="bg-transparent p-0">
        <CNav variant="underline">
          <CNavItem>
            <CNavLink
              href="#"
              active={activeKey === 1}
              onClick={(e) => {
                e.preventDefault()
                setActiveKey(1)
              }}
            >
              <CIcon icon={cilBell} />
            </CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink
              href="#"
              active={activeKey === 2}
              onClick={(e) => {
                e.preventDefault()
                setActiveKey(2)
              }}
            >
              <CIcon icon={cilSpeech} />
            </CNavLink>
          </CNavItem>
          <CNavItem className="ms-auto me-2 d-flex align-items-center">
            <CCloseButton onClick={() => dispatch({ type: 'set', asideShow: false })} />
          </CNavItem>
        </CNav>
      </CSidebarHeader>
      <CTabContent>
        <CTabPane visible={activeKey === 1}>
          <CListGroup flush>
            <CListGroupItem className="list-group-item border-start-4 border-start-secondary text-center fw-bold text-medium-emphasis text-uppercase small">
              리포트 처리 알림
            </CListGroupItem>
            {reportList.map((item, key) => {
              return <CListGroupItem className={`border-start-4 border-start-${getColor(item.sensorDataInput?.type)}`} key={key}>
                <div className="message">
                  <div>
                    <small className="text-medium-emphasis">{item.sensorDataInput?.sensor?.station?.name}</small>
                    <small className="text-medium-emphasis float-end mt-1">{filters.date.lastModifiedDate(item.sensorDataInput?.sensingDate)}</small>
                  </div>
                  <hr />
                  <div className="fw-semibold" style={{ color: getColorCode(item.sensorDataInput?.type) }}>{item.sensorDataInput?.sensor?.name}</div>
                  <small className="text-medium-emphasis">
                    처리 상태 : {filters.status.convertToReportStatus(item.status)}, {item.description}
                  </small>

                  {item.adminUser &&
                    <>
                      <hr />
                      <div>
                        <small className="text-medium-emphasis">처리자 <strong>{item.adminUser?.name}</strong> </small>
                        <CAvatar src={service.request.baseURL(item.adminUser?.profileImage)} size="md" className="float-end" />
                      </div>
                    </>
                  }
                </div>
              </CListGroupItem>
            })}
          </CListGroup>
        </CTabPane>

        <CTabPane className="p-3" visible={activeKey === 2}>
          <CListGroup flush>

            {noticeList.map((item, key) => {
              return (
                <div key={key}>
                  <div className="message">
                    <div className="py-3 pb-5 me-3 float-start">
                      <CAvatar src={service.request.baseURL(item.adminUser?.profileImage)} status="success" size="md" />
                    </div>
                    <div>
                      <small className="text-medium-emphasis">{item.adminUser?.name}</small>
                      <small className="text-medium-emphasis float-end mt-1">{filters.date.lastModifiedDate(item.createDate, item.updateDate)}</small>
                    </div>
                    <div className="text-truncate fw-semibold">{item.title}</div>
                    <small className="text-medium-emphasis" dangerouslySetInnerHTML={{ __html: item.description }}>
                    </small>
                  </div>
                  <hr />
                </div>
              )
            })}

          </CListGroup>
        </CTabPane>
      </CTabContent>

      <CCol md={12} style={{ position: "absolute", bottom: "0px" }}>
        <CListGroup flush>
          <CListGroupItem className="list-group-item border-start-4 border-start-secondary border-top text-center fw-bold text-medium-emphasis text-uppercase small">
            연락처
          </CListGroupItem>
          {userList.map((item, key) => {
            return (<CListGroupItem className="border-start-4 border-start-info" key={key}>
              <div>
                <strong>{item.name}</strong> ({item.levelName})
              </div>
              <small className="text-medium-emphasis me-3">
                <CIcon icon={cilMobile} /> {item.mobileNumber}
              </small>
            </CListGroupItem>
            )
          })}
        </CListGroup>
      </CCol>
    </CSidebar>
  )
}

export default React.memo(AppAside)
