import React from 'react'
import { CFooter } from '@coreui/react-pro'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="https://www.ipacom.co.kr/" target="_blank" rel="noopener noreferrer">
          Pacom 파콤(주)
        </a>
        <span className="ms-1">&copy; 2022 Pacom.</span>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
