import React, { useState, useEffect } from 'react'
import { Link, useHistory } from "react-router-dom"
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilEnvelopeOpen } from '@coreui/icons'

import danger from '../../assets/icon/danger.png'
import safety from '../../assets/icon/safety.png'
import warning from '../../assets/icon/warning.png'

import service from '../../modules/service'
import filters from '../../modules/filters'

const AppHeaderDropdownMssg = () => {
  const [itemsCount, setItemCount] = useState(0)
  const [list, setList] = useState([])


  const getData = function () {
    const url = `/admin/header/report`
    service.request.get(url, {}).then(function (response) {
      const data = response.data.payload
      setList(data.filter((element, index) => index < 4))
      setItemCount(data.length)
    })
  }

  useEffect(() => {
    getData()
    const interval = setInterval(() => {
      getData()
    }, 6000)
    return () => clearInterval(interval)
  }, [])

  const isEmpty = (itemsCount === 0)
  let items
  if (isEmpty) {
    items = (<CDropdownItem href="#">
    <div className="message">
      <div className="small text-medium-emphasis text-truncate"  style={{minWidth: "600px"}}>데이터가 없습니다.</div>
    </div>
  </CDropdownItem>)
  } else {
    items = list.map((item, key) => {
      return (<CDropdownItem href="/#/monitoring-management/report" key={key}>
        <div className="message">
          <div className="pt-3 me-3 float-start">
            {item.sensorDataInput?.type === 'NORMAL' && (<CAvatar src={safety} alt="" style={{ float: "right" }} />)}
            {(item.sensorDataInput?.type === 'LOW_WARNING' || item.sensorDataInput?.type === 'HIGH_WARNING') && (<CAvatar src={warning} alt="" style={{ float: "right" }} />)}
            {(item.sensorDataInput?.type === 'LOW_DANGER' || item.sensorDataInput?.type === 'HIGH_DANGER') && (<CAvatar src={danger} alt="" style={{ float: "right" }} />)}
          </div>
          <div>
            <small className="text-medium-emphasis">{item.sensorDataInput?.sensor?.station?.name} ({item.sensorDataInput?.sensor?.station?.address1})</small>
            <small className="text-medium-emphasis float-end mt-1">감지일자 ({filters.date.lastModifiedDate(item.sensorDataInput?.sensingDate || '')})</small>
          </div>
          <div className="text-truncate font-weight-bold">
            <span className="fa fa-exclamation text-danger"></span> {item.sensorDataInput?.sensor?.name} 
          </div>
          
          <div className="small text-medium-emphasis text-truncate"  style={{minWidth: "600px"}}>
            {filters.status.convertToSensorType(item.sensorDataInput?.sensor?.type)}, 
            데이터 숫자 ({item.sensorDataInput?.data}{item.sensorDataInput?.unit}), 
            처리 상태 : {filters.status.convertToReportStatus(item.status)}
          </div>
          <div className="small text-medium-emphasis text-truncate">
            처리 내역 : {item.description}
          </div>
        </div>
      </CDropdownItem>)
    })
  }
  return (
    <CDropdown variant="nav-item" alignment="end" >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CIcon icon={cilEnvelopeOpen} size="lg" className="my-1 mx-2" />
        <CBadge
          shape="rounded-pill"
          color="info-gradient"
          className="position-absolute top-0 end-0"
        >
          {itemsCount}
        </CBadge>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" >
        <CDropdownHeader>
          <strong>최근 리포팅 데이터</strong>
        </CDropdownHeader>

        {items}
        
        <CDropdownItem href="/#/monitoring-management/report" className="text-center border-top">
          <strong>전체 리포팅 데이터 보기</strong>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdownMssg
