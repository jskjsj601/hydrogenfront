import React, { useCallback, useEffect, useState } from 'react'
import { useHistory } from "react-router-dom"
import {
  CAvatar,
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react-pro'

import {
  WarningModal
} from '../../components/modal'

import { useTypedSelector } from '../../store'
import service from '../../modules/service'
const AppHeaderDropdown = () => {
  const history = useHistory()
  const [showPopup, setShowPopup] = useState(false)
  const logout = useCallback(() => {
    if(window.confirm("로그아웃 하시겠습니까?")) {
      history.push("/login")
    }
  }, [history])

  useEffect(() => {
    const interval = setInterval(() => {
      if (!window.navigator.onLine) {
        if(showPopup === false) {
          setShowPopup(true)
        }
      } else {
        if(showPopup !== false) {
          setShowPopup(false)
        }
      }
    }, 6000)
    return () => clearInterval(interval)
  }, [window.navigator.onLine])
  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))
  return (
    <CDropdown variant="nav-item" alignment="end">
      <WarningModal
        showPopup={showPopup} />
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar src={service.request.baseURL(userInfo.profileImage)}   />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0">
        <CDropdownHeader className="fw-semibold py-2">관리자 계정정보</CDropdownHeader>
        <CDropdownItem>
          {userInfo.username} ({userInfo.name})
        </CDropdownItem>
        <CDropdownItem>
          {userInfo.department?.name} / {userInfo.station?.name}
        </CDropdownItem>
        <CDropdownItem >
          {userInfo.email}
        </CDropdownItem>
        <CDropdownItem onClick={logout} className="text-center border-top" style={{cursor: "pointer"}}>
          <strong>로그아웃</strong>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
