import React, { useState, useEffect } from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CProgress,
  CAvatar,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilBell } from '@coreui/icons'

import danger from '../../assets/icon/danger.png'
import safety from '../../assets/icon/safety.png'
import warning from '../../assets/icon/warning.png'

import service from '../../modules/service'
import filters from '../../modules/filters'

const AppHeaderDropdownNotif = () => {
  const [itemsCount, setItemCount] = useState(0)
  const [list, setList] = useState([])
  const getData = function () {
    const url = `/admin/header/danger`
    service.request.get(url, {}).then(function (response) {
      const data = response.data.payload
      setList(data.filter((element, index) => index < 4))
      setItemCount(data.length)
    })
  }

  useEffect(() => {
    getData()
    const interval = setInterval(() => {
      getData()
    }, 6000)
    return () => clearInterval(interval)
  }, [])

  const isEmpty = (itemsCount === 0)
  let items
  if (isEmpty) {
    items = (<CDropdownItem href="#">
    <div className="message">
      <div className="small text-medium-emphasis text-truncate"  style={{minWidth: "600px"}}>데이터가 없습니다.</div>
    </div>
  </CDropdownItem>)
  } else {
    items = list.map((item, key) => {
      return (<CDropdownItem href={`/#/detail/sensor/${item?.sensor?.id}`} key={key}>
        <div className="message">
          <div className="pt-3 me-3 float-start">
            {item?.type === 'NORMAL' && (<CAvatar src={safety} alt="" style={{ float: "right" }} />)}
            {(item?.type === 'LOW_WARNING' || item?.type === 'HIGH_WARNING') && (<CAvatar src={warning} alt="" style={{ float: "right" }} />)}
            {(item?.type === 'LOW_DANGER' || item?.type === 'HIGH_DANGER') && (<CAvatar src={danger} alt="" style={{ float: "right" }} />)}
          </div>
          <div>
            <small className="text-medium-emphasis">{item.sensor?.station?.name} ({item.sensor?.station?.address1})</small>
            <small className="text-medium-emphasis float-end mt-1">감지일자 ({filters.date.lastModifiedDate(item?.sensingDate || '')})</small>
          </div>
          <div className="text-truncate font-weight-bold">
            <span className="fa fa-exclamation text-danger"></span> {item?.sensor?.name} 
          </div>
          
          <div className="small text-medium-emphasis text-truncate"  style={{minWidth: "600px"}}>
            {filters.status.convertToSensorType(item?.sensor?.type)}, 
            데이터 숫자 ({item?.data}{item?.unit})
          </div>
        </div>
      </CDropdownItem>)
    })
  }
  return (
    <CDropdown variant="nav-item" alignment="end">
      <CDropdownToggle caret={false}>
      <CIcon icon={cilBell} size="lg" className="my-1 mx-2" />
        <CBadge
          shape="rounded-pill"
          color="danger-gradient"
          className="position-absolute top-0 end-0"
        >
          {itemsCount}
        </CBadge>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" >
        <CDropdownHeader>
          <strong>최근 위험 센서 데이터</strong>
        </CDropdownHeader>
        {items}
        <CDropdownItem href="/#/statistics/sensor-data" className="text-center border-top">
          <strong>전체 이력 관리 보기</strong>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdownNotif
