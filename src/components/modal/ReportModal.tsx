import React, { useState, useEffect } from 'react'
import {
  CButton,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CTable,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CTableBody,
  CTableDataCell
} from '@coreui/react-pro'
import {
  Select,
  InputArea
} from '../../components/elements'
import styles from '../../modules/css/apply.module.css'
import { useFormik } from "formik"
import * as Yup from 'yup'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"
import filters from '../../modules/filters'
const ReportModal = ({ showPopup, setShowPopup, updateId, refresh, sensorId = 0 }) => {
  const [id, setId] = useState(updateId || 0)
  const [isUpdate, setUpdate] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [reportList, setReport] = useState<any>([])
  const formik = useFormik({
    initialValues: {
      description: "",
      status: "INCOMPLETE"
    },
    validationSchema: Yup.object().shape({}),
    onSubmit: (values) => {
      setIsLoading(true)
      if (isUpdate) {
        service.request.put(`/admin/sensorDataReport/${updateId}`, values).then(function () {
          setIsLoading(false)
          refresh()
          close()
        })
      } else {
        service.request.post(`/admin/sensorDataReport/sensor/${sensorId}`, values).then(function () {
          setIsLoading(false)
          refresh()
          close()
        })
      }
    }
  })

  const close = function () {
    setId(0)
    setShowPopup(false)
  }

  useEffect(() => {
    setId(updateId)
    if (updateId === 0) {
      setUpdate(false)
      setIsLoading(true)
      if (sensorId !== 0) {
        formik.setFieldValue("description", '')
        formik.setFieldValue("status", 'INCOMPLETE')
        service.request.get(`/admin/sensorDataReport/popup/${sensorId}`, {}).then(function (response) {
          const data = response.data.payload
          setReport(data?.reportList)
          setIsLoading(false)
        })
      }
    } else {
      setUpdate(true)
      setIsLoading(true)
      service.request.get(`/admin/sensorDataReport/${updateId}`, {}).then(function (response) {
        const data = response.data.payload
        formik.setFieldValue("description", data?.report?.description || '')
        formik.setFieldValue("status", data?.report?.status || 'INCOMPLETE')
        setReport(data?.reportList)
        setIsLoading(false)
      })
    }
  }, [showPopup])

  return (
    <>
      <CModal size="lg" visible={showPopup} keyboard={false} backdrop="static" onClose={() => close()} >
        <CModalHeader>
          {isUpdate ? (<CModalTitle>리포트 수정 </CModalTitle>) : (<CModalTitle>리포트 추가 </CModalTitle>)}
        </CModalHeader>

        <CForm onSubmit={formik.handleSubmit}>
          <LoadingOverlay
            active={isLoading}
            spinner
            text='로딩중 입니다....'
          >
            <CModalBody>
              <CRow>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>처리 내용</CFormLabel>
                    <InputArea
                      idOrName={"description"}
                      type={"text"}
                      placeholder={"알림내용을 입력하여 주시기 바랍니다."}
                      formik={formik}
                      value={formik.values.description}
                      error={formik.errors.description}
                      touched={formik.touched.description} />
                  </div>
                </CCol>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>상태</CFormLabel>
                    <Select
                      idOrName={"status"}
                      formik={formik}
                      value={formik.values.status}
                      error={formik.errors.status}
                      touched={formik.touched.status}
                      items={[
                        { "id": "INCOMPLETE", "name": "처리전" },
                        { "id": "CHECKING", "name": "처리중" },
                        { "id": "COMPLETE", "name": "처리완료" },
                        { "id": "DENIED", "name": "조치 보류" },
                        { "id": "EMERGENCY", "name": "긴급상황" }
                      ]} />
                  </div>
                </CCol>
              </CRow>
              <CRow>
                <CCol md={12}>
                  조치사항
                </CCol>
                <CCol md={12}>
                  <CTable align="middle" className="mb-0 border" hover responsive bordered>
                    <CTableHead color="light">
                      <CTableRow>
                        <CTableHeaderCell className={styles.tableFont} scope="col">조치상태</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">조치사항</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">담당자</CTableHeaderCell>
                        <CTableHeaderCell className={styles.tableFont} scope="col">처리일자</CTableHeaderCell>
                      </CTableRow>
                    </CTableHead>
                    <CTableBody>
                      {
                        (reportList?.length === 0) ?
                          (<CTableRow><CTableDataCell colSpan={10} style={{ textAlign: "center" }} >데이터가 없습니다.</CTableDataCell></CTableRow>) :
                          (
                            reportList?.map((item, key) => {
                              return <CTableRow key={key}>
                                <CTableDataCell className={styles.tableFont} scope="row">{filters.status.convertToReportStatus(item.status)}</CTableDataCell>
                                <CTableDataCell className={styles.tableFont} scope="row">{item.description}</CTableDataCell>
                                <CTableDataCell className={styles.tableFont} scope="row">
                                  {item?.adminUser?.name}
                                </CTableDataCell>
                                <CTableDataCell className={styles.tableFont} scope="row">
                                  {filters.date.lastModifiedDate(item?.createDate)}
                                </CTableDataCell>
                              </CTableRow >
                            })
                          )
                      }
                    </CTableBody>
                  </CTable>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
              <CButton color="secondary" onClick={() => close()}>
                닫기
              </CButton>
              {isUpdate ? (<CButton color="primary" type="submit">수정하기</CButton>) : (<CButton color="primary" type="submit">추가하기</CButton>)}
            </CModalFooter>
          </LoadingOverlay>

        </CForm>
      </CModal>
    </>
  )

}
export default ReportModal
