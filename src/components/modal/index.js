import ManagerModal from './ManagerModal'
import PostCodeModal from './PostCodeModal'
import ReportModal from './ReportModal'
import WarningModal from './WarningModal'
export  {
  ManagerModal,
  PostCodeModal,
  ReportModal,
  WarningModal
}
