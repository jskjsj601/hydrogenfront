import React, { useState, useEffect } from 'react'
import {
  CButton,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CForm,
  CFormLabel,
} from '@coreui/react-pro'
import {
  Input,
  Select
} from '../../components/elements'
import { useFormik } from "formik"
import * as Yup from 'yup'
import LoadingOverlay from 'react-loading-overlay-ts'
import service from "../../modules/service"

const ManagerModal = ({ showPopup, setShowPopup, updateId, search, setUpdateId, type, baseId }) => {
  const [id, setId] = useState(0)
  const [isUpdate, setUpdate] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const form = Yup.object().shape({
    name: Yup.string().required('담당자명은 필수 입니다.'),
    email: Yup.string().email("이메일 형식을 제대로 입력하여 주시기 바랍니다.").required('이메일은 필수 입니다.'),
    mobileNumber: Yup.string().matches(/^01([0|1|6|7|8|9])-([0-9]{3,4})-([0-9]{4})$/, "전화번호 형식을 확인하여 주시기 바랍니다.").required('휴대폰번호는 필수 입니다.'),
    levelName: Yup.string().required('직급은 필수 입니다.'),
  })

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      mobileNumber: "",
      levelName: "",
      status: "USE"
    },
    validationSchema: form,
    onSubmit: (values) => {
      if (isUpdate) {
        if (window.confirm("담당자를 수정 하시겠습니까?")) {
          setIsLoading(true)
          service.request.put(`/admin/manager/${id}`, values).then(function () {
            alert("담당자 정보가 수정 되었습니다.")
            setIsLoading(false)
            close()
            search()
          })
        }
      } else {
        if (window.confirm("담당자를 등록하시겠습니까?")) {
          setIsLoading(true)
          service.request.post(`/admin/manager/${type}/${baseId}`, values).then(function () {
            alert("담당자 정보가 등록 되었습니다.")
            setIsLoading(false)
            close()
            search()
          })
        }
      }
    }
  })

  const onKeyPressNumber = function (value) {
    value = value
      .replace(/[^0-9]/g, '')
      .replace(/^(\d{0,3})(\d{0,4})(\d{0,4})$/g, "$1-$2-$3").replace(/(-{1,2})$/g, "")
    formik.setFieldValue("mobileNumber", value)
  }


  const close = function () {
    setUpdateId(0)
    for (const touch in formik.touched) {
      formik.touched[touch] = false
    }
    for (const error in formik.errors) {
      formik.errors[error] = ""
    }
    for (const value in formik.values) {
      formik.values[value] = ""
    }
    setShowPopup(false)
  }

  useEffect(() => {
    /* eslint-disable react-hooks/exhaustive-deps */
    setId(updateId)
    if (updateId === 0) {
      setUpdate(false)
      // 
    } else {
      setUpdate(true)
      setIsLoading(true)
      service.request.get(`/admin/manager/${updateId}`, {}).then(function (response) {
        const data = response.data.payload
        formik.setFieldValue("name", data.name || '')
        formik.setFieldValue("email", data.email || '')
        formik.setFieldValue("mobileNumber", data.mobileNumber || '')
        formik.setFieldValue("status", data.status || '')
        formik.setFieldValue("levelName", data.levelName || '')
        setIsLoading(false)
      })
    }
  }, [updateId])

  return (
    <>
      <CModal size="lg" visible={showPopup} keyboard={false} backdrop="static" onClose={() => close()} >
        <CModalHeader>
          {isUpdate ? (<CModalTitle>담당자 수정 </CModalTitle>) : (<CModalTitle>담당자 추가 </CModalTitle>)}
        </CModalHeader>
        <CForm onSubmit={formik.handleSubmit}>
          <LoadingOverlay
            active={isLoading}
            spinner
            text='로딩중 입니다....'
          >
            <CModalBody>
              <CRow>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>이름</CFormLabel>
                    <Input
                      idOrName={"name"}
                      type={"text"}
                      placeholder={"담당자명을 입력하여 주세요"}
                      formik={formik}
                      value={formik.values.name}
                      error={formik.errors.name}
                      touched={formik.touched.name} />
                  </div>
                </CCol>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>이메일</CFormLabel>
                    <Input
                      idOrName={"email"}
                      type={"text"}
                      placeholder={"이메일을 입력하여 주세요"}
                      formik={formik}
                      value={formik.values.email}
                      error={formik.errors.email}
                      touched={formik.touched.email} />
                  </div>
                </CCol>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>휴대폰 번호</CFormLabel>
                    <Input
                      idOrName={"mobileNumber"}
                      type={"text"}
                      placeholder={"휴대폰 번호를 입력하여 주세요"}
                      formik={formik}
                      value={formik.values.mobileNumber}
                      error={formik.errors.mobileNumber}
                      touched={formik.touched.mobileNumber}
                      onKeyPressNumber={onKeyPressNumber} />
                  </div>
                </CCol>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>직급</CFormLabel>
                    <Input
                      idOrName={"levelName"}
                      type={"text"}
                      placeholder={"직급을 입력하여 주세요"}
                      formik={formik}
                      value={formik.values.levelName}
                      error={formik.errors.levelName}
                      touched={formik.touched.levelName} />
                  </div>
                </CCol>
                <CCol md={12}>
                  <div className="mb-3">
                    <CFormLabel>상태</CFormLabel>
                    <Select
                      idOrName={"status"}
                      formik={formik}
                      value={formik.values.status}
                      error={formik.errors.status}
                      touched={formik.touched.status}
                      items={[
                        { "id": "USE", "name": "사용중" },
                        { "id": "NOT_USE", "name": "사용중지" }
                      ]} />
                  </div>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
              <CButton color="secondary" onClick={() => close()}>
                닫기
              </CButton>
              {isUpdate ? (<CButton color="primary" type="submit">수정하기</CButton>) : (<CButton color="primary" type="submit">추가하기</CButton>)}
            </CModalFooter>
          </LoadingOverlay>
        </CForm>
      </CModal>
    </>
  )
}
export default ManagerModal