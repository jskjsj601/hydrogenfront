import React, { useState, useEffect } from 'react'
import {
  CModal,
  CModalBody,
  CRow,
  CCol,
  CImage
} from '@coreui/react-pro'
import warning from '../../assets/icon/server_warning.png'

const WarningModal = ({ showPopup }) => {
  return (
    <>
      <CModal size="lg" visible={showPopup} keyboard={false} backdrop="static">
        <CModalBody>
          <CRow>
            <CCol>
              <span style={{ color: "red", fontWeight: "bold" }}>서버 연결 끊김</span>
            </CCol>
          </CRow>
          <CRow>
            <CCol>
              <CImage src={warning} />
            </CCol>
          </CRow>
          <CRow>
            <CCol style={{ margin: "0 auto" }}>
              <span style={{ color: "red", fontWeight: "bold" }}>서버 접속이 되지 않습니다. <br />관리자에게 문의하여 주시기 바랍니다.</span>
            </CCol>
          </CRow>
        </CModalBody>
      </CModal>
    </>
  )
}
export default WarningModal