import React from 'react'
import {
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CButton,
  CModalTitle,
  CRow,
  CCol
} from '@coreui/react-pro'
import DaumPostcode from 'react-daum-postcode'
const PostCodeModal = ({ showPopup, setShowPopup, handlePostcode }) => {
  const close = function () {
    setShowPopup(false)
  }
  return (
    <>
      <CModal size="lg" visible={showPopup} keyboard={false} backdrop={true} onClose={() => close()} >
        <CModalHeader>
          <CModalTitle>주소 찾기 </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol md={12}>
              <DaumPostcode
                onComplete={handlePostcode}
                autoClose
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => close()}>
            닫기
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default PostCodeModal
