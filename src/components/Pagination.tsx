import React, { useState, useEffect } from 'react'
import {
  CPaginationItem,
  CPagination
} from '@coreui/react-pro'

const Pagination = ({ currentPage, totalElements, onChangePage, viewType = "compact" }): JSX.Element => {
  const [page, setPage] = useState(1)
  const max = 10
  useEffect(() => {
    setPage(currentPage)
  }, [currentPage])

  const setPageFunction = function (page) {
    onChangePage(page)
  }

  const getPager = function (totalElements, currentPage, pageSize) {
    const totalPages = Math.ceil(totalElements / pageSize)
    let startPage
    let endPage
    if (totalPages <= 10) {
      startPage = 1
      endPage = totalPages
    } else {
      if (currentPage <= 6) {
        startPage = 1
        endPage = 10
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9
        endPage = totalPages
      } else {
        startPage = currentPage - 5
        endPage = currentPage + 4
      }
    }

    const startIndex = (currentPage - 1) * pageSize
    const endIndex = Math.min(startIndex + pageSize - 1, totalElements - 1)
    const pager: string[] = []
    for (let i = startPage; i < endPage + 1; i++) {
      pager.push(String(i))
    }

    return {
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pager
    }
  }

  const pager = getPager(totalElements, page, max)
  const isPrev = pager.currentPage === 1 ? true : false
  const isNext = pager.currentPage === pager.totalPages

  return <>
    <CPagination aria-label="Page navigation example" style={{ float: "right" }}>
      <CPaginationItem aria-label="Previous" disabled={isPrev} onClick={() => setPageFunction(pager.currentPage - 1)} style={{ cursor: "pointer" }}>
        <span aria-hidden="true">&laquo;</span>
      </CPaginationItem>

      {viewType === 'full' && pager.pages.map((obj, index) =>
        <CPaginationItem key={index} style={{ cursor: "pointer" }} onClick={() => setPageFunction(Number(obj))}
          active={pager.currentPage === Number(obj)}>{obj}</CPaginationItem>
      )
      }
      {viewType === 'compact' &&
        (<CPaginationItem style={{ cursor: "pointer" }} onClick={() => setPageFunction(Number(pager.currentPage))}
          active={true}>{pager.currentPage}</CPaginationItem>)
      }
      <CPaginationItem aria-label="Next" disabled={isNext} onClick={() => setPageFunction(pager.currentPage + 1)} style={{ cursor: "pointer" }}>
        <span aria-hidden="true">&raquo;</span>
      </CPaginationItem>
    </CPagination>
  </>
}

export default Pagination
