import React, { useRef, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { useTypedSelector } from '../store'
import {
  CButtonGroup,
  CFormCheck,
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
} from '@coreui/react-pro'
import {
  cilApplicationsSettings,
  cilMenu,
  cilSun,
  cilMoon
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { Toast } from './common'
import { AppBreadcrumb, } from './index'
import { logo } from '../assets/brand/logo'
import service from "../modules/service"
import { isMobile } from "react-device-detect"

import {
  AppHeaderDropdown,
  AppHeaderDropdownNotif,
  AppHeaderDropdownTasks,
  AppHeaderDropdownMssg,
} from './header/index'

const AppHeader = (): JSX.Element => {
  const dispatch = useDispatch()
  const theme = useTypedSelector((state) => state.theme)
  const toastRef = useRef<any>()
  const [count, setCount] = useState(0)

  theme === 'dark'
    ? document.body.classList.add('dark-theme')
    : document.body.classList.remove('dark-theme')

  const sidebarShow = useTypedSelector((state) => state.sidebarShow)
  const asideShow = useTypedSelector((state) => state.asideShow)


  return (
    <CHeader position="fixed" className="mb-4">
      <Toast
        ref={toastRef}
        title={"위험이 감지 되었습니다."}
        date={"2022-04-20 12:11:12"}
        contents={"서울 충전소 SN112 센서에서 위험 5건이 감지되었습니다."}
        color={"#ff1919"} />
      <CContainer fluid>
        <CHeaderToggler
          className="ps-1"
          onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none">
        </CHeaderBrand>

        <CHeaderNav className="ms-auto me-4">
          <CButtonGroup aria-label="Theme switch">
            <CFormCheck
              type="radio"
              button={{ color: 'primary' }}
              name="theme-switch"
              id="btn-light-theme"
              autoComplete="off"
              label={<CIcon icon={cilSun} />}
              checked={theme === 'default'}
              onChange={() => dispatch({ type: 'set', theme: 'light' })}
            />
            <CFormCheck
              type="radio"
              button={{ color: 'primary' }}
              name="theme-switch"
              id="btn-dark-theme"
              autoComplete="off"
              label={<CIcon icon={cilMoon} />}
              checked={theme === 'dark'}
              onChange={() => dispatch({ type: 'set', theme: 'dark' })}
            />
          </CButtonGroup>
        </CHeaderNav>
        {!isMobile &&
          <CHeaderNav>
            <AppHeaderDropdownMssg />
            <AppHeaderDropdownTasks />
            <AppHeaderDropdownNotif />
          </CHeaderNav>
        }
        {!isMobile &&
          <CHeaderNav className="ms-3 me-4">
            <AppHeaderDropdown />
          </CHeaderNav>
        }
        {!isMobile &&
          <CHeaderToggler
            className="px-md-0 me-md-3"
            onClick={() => dispatch({ type: 'set', asideShow: !asideShow })}>
            <CIcon icon={cilApplicationsSettings} size="lg" />
          </CHeaderToggler>
        }
      </CContainer>
      <CHeaderDivider />
      <CContainer fluid>
        <AppBreadcrumb />
      </CContainer>
    </CHeader>
  )
}

export default AppHeader
