import React, { Suspense, useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import { useTypedSelector } from '../store'
import { Redirect, Route, Switch } from 'react-router-dom'
import { CContainer, CSpinner } from '@coreui/react-pro'

// routes config
import routes from '../routes'

const AppContent = () => {
  const userInfo = JSON.parse(useTypedSelector((state) => state.userInfo))
  const history = useHistory()
  const [time, setTime] = useState(sessionStorage.getItem("checkTime") ? Number(sessionStorage.getItem("checkTime")) : userInfo.sessionIntervalTime)
  let checkTime = 0

  useEffect(() => {
    if (userInfo.checkSessionExpired === 'UNCHECK') {
      /* eslint-disable react-hooks/exhaustive-deps */
      checkTime = sessionStorage.getItem("checkTime") ? Number(sessionStorage.getItem("checkTime")) : userInfo.sessionIntervalTime
      sessionStorage.setItem("checkTime", String(checkTime))
      const interval = setInterval(() => {
        checkTime = Number(sessionStorage.getItem("checkTime")) - 1
        sessionStorage.setItem("checkTime", String(checkTime))
        setTime(checkTime)
        if (checkTime === 0) {
          alert("세션의 만료 시간이 되었습니다.\n로그인을 다시 해주시기 바랍니다.")
          history.push("/login")
        }
      }, 1000)
      return () => clearInterval(interval)
    }
  }, [])
  return (
    <CContainer fluid>
      <p className="sessionTime">{(userInfo.checkSessionExpired === 'UNCHECK') ? `세션 유지 시간(초) : ${time}` : `세션 만료 없음`}</p>
      <Suspense fallback={<CSpinner color="primary" />}>
        <Switch>
          {routes.map((route, idx) => {
            return (
              route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  render={() => (
                    <>
                      <route.component />
                    </>
                  )}
                />
              )
            )
          })}
          <Redirect from="/" to="/realtime-content" />
        </Switch>
      </Suspense>
    </CContainer>
  )
}

export default React.memo(AppContent)
